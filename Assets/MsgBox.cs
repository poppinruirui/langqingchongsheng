﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MsgBox : MonoBehaviour {

    public static MsgBox s_Instance = null;

    public Text _txtContent;

    void Awake()
    {
        s_Instance = this;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowMsg( string szContent )
    {
        _txtContent.text = szContent;
    }
}
