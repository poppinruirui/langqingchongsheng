using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using SonicBloom.Koreo;
    
public class GUIAction : MonoBehaviour
{
    public const string KOREO_EVENT_TRACK_BEAT_CURVE = "TRACK_BEAT_CURVE";

    public Canvas _canvas;
    public GameObject _product_title;
    public Text _bgm_title;

    public Button _menu_button;    
    public Button _play_button;
    public Button _next_button;
    public Button _prev_button;

    public Button _home_button;
    public Button _shoot_button;
    public Button _split_button;
    public Dropdown _ctrl_mode_dropdown;
    public Dropdown _dir_indicator_type_dropdown;

	public Button _spit_spore;
	public Button _spit_ball;
	public Button _obscene_unfold;
	public Button _spit_thorn;


    public float _rhythm_rotate_factor = 2.5f;
    public float _rhythm_scale_factor  = 0.04f;

    void Start()
    {
//        this._play_button.onClick.AddListener(this.OnPlayButtonClick);
//        this._home_button.onClick.AddListener(this.OnHomeButtonClick);
//        this._next_button.onClick.AddListener(this.OnNextButtonClick);
//        this._prev_button.onClick.AddListener(this.OnPrevButtonClick);
//        this._shoot_button.onClick.AddListener(this.OnShootButtonClick);
//        this._split_button.onClick.AddListener(this.OnSplitButtonClick);
//        this._menu_button.onClick.AddListener(this.OnMenuButtonClick);
        this._ctrl_mode_dropdown.onValueChanged.AddListener(this.OnCtrlModeDropdownValueChanged);
//        this._dir_indicator_type_dropdown.onValueChanged.AddListener(this.OnDirIndicatorTypeDropdownValueChanged);
		this._spit_spore.onClick.AddListener(this.OnSpitSporeButtonClick);
		this._spit_ball.onClick.AddListener(this.OnSpitBallButtonClick);
		this._obscene_unfold.onClick.AddListener(this.OnObsceneUnfoldButtonClick);
		this._spit_thorn.onClick.AddListener(this.OnSpitThornButtonClick);

//        Koreographer.Instance.RegisterForEventsWithTime(KOREO_EVENT_TRACK_BEAT_CURVE,
//                                                        this.OnKoreoEventTrackBeatCurve);

		SetCtrlMode ( CtrlMode.CTRL_MODE_HAND );
    }

    void OnDestroy()
    {
//        if (Koreographer.Instance != null) {
//            Koreographer.Instance.UnregisterForAllEvents(this);
//        }
    }
    
    void FixedUpdate()
    {
    }

    public bool Intersect(Vector3 point)
    {
		/*
		    RectTransformUtility.RectangleContainsScreenPoint(this._play_button.GetComponent<RectTransform>(),
                                                                 point,
                                                                 this._canvas.worldCamera) ||
            RectTransformUtility.RectangleContainsScreenPoint(this._home_button.GetComponent<RectTransform>(),
                                                              point,
                                                              this._canvas.worldCamera) ||
            RectTransformUtility.RectangleContainsScreenPoint(this._next_button.GetComponent<RectTransform>(),
                                                              point,
                                                              this._canvas.worldCamera) ||
            RectTransformUtility.RectangleContainsScreenPoint(this._prev_button.GetComponent<RectTransform>(),
                                                              point,
                                                              this._canvas.worldCamera) ||
            RectTransformUtility.RectangleContainsScreenPoint(this._shoot_button.GetComponent<RectTransform>(),
                                                              point,
                                                              this._canvas.worldCamera) ||
            RectTransformUtility.RectangleContainsScreenPoint(this._split_button.GetComponent<RectTransform>(),
                                                              point,
                                                              this._canvas.worldCamera) ||       
			
            RectTransformUtility.RectangleContainsScreenPoint(this._ctrl_mode_dropdown.GetComponent<RectTransform>(),
                                                              point,
                                                              this._canvas.worldCamera) ||
            RectTransformUtility.RectangleContainsScreenPoint(this._dir_indicator_type_dropdown.GetComponent<RectTransform>(),
                                                              point,
                                                              this._canvas.worldCamera);
		*/

		return RectTransformUtility.RectangleContainsScreenPoint(this._spit_spore.GetComponent<RectTransform>(),
			point,
			this._canvas.worldCamera) ||
			RectTransformUtility.RectangleContainsScreenPoint(this._spit_ball.GetComponent<RectTransform>(),
				point,
				this._canvas.worldCamera) ||
			RectTransformUtility.RectangleContainsScreenPoint(this._obscene_unfold.GetComponent<RectTransform>(),
				point,
				this._canvas.worldCamera) ||			
			RectTransformUtility.RectangleContainsScreenPoint(this._spit_thorn.GetComponent<RectTransform>(),
				point,
				this._canvas.worldCamera) ||
			RectTransformUtility.RectangleContainsScreenPoint(this._ctrl_mode_dropdown.GetComponent<RectTransform>(),
				point,
				this._canvas.worldCamera) 

			;

		return false;
    }

	void OnSpitSporeButtonClick()
	{
		
	}
    
	void OnSpitBallButtonClick()
	{
		
	}

	void OnObsceneUnfoldButtonClick()
	{

	}

	void OnSpitThornButtonClick()
	{
		
	}

    void OnPlayButtonClick()
    {
   //     this.GetComponent<SceneAction>().SetSceneStatus(SceneStatus.SceneStatusPlaying);
    }

    void OnHomeButtonClick()
    {
     //   this.GetComponent<SceneAction>().SetSceneStatus(SceneStatus.SceneStatusWaiting);
    }    

    void OnNextButtonClick()
    {
 ///       this.GetComponent<PlayListAction>().NextBGM();
 //       this.GetComponent<BGPanelAction>().RandomBackgroundColor(true);
    }
    
    void OnPrevButtonClick()
    {
//        this.GetComponent<PlayListAction>().PrevBGM();
//        this.GetComponent<BGPanelAction>().RandomBackgroundColor(true);
    }

    void OnShootButtonClick()
    {
       // this.GetComponent<PlayListAction>().TriggerExplosion();
    }
    
    void OnSplitButtonClick()
    {
        Debug.Log("Split Button Clicked!");
    }

    void OnMenuButtonClick()
    {
        if (this._ctrl_mode_dropdown.IsActive()) {
            this._ctrl_mode_dropdown.gameObject.SetActive(false);
            this._dir_indicator_type_dropdown.gameObject.SetActive(false);
        }
        else {
            this._ctrl_mode_dropdown.gameObject.SetActive(true);
            this._dir_indicator_type_dropdown.gameObject.SetActive(true);            
        }
    }

    public void UpdateCtrlModeDropDown(int value)
    {
        this._ctrl_mode_dropdown.value = value;
    }
    
    void OnCtrlModeDropdownValueChanged(int value)
    {
		SetCtrlMode ( value );
    }

	public void SetCtrlMode( int value )
	{
		CtrlMode.SetCtrlMode(value);
		if (value == CtrlMode.CTRL_MODE_HAND) {
			CtrlMode.SetDirIndicatorType(CtrlMode.DIR_INDICATOR_LINE);
			this.UpdateDirIndicatorDropDown(CtrlMode.DIR_INDICATOR_LINE);

		}
		else if (value == CtrlMode.CTRL_MODE_WORLD_CURSOR ||
			value == CtrlMode.CTRL_MODE_SCREEN_CURSOR ||
			value == CtrlMode.CTRL_MODE_JOYSTICK) {
			CtrlMode.SetDirIndicatorType(CtrlMode.DIR_INDICATOR_ARROW);
			this.UpdateDirIndicatorDropDown(CtrlMode.DIR_INDICATOR_ARROW);
		}
		else {
			CtrlMode.SetDirIndicatorType(CtrlMode.DIR_INDICATOR_NONE);
			this.UpdateDirIndicatorDropDown(CtrlMode.DIR_INDICATOR_NONE);
		}
	}

    public void UpdateDirIndicatorDropDown(int value)
    {
        //this._dir_indicator_type_dropdown.value = value;
    }
    
    void OnDirIndicatorTypeDropdownValueChanged(int value)
    {
        CtrlMode.SetDirIndicatorType(value);
    }

	/*
    void OnKoreoEventTrackBeatCurve(KoreographyEvent koreoEvent,
                                    int sampleTime,
                                    int sampleDelta,
                                    DeltaSlice deltaSlice)
    {
        float value = koreoEvent.GetValueOfCurveAtTime(sampleTime);
        GameObject[] rhythmers = GameObject.FindGameObjectsWithTag("Rhythmer");
        for (int i=0; i<rhythmers.Length; ++i) {
            GameObject rhythmer = rhythmers[i];
            rhythmer.transform.localRotation =
                Quaternion.Euler(0, 0, value * this._rhythm_rotate_factor);
            rhythmer.transform.localScale =
                new Vector3(rhythmer.transform.localScale.x,
                            1.0f + value * this._rhythm_scale_factor,
                            rhythmer.transform.localScale.z);
            
        }

        GameObject[] shrinkers = GameObject.FindGameObjectsWithTag("Shrinker");
        for (int i=0; i<shrinkers.Length; ++i) {
            GameObject shrinker = shrinkers[i];
            shrinker.transform.localScale =
                new Vector3(1.0f + value * this._rhythm_scale_factor,
                            1.0f + value * this._rhythm_scale_factor,
                            shrinker.transform.localScale.z);
            
        }
		
    }
    */
};
