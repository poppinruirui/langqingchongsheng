using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : Photon.PunBehaviour , IPunObservable 
{
	public GameObject _MainForAlpha;
	public GameObject _shell; 
	public GameObject _shellForAlpha;
	public GameObject _thorn;
	public GameObject _thornForAlpha;
	public GameObject _trigger;
	public SpriteRenderer _srMain;
	public CircleCollider2D _Trigger;
	public CircleCollider2D _Collider;
	public CircleCollider2D _ColliderTemp;
	public GameObject _Mouth;
	public SpriteRenderer _srMouth;
	public GameObject _main; // 暂不废弃
	public Rigidbody2D _rigid;

	public SpriteRenderer _srMainForAlpha;
	public SpriteRenderer _srThornForAlpha;
	public SpriteRenderer _srShellForAlpha;

	bool m_bIsEjecting = false;

	public Vector3 vecTempSize = new Vector3();
    public Vector3 vecTempPos = new Vector3 ();
    public Vector3 vecTempPos2 = new Vector3(); 
    public Vector3 vecTempScale = new Vector3 ();
	public Vector2 vec2Temp = new Vector2 ();
    public Color cTempColor = new Color();

	Vector2 _direction = new Vector2();

	public TextMesh _text;

    public BayonetTunnel _bayoTunnel = null;

    //// MiniMap 相关
    MiniMapBall m_MyMiniMapDouble = null;
    public MiniMapBall GetMyMiniMapDouble()
    {
        return m_MyMiniMapDouble;
    }

    public void SetMiniMapDouble(MiniMapBall obj )
    {
        m_MyMiniMapDouble = obj; 
    }

    float m_fDirX = 0.0f;
	float m_fDirY = 0.0f;
 	public float _dirx
	{
		set {  
			if (_ba == null) {
				_direction.x = value;
			} else {
				_ba._direction.x = value; 
			}

		}
		get { 
			if (_ba == null) {
				return _direction.x;
			} else {
				return _ba._direction.x; 
			}
		}
	}

	public float _diry
	{
		set {  
			if (_ba == null) {
				_direction.y = value;
			} else {
				_ba._direction.y = value; 
			}

		}
		get { 
			if (_ba == null) {
				return _direction.y;
			} else {
				return _ba._direction.y; 
			}
		}
	}
   
	public ObsceneMaskShader _obsceneCircle;
	float m_fSpeed = 0.0f;
    float m_fTargetSize = 1.0f;

	public float _speed
	{
		get { 
				return m_fSpeed;
		}

		set{
			m_fSpeed = value;

		}
	}

	public void SetDirX( float val )
	{
		photonView.RPC ( "RPC_SetDirX", PhotonTargets.All, val );
	}

	[PunRPC]
	public void RPC_SetDirX( float val )
	{
		_dirx = val;
	}

	public void SetDirY( float val )
	{
		photonView.RPC ( "RPC_SetDirY", PhotonTargets.All, val );
	}

	[PunRPC]
	public void RPC_SetDirY( float val )
	{
		_diry = val;
	}

	public void SetSpeed( float fSpeed )
	{
		photonView.RPC ( "RPC_SetSpeed", PhotonTargets.All, fSpeed );
	}

	[PunRPC]
	public void RPC_SetSpeed( float fSpeed )
	{
		_speed = fSpeed;
	}

	public Player _Player = null; // 本Ball所属的Player
	public GameObject _player = null; // 这个 ball所属的player
				
	public int ownerId
	{
		get { return photonView.ownerId;  }
	}

	PhotonTransformView m_PhoTransView;

	eBallType m_eBallType = eBallType.ball_type_ball;
	public eBallType _balltype
	{
		set {  m_eBallType = value; }
		get {  return m_eBallType; }
	}

	public enum eBallType
	{
		ball_type_ball,          // 常规的球
		ball_type_thorn,         // 刺
		ball_type_bean,          // 豆子
		ball_type_spore,         // 孢子 
	};

	public BallAction _ba; // 小熊那边做的BallAction模块


	static Dictionary<int, Dictionary<int, Ball>> m_dicBalls = new Dictionary<int, Dictionary<int, Ball>> ();
	public static void AddOneBall( Ball ball )
	{
		int nOwnerId = ball.photonView.ownerId;
		int nViewId = ball.photonView.viewID;
		Dictionary<int, Ball> dic = null;
		if (!m_dicBalls.TryGetValue (nOwnerId, out dic)) {
			dic = new Dictionary<int, Ball> ();
			m_dicBalls [nOwnerId] = dic;
		}
		dic [nViewId] = ball;
	}

	static Dictionary<int, List<Ball>> m_dicDestroyedBalls = new Dictionary<int, List<Ball>> ();
	public static void RemoveOneBall( Ball ball )
	{
		int nOwnerId = ball.photonView.ownerId;
		int nViewId = ball.photonView.viewID;
		List<Ball> lst = null;
		if (!m_dicDestroyedBalls.TryGetValue (nOwnerId, out lst)) {
			lst = new List<Ball>();
			m_dicDestroyedBalls [nOwnerId] = lst;
		}
		lst.Add ( ball );
	}

	public static Ball NewOneBall( int nOwnerId )
	{
		Ball ball = null;
/*
		List<Ball> lst = null;
		if (m_dicDestroyedBalls.TryGetValue (nOwnerId, out lst)) {
			if (lst.Count > 0) {
				ball = lst [0];
				lst.RemoveAt (0);
				ball.SetDead( false );
				ball.gameObject.SetActive ( true );
				ball.transform.parent = ball._Player.transform;
				ball.ActivateOneBall ( nOwnerId );
				return ball;
			}
		}
*/
	
		Main.s_Instance.s_Params[0] = Main.eInstantiateType.Ball;
		Main.s_Instance.s_Params[1] = nOwnerId;
		Main.s_Instance.s_Params[2] = nOwnerId;
		GameObject goNewBall = Main.s_Instance.PhotonInstantiate(Main.s_Instance.m_preBall, Vector3.zero, Main.s_Instance.s_Params);
		ball = goNewBall.GetComponent<Ball> ();
		return ball;
	}

	public void ActivateOneBall( int nOwnerId )
	{
		photonView.RPC ( "RPC_ActivateOneBall", PhotonTargets.All, nOwnerId );
	}

	[PunRPC]
	public void RPC_ActivateOneBall( int nOwnerId )
	{
		Local_ActivateOneBall ( nOwnerId );
	}

	public void Local_ActivateOneBall( int nOwnerId )
	{
		Ball ball = null;
		List<Ball> lst = null;
		if (!m_dicDestroyedBalls.TryGetValue (nOwnerId, out lst)) {
			return;
		}
		if (lst.Count <= 0) {
			return;
		}
				
		ball = lst [0];
		lst.RemoveAt (0);
		ball.SetDead( false );
		ball.gameObject.SetActive ( true );
		ball.transform.parent = ball._Player.transform;
	}

	public void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{
		// poppin to do：这里明显需要优化：很多属性是不需要频繁同步的（对方客户端完全可以自己运算）。
		if(stream.isWriting) // MainPlayer
		{
			/*
			stream.SendNext(GetSize());
//			stream.SendNext(GetThornSize());

			s
			tream.SendNext ( _speed );
			stream.SendNext ( _dirx );
			stream.SendNext ( _diry );

			stream.SendNext (m_fShellShrinkCurTime);
			*/
        }

		else // Other
		{
			/*
			float fSize = (float)stream.ReceiveNext ();
			Local_SetSize (fSize);
//			Local_SetThornSize ((float)stream.ReceiveNext ());
			_speed = (float)stream.ReceiveNext ();
			_dirx = (float)stream.ReceiveNext ();
			_diry = (float)stream.ReceiveNext ();

            m_fShellShrinkCurTime = (float)stream.ReceiveNext(); // 现在是核心玩法迭代器，不用太考虑性能，除非性能已经影响了Demo效果
			*/
        }	
	}

	public void DestroyBall()
	{
		if (_relateTarget) {
			GameObject.Destroy ( _relateTarget.gameObject );
		}
        
		if (photonView.isMine) {
			//DestroyOneBall ( this.photonView.ownerId, this.photonView.viewID );
			if (_Player) {
				_Player.RemoveOneBall (this);
			}
			PhotonNetwork.Destroy( this.gameObject);
		} 
	}


	public void DestroyOneBall( int nOwnerId, int nViewId )
	{
		photonView.RPC ( "RPC_DestroyOneBall", PhotonTargets.All, nOwnerId, nViewId );
	}

	[PunRPC]
	public void RPC_DestroyOneBall( int nOwnerId, int nViewId )
	{
		//Debug.Log ( "删球：" + nOwnerId + " , " + nViewId );
		Dictionary<int, Ball> dic = null;
		if (!m_dicBalls.TryGetValue (nOwnerId, out dic)) {
			Debug.LogError ( "根据OwnerId没有查到结果" );
			return;
		}
		Ball ball = null;
		if (!dic.TryGetValue (nViewId, out ball)) {
			Debug.LogError ( "根据nViewId没有查到结果" );
			return;
		}

		ball.transform.parent = Main.s_Instance.m_goDestroyedBalls.transform;
		ball.SetDead( true );
		ball.gameObject.SetActive( false );
		RemoveOneBall ( this );
	}


	/*
	[PunRPC]
	public void RPC_DestroyBall()
	{
		if (photonView.isMine) {
			Main.s_Instance.PhotonDestroy (this.gameObject);
		} 
	}
	*/

	bool m_bDead = false;
	public void Die()
	{
        /*
		m_bDead = true;
		
		if (_relateTarget) {
			DestroyTarget ( _relateTarget );
		}

		gameObject.SetActive ( false );

		if (_Trigger) {
			_Trigger.enabled = false;
		}

		if (_Collider) {
			_Collider.enabled = false;
		}
        */
	}

	public bool IsDead()
	{
		return m_bDead;
	}

	// [poppin youhua]
	public static void DestroyTarget( SpitBallTarget target )
	{
		GameObject.Destroy ( target.gameObject );
	}

	// [poppin youhua]
	public static void DestroyBean( Bean bean )
	{
		bean.gameObject.SetActive ( false );
		GameObject.Destroy ( bean.gameObject );
		if (bean._Trigger) {
			bean._Trigger.enabled = false;
		}

        if (bean._beanType == Bean.eBeanType.scene)
        {
            Bean.ApplyGenerateOneNewBean();
        }
	}

	// [poppin youhua]
	public static void DestroyThorn( Thorn thorn )
	{
		thorn.gameObject.SetActive ( false );
		GameObject.Destroy ( thorn.gameObject );
		if (thorn._Trigger) {
			thorn._Trigger.enabled = false;
		}
		Thorn.ApplyGenerateOneNewThorn ();
	}

	SpriteRenderer m_sr;
	void Awake()
	{
		
        
        Init ();

		m_sr = this.gameObject.GetComponent<SpriteRenderer> () ;

	}

    void Start()
    {
		Main.s_Instance.s_nBallTotalNum++;
    }



    public virtual void Init()
	{
		GameObject go;

		　//this.gameObject.layer = photonView.ownerId + Main.c_nStartUserLayerId;

			if (photonView.isMine) 
			{
				//_thornForAlpha.SetActive (true);
				//_MainForAlpha.SetActive (true);
				//_shellForAlpha.SetActive (true);
			}
			else
			{
				//_thornForAlpha.SetActive( false );
				//_MainForAlpha.SetActive (false);
			    //_shellForAlpha.SetActive (false);
			}

        _srMain = this.gameObject.GetComponent<SpriteRenderer>();



		_player = GameObject.Find ("Balls/player_" + photonView.ownerId);
        if (_player)
        {
            _Player = _player.GetComponent<Player>();
            this.gameObject.transform.parent = _player.transform;

			_Player.AddOneBall ( this );

            _srMain.color = _Player._color_inner;
            if (photonView.isMine)
            {
                DarkForest.s_Instance.AddBall(this);
            }

            MiniMap.s_Instance.AddBall(this, photonView.isMine);

        }

		/*
		cTempColor =  _srMain.color;
		cTempColor.a = 0.4f;
		_srMainForAlpha.color = cTempColor;
		*/

		Vector3 vecSize = new Vector3 ();
		vecSize.x = 10;
		vecSize.y = 15;

		Transform transShell = this.gameObject.transform.Find ("Shell"); 
		if (transShell) {
			_shell = transShell.gameObject;
			SpriteRenderer sr = _shell.GetComponent<SpriteRenderer> ();
            if (_Player)
            {
                sr.color = _Player._color_ring;
            }
			/*
			cTempColor = sr.color;
			cTempColor.a = 0.4f;
			_srShellForAlpha.color = cTempColor;
			*/

		} else {
			Debug.LogError ( "transShell empty" );
			return;
		}
		_Collider = _shell.GetComponent<CircleCollider2D> ();
		_Collider.enabled = false;

		Transform transThorn = this.gameObject.transform.Find ("Thorn"); 
		if (transThorn) {
			_thorn = transThorn.gameObject;
			SpriteRenderer sr = _thorn.GetComponent<SpriteRenderer> ();
            if (_Player)
            {
                sr.color = _Player._color_poison;
            }
			transThorn.localScale = Vector3.zero;
			/*
			cTempColor =  sr.color;
			cTempColor.a = 0.4f;
			_srThornForAlpha.color = cTempColor;
			*/

		} else {
			Debug.LogError ( "transThorn empty" );
			return;
		}


		Transform transMouth = this.gameObject.transform.Find ("Mouth"); 
		if (transMouth) {
			_Mouth = transMouth.gameObject;
			_srMouth = _Mouth.GetComponent<SpriteRenderer> ();
            if (_Player)
            {
                _srMouth.color = _Player._color_inner;
            }
		} else {
			Debug.LogError ( "transMouth empty" );
			return;
		}

//		_srMain.sprite = Main.s_Instance.GetSpriteByPhotonOwnerId (photonView.ownerId);  
//		_srMouth.sprite = Main.s_Instance.GetSpriteByPhotonOwnerId (photonView.ownerId);

		_rigid = this.gameObject.GetComponent<Rigidbody2D> ();
			
		_Trigger = this.gameObject.GetComponent<CircleCollider2D> ();
		_Trigger.isTrigger = true;

		cTempColor = _srMouth.color;
		cTempColor.a = 0.0f;
		_srMouth.color = cTempColor;



		m_PhoTransView = GetComponent<PhotonTransformView> ();

		GameObject goObsceneMask = this.gameObject.transform.Find ( "ObsceneCircle" ).gameObject;
		_obsceneCircle = goObsceneMask.GetComponent<ObsceneMaskShader> ();
        if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game)
        {
            vecTempScale.x = Main.s_Instance.m_fUnfoldCircleSale;
            vecTempScale.y = Main.s_Instance.m_fUnfoldCircleSale;
            vecTempScale.z = 1.0f;   
            _obsceneCircle.transform.localScale = vecTempScale;
        }
        else
        {
            goObsceneMask.SetActive( false );
        }
		_ba = this.gameObject.GetComponent<BallAction> ();

		go = this.gameObject.transform.Find ( "Text" ).gameObject;
		_text = go.GetComponent<TextMesh> ();

		// poppin test
		if ( !photonView.isMine )
		{
			//Debug.Log ( "worinima: " + photonView.instantiationData[2] );
		}

		BallSizeChange ();

		AddOneBall (this);

		_ColliderTemp.enabled = false;
	}

	// Update is called once per frame
	public virtual void Update () 
    {
		PreUnfolding ();
		Unfolding ();
		Shell ();
		Move ();
		NeedSyncSizeLoop ();
		Splitting ();
		Ejecting ();
		ProcessColliderTemp ();
    }

	public virtual void  FixedUpdate()
    {
		
    }

	bool m_bNeedSyncSize = false;
	float m_fLastSyncSizeTime = 0f;
	public void SetSize( float val )
	{
		if (IsDead()) {
			return;
		}

		if (photonView.isMine) {
			photonView.RPC ("RPC_SetSize", PhotonTargets.All, val);   
			m_bNeedSyncSize = false;
		}
	}

	[PunRPC]
	public void RPC_SetSize( float val )
	{
		Local_SetSize (val);
	}

	public void Local_SetSize( float val )
	{
		vecTempSize.x = val;
		vecTempSize.y = val;
		vecTempSize.z = 1.0f;
		this.transform.localScale = vecTempSize;

		vecTempPos = this.transform.position;
		vecTempPos.z = -val;
		this.transform.position = vecTempPos;


		BallSizeChange ();


		ShowSize ();

	}

	
	float m_fSize2CiFang = 0.0f;
	float m_fSize3CiFang = 0.0f;
	float m_fSize2KaiGenHao = 0.0f;
	float m_fSize3KaiGenHao = 0.0f;
	void BallSizeChange()
	{
		m_fSize2CiFang = GetSize () * GetSize ();
		m_fSize3CiFang = m_fSize2CiFang * GetSize ();
		m_fSize2KaiGenHao = Mathf.Sqrt ( GetSize () );
		m_fSize3KaiGenHao = CyberTreeMath.AreaToSize ( GetSize (), 3 );
		//SetThornSize ( GetThornSize() );
		//CalculateMassBySize ( GetSize() );
		CalculateMianJiByRadius ();

	}

	void SetNeedSyncSize()
	{
		if (!photonView.isMine) {
			return;
		}

		if (!m_bNeedSyncSize) {
			m_fLastSyncSizeTime = (float)PhotonNetwork.time;
		}
		m_bNeedSyncSize = true;
	}

	void NeedSyncSizeLoop()
	{
		if (!photonView.isMine) {
			return;
		}

		if (!m_bNeedSyncSize) {
			return;
		}

		float fDelta = (float)PhotonNetwork.time - m_fLastSyncSizeTime;
		if ( fDelta > 1.0f)
		{

			SetSize( GetSize() );
			m_bNeedSyncSize = false;
		}

	}

	public float GetArea( int nShit )
	{
		if (nShit == 2) {
			return GetSize2CiFang ();
		} else if (nShit == 3) {
			return GetSize3CiFang ();
		}
		return 0.0f;
	}

	public float GetSize2CiFang()
	{
		return m_fSize2CiFang;
	}

	public float GetSize3CiFang()
	{
		return m_fSize3CiFang;
	}

	public float GetSize2KaiGenHao ()
	{
		return m_fSize2KaiGenHao;
	}

	public float GetSize3KaiGenHao ()
	{
		return m_fSize3KaiGenHao;
	}


	float m_fMianJi = 0.0f; // 这个是真正数学意义上的面积，不是咱们游戏自定义的一些数据结构
	float m_fRadius = 0.0f;
	void CalculateMianJiByRadius ()
	{
		m_fRadius = _srMain.bounds.size.x / 2.0f;
		m_fMianJi =  Mathf.PI *  m_fRadius * m_fRadius;
	}

	public float GetRadius()
	{
		return m_fRadius;
	}

	public float GetMianJi()
	{
		return m_fMianJi;
	}

	void CalculateMassBySize( float fSize )
	{
		if (_rigid == null) {
			_rigid = this.gameObject.GetComponent<Rigidbody2D> ();
		}
		_rigid.mass = fSize * fSize * fSize;
	}

	public float GetSize()
	{
		return this.transform.localScale.x;
	}

	float m_fColliderTempTime = 0f;
	void ProcessColliderTemp ()
	{
		if ( _ColliderTemp == null || _ColliderTemp.enabled == false )
		{
			return;
		}
		m_fColliderTempTime -= Time.deltaTime;
		if (m_fColliderTempTime <= 0f) {
			_ColliderTemp.enabled = false;
		}

	}

	public bool IsEjecting()
	{
		return m_bIsEjecting;
	}

	float m_fEjectInitialSpeed = 0.0f;
	float m_fEjectSpeed = 0.0f;
	float m_fEjectAccelerate = 0.0f;
	float m_fStartPosX = 0.0f;
	float m_fStartPosY = 0.0f;
	float m_fTimeClapse = 0.0f;
	float m_fEjectingTotalTime = 0f;
	public void BeginEject( float fInitialSpeed, float fAccelerate, float fEjectingTotalTime, bool bRealTimeFollowMouse = false, bool bUseColliderTemp = false  )
	{
		photonView.RPC ( "RPC_BeginEject", PhotonTargets.All, fInitialSpeed, fAccelerate, fEjectingTotalTime, bRealTimeFollowMouse, bUseColliderTemp );
	}

	[PunRPC]
	public void RPC_BeginEject( float fInitialSpeed, float fAccelerate, float fEjectingTotalTime, bool bRealTimeFollowMouse = false, bool bUseColliderTemp = false   )
	{
		Local_BeginEject ( fInitialSpeed, fAccelerate, fEjectingTotalTime, bRealTimeFollowMouse, bUseColliderTemp );
	}

	bool m_bRealTimeFollowMouse = false;
	bool m_bUseColliderTemp = false;
	public void Local_BeginEject( float fInitialSpeed, float fAccelerate, float fEjectingTotalTime, bool bRealTimeFollowMouse = false, bool bUseColliderTemp = false  )
	{
		m_bUseColliderTemp = bUseColliderTemp;
		m_fEjectingTotalTime = fEjectingTotalTime;
		m_bRealTimeFollowMouse = bRealTimeFollowMouse;
		m_bIsEjecting = true;
		m_fEjectInitialSpeed = fInitialSpeed;
		m_fEjectSpeed = fInitialSpeed;
		m_fEjectAccelerate = fAccelerate;
		if (_Collider) {
			_Collider.enabled = false;
		}

		m_fStartPosX = this.gameObject.transform.position.x;
		m_fStartPosY = this.gameObject.transform.position.y;
		m_fTimeClapse = 0.0f;
	}

	public bool CheckNeedCollider()
	{
		if (!m_bShell) {
			return false;
		}

		if (Main.s_Instance.m_fShellShrinkTotalTime == 0f) {
			return false;
		}

		return true;
	}

	public void EndEject()
	{
		m_bIsEjecting = false;
		BeginShell ();

		if (m_bUseColliderTemp) {
			_ColliderTemp.enabled = true;
			m_fColliderTempTime = 0.5f;
		}

		if (m_bShell) {
			if (_Collider) {
				if (CheckNeedCollider()) {
					if (Main.s_Instance.m_fShellShrinkTotalTime > 0f) {
						_Collider.enabled = true; 
					}
				}
			}
		}

		if (_relateTarget) {
			DestroyTarget ( _relateTarget );
		}
	}



	void Ejecting()
	{
		if (!m_bIsEjecting) {
			return;
		}

		if (m_fTimeClapse >= m_fEjectingTotalTime/*Main.s_Instance.m_fSpitRunTime*/) {
			EndEject ();
			return;
		}

		if (m_bRealTimeFollowMouse ) {
				vec2Temp = _Player.GetWorldCursorPos ()- this.GetPos ();
		

					vec2Temp.Normalize ();
					_dirx = vec2Temp.x;
					_diry = vec2Temp.y;
		}

		/*
		m_fTimeClapse += Time.fixedDeltaTime;
		vecTempPos.x = m_fStartPosX;
		vecTempPos.y = m_fStartPosY;
		vecTempPos.z = this.gameObject.transform.position.z;
		float S = CyberTreeMath.CalculateDistance( m_fEjectInitialSpeed, m_fEjectAccelerate, m_fTimeClapse ); //m_fEjectSpeed * m_fTimeClapse + 0.5f * m_fAccelerate * m_fTimeClapse * m_fTimeClapse;
		vecTempPos.x += S * _dirx;
		vecTempPos.y += S * _diry;
		*/
		m_fTimeClapse += Time.deltaTime;
		vecTempPos = GetPos ();
		float fDeltaX = Time.deltaTime * m_fEjectInitialSpeed * _dirx;
		float fDeltaY = Time.deltaTime * m_fEjectInitialSpeed * _diry;
		float fExpectedX = vecTempPos.x + fDeltaX;
		float fExpectedY = vecTempPos.y + fDeltaY;
		if (fDeltaX > 0) {
			if (MapEditor.s_Instance.CheckIfExceedWorldBorder (fExpectedX, 0)) {
				fExpectedX = vecTempPos.x;
			}
		} else if (fDeltaX < 0) {
			if (MapEditor.s_Instance.CheckIfExceedWorldBorder (fExpectedX, 1)) {
				fExpectedX = vecTempPos.x;
			}
		} 
		vecTempPos.x = fExpectedX;

		if (fDeltaY > 0) {
			if (MapEditor.s_Instance.CheckIfExceedWorldBorder (fExpectedY, 2)) {
				fExpectedY = vecTempPos.y;
			}
		} else if (fDeltaY < 0) {
			if (MapEditor.s_Instance.CheckIfExceedWorldBorder (fExpectedY, 3)) {
				fExpectedY = vecTempPos.y;
			}
		} 
		vecTempPos.y = fExpectedY;

		this.transform.position = vecTempPos;


		m_fEjectInitialSpeed += Time.deltaTime * m_fEjectAccelerate;
		if (m_fEjectInitialSpeed <= 0f) {
			EndEject();
		}

	//	SetPos ( vecTempPos );

        bool bRet = CyberTreeMath.ClampBallPosWithinSCreen(vecTempPos, ref vecTempPos2); 
		this.gameObject.transform.position = vecTempPos2;
        if ( bRet )
        {
            EndEject();
        }
	}

	int m_nUnfoldStatus = 0;
	public bool CheckIfCanUnfold()
	{
		if (m_nUnfoldStatus > 0) {
			return false;
		}

		return true;
	}

	public void BeginPreUnfold()
	{
		//_obsceneCircle.Begin ( 2.0f );
		//m_nUnfoldStatus = 1;
		photonView.RPC ( "RPC_BeginPreUnfold", PhotonTargets.All );
	}

	[PunRPC]
	public void RPC_BeginPreUnfold()
	{
		vecTempScale.x = Main.s_Instance.m_fUnfoldSale;
		vecTempScale.y = Main.s_Instance.m_fUnfoldSale;
		vecTempScale.z = 1f;
		//_obsceneCircle.transform.localScale = vecTempScale;

		_obsceneCircle.Begin ( 2.0f );
		m_nUnfoldStatus = 1;
	}

	void PreUnfolding()
	{
		if (m_nUnfoldStatus != 1) {
			return;
		}

		if (_obsceneCircle.IsEnd ()) {
			BeginUnfold ();
		}
	}

	float m_fUnfoldLeftTime = 0.0f;
	void BeginUnfold()
	{
		photonView.RPC ( "RPC_BeginUnfold", PhotonTargets.All );

		//_Mouth.transform.localScale = vecTempSize;
		//_Trigger.radius = 1.0f;

		/*
		cTempColor = _srMouth.color;
		cTempColor.a = 1.0f;
		_srMouth.color = cTempColor;
		*/
	}

	float m_fRealSize = 0.0f;
	public bool IsUnfolding()
	{
		return ( m_nUnfoldStatus == 2 );
	}

	[PunRPC]
	void RPC_BeginUnfold()
	{
		m_nUnfoldStatus = 2;
		m_fUnfoldLeftTime = Main.s_Instance.m_fMainTriggerUnfoldTime;
		vecTempSize.x = Main.s_Instance.m_fUnfoldSale;
		vecTempSize.y = Main.s_Instance.m_fUnfoldSale;
		vecTempSize.z = 1.0f;
		_Mouth.transform.localScale = vecTempSize;
		_Trigger.radius = Main.s_Instance.m_fTriggerRadius * Main.s_Instance.m_fUnfoldSale;
		cTempColor = _srMouth.color;
		cTempColor.a = 1.0f;
		_srMouth.color = cTempColor;
	
	}


	void Unfolding()
	{
		if (m_nUnfoldStatus != 2) {
			return;
		}

		m_fUnfoldLeftTime -= Time.deltaTime;
		if (m_fUnfoldLeftTime <= 0.0f) {
			EndUnfold ();
		}

	}

	void EndUnfold()
	{

		m_nUnfoldStatus = 0;
		vecTempSize = Vector3.one;
		_Mouth.transform.localScale = vecTempSize;
		cTempColor = _srMouth.color;
		cTempColor.a = 0.0f;
		_srMouth.color = cTempColor;
		_Trigger.radius = Main.s_Instance.m_fTriggerRadius;
	}


	public void ProcessNail( Nail nail )
	{
		if (this.IsEjecting ()) {
			this.EndEject ();
		}

		this.gameObject.transform.parent = Main.s_Instance.m_goBallsBeNailed.transform;
		_Collider.enabled = false;
	}

	public void ProcessPk( Ball ballOpponent )
	{
		if ( this.photonView && ballOpponent.photonView )
		{
			if (this.photonView.ownerId == ballOpponent.photonView.ownerId) {
				if (this.IsEjecting () || ballOpponent.IsEjecting ()) {
					return;
				}
			}
		}

		if ( this.photonView != null && ballOpponent.photonView != null && this.photonView.ownerId != ballOpponent.photonView.ownerId) {
			Physics2D.IgnoreCollision (this._Collider, ballOpponent._Collider);
		}

		if (IsDead () || ballOpponent.IsDead ()) {
			return;
		}

		if ( ( this._Player != null && this._Player.IsProtect () ) || ( ballOpponent._Player != null && ballOpponent._Player.IsProtect () )) {
			return;
		}

		int nRet = Ball.WhoIsBig (this, ballOpponent);
		if (nRet == 1) {
			this.PreEat (ballOpponent);
		} else if (nRet == -1) {
			ballOpponent.PreEat (this);
		}
	}

	public void ProcessTattooFood( TattooFood food )
	{
		if (!photonView.isMine) {
			return;
		}

		if (food.IsDestroyed ()) {
			return;
		}

		if (CheckIfTotallyCover( _Trigger, food._Trigger ) ) {
			EatTattooFood ( food );
		}
	}

	public void DoEatBeiShuDouZi ( TattooFood food )
	{
		float fCurSize = GetSize ();
		float fCurArea = CyberTreeMath.SizeToArea ( fCurSize, Main.s_Instance.m_nShit );
		float fNewArea = fCurArea * (float)food.GetParam( 0 );
		float fAreaToAdd = fNewArea - fCurArea;
		AddArea ( fAreaToAdd );
		//float fNewSize = CyberTreeMath.AreaToSize ( fCurArea, Main.s_Instance.m_nShit );
		//SetSize ( fNewSize );

		Main.s_Instance.m_MainPlayer.RemoveOnetattoo( food );
	}

	public void EatBeiShuDouZi ( TattooFood food )
	{
		switch (food._subtype) {
		case 0: // 单个球加体积
			{
				/*
				float fCurSize = GetSize ();
				float fCurArea = CyberTreeMath.SizeToArea ( fCurSize, Main.s_Instance.m_nShit );
				fCurArea *= (float)food.GetParam( 0 );
				float fNewSize = CyberTreeMath.AreaToSize ( fCurArea, Main.s_Instance.m_nShit );
				SetSize ( fNewSize );
				Main.s_Instance.m_MainPlayer.RemoveOnetattoo( food );
				*/
				DoEatBeiShuDouZi ( food );
			}
			break;
		case 1: // 整个队伍加体积
			{
				List<Ball> lst = Main.s_Instance.m_MainPlayer.GetBallList ();
				for (int i = lst.Count - 1; i >= 0; i--) {
					Ball ball = lst [i];
					if (ball == null) {
						continue;
					}
					ball.DoEatBeiShuDouZi ( food );
				}
			}
			break;
		}
	}

	public void EatTattooFood( TattooFood food )
	{
		switch (food._type) {
		case TattooFood.eTattooFoodType.TattooFood_BeiShuDouZi:
			{
				EatBeiShuDouZi ( food );
			}
			break;
		}
	}

	void PreEat( Ball ballOpponent )
	{
		if (this._balltype != eBallType.ball_type_ball) {
			return;
		}

		if (Launcher.m_eGameMode == Launcher.eGameMode.kuangzhang ||
			(ballOpponent._balltype == eBallType.ball_type_bean || ballOpponent._balltype == eBallType.ball_type_thorn || ballOpponent._balltype == eBallType.ball_type_spore)
		
		) {
			if (!CheckIfTotallyCover ( this._Trigger, ballOpponent._Trigger )) {
				return;
			}
		} else if (Launcher.m_eGameMode == Launcher.eGameMode.yaqiu) {
			if (!CheckIfPartiallyCover (ballOpponent)) {
				return;
			}
		} else if (Launcher.m_eGameMode == Launcher.eGameMode.xiqiu) {
			DrawBall ( ballOpponent );
			return;
		}
		this.Eat (ballOpponent);
	}

	public void DoDraw()
	{
		float fCurArea = GetArea (Main.s_Instance.m_nShit);
		float fAreaDelta = m_fPreDrawDeltaTime * MapEditor.s_Instance.m_fXiQiuSuDu;
		m_fPreDrawDeltaTime = 0.0f;
		float fNewArea = fCurArea + fAreaDelta;
		if (fNewArea <= 0.0f) {
			SetDead ( true );
			DestroyBall ();
			return;
		}
		Local_SetSize ( CyberTreeMath.AreaToSize ( fNewArea, Main.s_Instance.m_nShit ) ); 
		SetNeedSyncSize ();
	}

	void DrawBall(  Ball ballOpponent )
	{
		if (this.photonView.ownerId == ballOpponent.photonView.ownerId) {
			if (this.m_bShell && ballOpponent.m_bShell) {
				return;
			}
		}

		if (ballOpponent.IsDead() ) {
			return;
		}

		// 因为被吸的是绝对数量而不是自身体积的百分比，因此非常好计算。
		this.PreDraw(Time.deltaTime);
		ballOpponent.PreDraw ( -Time.deltaTime );
	}

	float m_fPreDrawDeltaTime = 0.0f;
	public void PreDraw( float fDeltaTime )
	{
		m_fPreDrawDeltaTime += fDeltaTime;

	}

	public void AddArea( float fAreaToAdd )
	{

			float fCurArea = CyberTreeMath.SizeToArea ( GetSize(), Main.s_Instance.m_nShit );
			float fNewArea = fCurArea + fAreaToAdd;
			float fNewSize = CyberTreeMath.AreaToSize ( fNewArea, Main.s_Instance.m_nShit );
			SetSize ( fNewSize );
	}

	public void AddSize( float fSizeToAdd )
	{
		if (IsUnfolding ()) {

		} else {
			SetSize (CyberTreeMath.CalculateNewSize ( GetSize(), fSizeToAdd, Main.s_Instance.m_nShit));
		}
	}

	void Eat( Ball ballOpponent )
	{
		if (ballOpponent._balltype == eBallType.ball_type_bean) {
			EatBean ( (Bean)ballOpponent );
			return;
		}

		if (ballOpponent._balltype == eBallType.ball_type_spore) {
			EatSpore ( (Spore)ballOpponent );
			return;
		}

        // “刺”不再继承于“球”，因为最新的设计是把刺归于“场景物件”类，而不是“球”类。
        // 因此“吃刺”的环节也独立出来，不走“吃球”流程
        /*
		if (ballOpponent._balltype == eBallType.ball_type_thorn) {
			EatThorn ( (Thorn)ballOpponent );
			return;
		}
        */

		EatBall ( ballOpponent );
	}

	void EatBean( Bean bean )
	{
  		//SetSize ( Main.s_Instance.m_fBeanSize + GetSize() );
		float fCurSize = GetSize();
		float fBeanSize = Main.s_Instance.m_fBeanSize;
		float fNewSize = 0.0f;

		//fNewSize = CyberTreeMath.CalculateNewSize (fCurSize, fBeanSize, Main.s_Instance.m_nShit  );
		//SetSize ( fNewSize );
		float fAreaToAdd = CyberTreeMath.SizeToArea( fBeanSize, Main.s_Instance.m_nShit );
		AddArea( fAreaToAdd );

		DestroyBean ( bean );
	}

    Thorn.ColorThornParam m_paramColorThorn = new Thorn.ColorThornParam();
    public void EatThorn( Thorn thorn )
	{
		if (GetSize () < GetMinExplodeSize() || GetSize ()  < Main.s_Instance.m_fMinDiameterToEatThorn ) {
			return;
		}

		float fCurThornSize = GetThornSize ();
        int nColorIdx = thorn.GetColorIdx();
        m_paramColorThorn = MapEditor.GetColorThornParamByIdx(nColorIdx);
        float fAddedThornSize = Main.s_Instance.m_fThornSize;
        if (nColorIdx > 0)
        {
            fAddedThornSize = m_paramColorThorn.thorn_size;
        }

		SetThornSize ( CyberTreeMath.CalculateNewSize ( fCurThornSize, fAddedThornSize, Main.s_Instance.m_nShit ));
		DestroyThorn ( thorn );

//		if (Main.s_Instance.GetCurAvailableBallCount () <= 0) {
//			return;
//		}

		// 吃刺也会增加球本身的总体积。但如果当前队伍已经不能爆刺了，则吃刺不增加球的体积
		float fCurBallSize = GetSize();
		float fAddedBallSize = Main.s_Instance.m_fThornContributeToBallSize;
        if (nColorIdx > 0)
        {
            fAddedBallSize = m_paramColorThorn.ball_size;
        }
        SetSize( CyberTreeMath.CalculateNewSize ( fCurBallSize, fAddedBallSize, Main.s_Instance.m_nShit ) );
	}

	public void SetThornSize( float fThornSize )
	{
		if (!photonView.isMine) {
			return;
		}

		photonView.RPC ( "RPC_SetThornSize", PhotonTargets.All, fThornSize );
	}

	[PunRPC]
	public void RPC_SetThornSize( float fThornSize )
	{
		Local_SetThornSize (fThornSize);
	}

	float m_fThornSize = 0.0f;
	public void Local_SetThornSize( float fThornSize )
	{
		if (_thorn == null) {
			return;
		}

		m_fThornSize = fThornSize;
		float fBallSize = GetSize ();
		if (m_fThornSize > fBallSize) {
			m_fThornSize = fBallSize;
		}
		float fThornRealScaleSize = m_fThornSize / fBallSize;
		vecTempScale.x = fThornRealScaleSize;
		vecTempScale.y = fThornRealScaleSize;
		vecTempScale.z = 1.0f;
		_thorn.transform.localScale = vecTempScale;
		//_thornForAlpha.transform.localScale = vecTempScale;
		if (CheckIfExplode ()) {
			Explode ();
		}
	}

	public float GetThornSize()
	{
		return m_fThornSize;
	}

	bool  CheckIfExplode()
	{
		float fBallSize = GetSize ();
		if (m_fThornSize < fBallSize ) {
			return false;
		}

		if (fBallSize < 2.0f) {
			return false;
		}

		return true;
	}

	public static Vector2[] s_aryBallExplodeDirection = { 
		new Vector2( 0.0f, 1.0f ),
		new Vector2( 0.707f, 0.707f ),
		new Vector2( 1.0f, 0.0f ),
		new Vector2( 0.707f, -0.707f ),
		new Vector2( 0, -1.0f ),
		new Vector2( -0.707f, -0.707f ),
		new Vector2( -1.0f, 0.0f ),
		new Vector2( -0.707f, 0.707f ),

	};

	/*
	 * 爆裂的规则：
	 * 1、准备爆裂的母球，半径最小为2。不然没法爆，因为爆出的小球最小半径得是1（一个孢子的尺寸），并且爆了之后母球剩下的半径至少也得是1.
	 * 2、母球分一半的半径出来爆。
	 * 3、能爆成8等分尽量8等分（确保爆出来的每个小球的半径大于等于1）；不够8等分则能爆几个算几个。
	 * */
	public float GetMinExplodeSize()
	{
		return 2.0f * Main.BALL_MIN_SIZE;
	}

    float m_fShitExplodeTime = 0.0f;
	public void Explode()
	{
		if (!photonView.isMine) {
			return;
		}

		float fBallSize = GetSize ();
		if (fBallSize < GetMinExplodeSize() ) {
			//Debug.LogError ("有Bug, 母球直径小于2肿么爆");
			return;
		}

		/*
		float fChildTotalSize = fBallSize / 2.0f; // 母球分一半的半径出来爆
		float fChildSize = 0.0f;
		int nExpectNum = 8; // 预期分为8份，如果不够8份，则能分几个算几个
		int nRealNum = 0;
		for ( nRealNum = nExpectNum; nRealNum >= 1; nRealNum--) {
			fChildSize = fChildTotalSize / nRealNum;
			if (fChildSize >= Main.BALL_MIN_SIZE) {
				break;
			}
		}
		*/

		int nAvailableNum = (int)Main.s_Instance.m_fMaxBallNumPerPlayer - Main.s_Instance.GetMainPlayerCurBallCount ();
		if (nAvailableNum <= 0) {
			return;
		}

		SetThornSize (0.0f);
		float fMotherCurArea = CyberTreeMath.SizeToArea (fBallSize, Main.s_Instance.m_nShit);
		float fChildsArea = fMotherCurArea / 2.0f; // 母球分一半的面积出来爆
		int nExpectNum =  (int)Main.s_Instance.m_fExpectExplodeNum;; // 预期分为8份，如果不够8份，则能分几个算几个
		int nRealNum = 0;
		float fChildSize = 0.0f;
		if (nExpectNum > nAvailableNum) {
			nExpectNum = nAvailableNum;
		}
		for (nRealNum = nExpectNum; nRealNum >= 1; nRealNum--) {
			fChildSize = CyberTreeMath.AreaToSize (fChildsArea / nRealNum, Main.s_Instance.m_nShit);
			if (fChildSize >= Main.BALL_MIN_SIZE) {
				break;
			}
		}

		if (nRealNum == 0) {
			Debug.LogError ("有Bug, 没计算出合适的分球个数");
			return;
		}

		// 生成小球
		float fRadiusMother = GetBoundsSize() / 2.0f;
		vecTempPos = this.gameObject.transform.position;
		for (int i = 0; i < nRealNum; i++) {
            /*
            Main.s_Instance.s_Params [0] = Main.eInstantiateType.Ball;
			Main.s_Instance.s_Params [1] = 222;
			GameObject goNewBall = Main.s_Instance.PhotonInstantiate( Main.s_Instance.m_preBall, vecTempPos, Main.s_Instance.s_Params ) ;//GameObject.Instantiate ( Main.s_Instance.m_preBall );
			Ball new_ball = goNewBall.GetComponent<Ball> ();
			goNewBall.transform.position = vecTempPos;
			new_ball.SetSize ( fChildSize );
			float fRadiusChild = new_ball.GetBoundsSize ();
			new_ball.gameObject.transform.parent = _player.transform;
			Vector2 vecDire = s_aryBallExplodeDirection [i];
			this.CalculateNewBallBornPosAndRunDire ( new_ball, vecDire.x, vecDire.y );
			new_ball._dirx = vecDire.x;
			new_ball._diry = vecDire.y;
			new_ball.BeginShell ();
			CalculateSpitBallRunParams ( fRadiusMother, fRadiusChild, Main.s_Instance.m_fExplodeRunDistanceMultiple, ref m_fExplodeInitSpeed, ref m_fExplodeInitAccelerate );
			new_ball.BeginEject ( m_fExplodeInitSpeed, m_fExplodeInitAccelerate );
            */
            GenerateNewBall(fChildSize, fRadiusMother, i);

        }



		// 母球剩余面积
		float fMotherLeftArea = fMotherCurArea - fChildsArea;
		float fMotherLeftSize = CyberTreeMath.AreaToSize ( fMotherLeftArea, Main.s_Instance.m_nShit );
		SetSize( fMotherLeftSize );
		BeginShell ();


	} // end explode()

    void GenerateNewBall( float fChildSize, float fRadiusMother, int idx)
    {
        Main.s_Instance.s_Params[0] = Main.eInstantiateType.Ball;
        Main.s_Instance.s_Params[1] = 222;
		Ball new_ball = Ball.NewOneBall( this.photonView.ownerId );//Main.s_Instance.PhotonInstantiate(Main.s_Instance.m_preBall, vecTempPos, Main.s_Instance.s_Params);//GameObject.Instantiate ( Main.s_Instance.m_preBall );
		//new_ball.transform.position = vecTempPos;
        new_ball.SetSize(fChildSize);
        float fRadiusChild = new_ball.GetBoundsSize();
        new_ball.gameObject.transform.parent = _player.transform;
        Vector2 vecDire = s_aryBallExplodeDirection[idx];
        this.CalculateNewBallBornPosAndRunDire(new_ball, vecDire.x, vecDire.y);
        new_ball._dirx = vecDire.x;
        new_ball._diry = vecDire.y;
        new_ball.BeginShell();
        CalculateSpitBallRunParams(fRadiusMother, fRadiusChild, Main.s_Instance.m_fExplodeRunDistanceMultiple, ref m_fExplodeInitSpeed, ref m_fExplodeInitAccelerate);
		new_ball.BeginEject(m_fExplodeInitSpeed, m_fExplodeInitAccelerate, Main.s_Instance.m_fSpitRunTime);

    }

    void EatSpore( Spore spore )
	{
		float fChildSize = Main.BALL_MIN_SIZE;
		float fMotherSize = GetSize ();
		//SetSize ( CyberTreeMath.CalculateNewSize( fMotherSize, fChildSize, Main.s_Instance.m_nShit ) );
		float fAreaToAdd = CyberTreeMath.SizeToArea( fChildSize, Main.s_Instance.m_nShit );
		AddArea ( fAreaToAdd );
		spore.DestroyBall();
	}

    public struct EatNode
    {
        public Ball eatenBall;
        public float eatenSize; 
    };

	List<EatNode> m_lstEaten = new List<EatNode>();

    void AddEatenNode(EatNode node)
    {
        m_lstEaten.Add( node );
    }

	void ProcessEatenNode()
    {
        for ( int i = m_lstEaten.Count - 1; i >= 0; i--  )
        {
		    EatNode node = m_lstEaten[i];

            if (node.eatenBall == null)
            {
				float fMotherSize = GetSize();
                float fNewSize = CyberTreeMath.CalculateNewSize(fMotherSize, node.eatenSize, Main.s_Instance.m_nShit);
				SetSize(fNewSize);
                m_lstEaten.Remove( node );
            }
        }
    }

	bool CheckIfInEatenList( Ball the_eater, Ball ballOpponent)
    {
        for (int i = m_lstEaten.Count - 1; i >= 0; i--)
        {
            EatNode node = m_lstEaten[i];
			if ( node.eatenBall == the_eater ||  node.eatenBall == ballOpponent)
            {
                return true;
            }
        }

        return false;
    }


	public void SetDead( bool val )
	{
		m_bDead = val;

		if (m_bDead) {
			_Trigger.enabled = false;
			_Collider.enabled = false;
		} else {
			_Trigger.enabled = true;
			_Collider.enabled = true;
		}
	}

    public void EatBall( Ball ballOpponent )
	{
		if (!photonView.isMine) {
			return;
		}

		if (ballOpponent.IsDead ()) {
			return;
		}

		float fChildSize = ballOpponent.GetSize ();
		float fMotherSize = GetSize ();

        //EatNode node = new EatNode();
        //node.eatenSize = fChildSize;
        //node.eatenBall = ballOpponent;
        //AddEatenNode(node);
        //float fNewSize = CyberTreeMath.CalculateNewSize(fMotherSize, fChildSize, Main.s_Instance.m_nShit);
        //SetSize(fNewSize);

        // 如果被吃的球身上有刺，则刺也被吃过来 poppin test
        //float fCurThornSize = GetThornSize ();
		//float fAddedThornSize = ballOpponent.GetThornSize ();
		//SetThornSize ( CyberTreeMath.CalculateNewSize( fCurThornSize, fAddedThornSize, Main.s_Instance.m_nShit ) );

		//float fNewSize = CyberTreeMath.CalculateNewSize(fMotherSize, fChildSize, Main.s_Instance.m_nShit);
		//SetSize (fNewSize);
		float fAreaToAdd = CyberTreeMath.SizeToArea( fChildSize, Main.s_Instance.m_nShit );
		AddArea ( fAreaToAdd );

		ballOpponent.SetDead ( true );
		/*
		if (ballOpponent.photonView.isMine) {
			ballOpponent.DestroyBall ();
		}
		*/

		// 流程越来越统一了。逐步掌握了帧同步的真谛。
		ballOpponent.Terminate ();
	}

	public void Terminate()
	{
		photonView.RPC ( "RPC_Terminate", PhotonTargets.All );
	}

	[PunRPC]
	public void RPC_Terminate()
	{
		if (!photonView.isMine) {
			return;
		}

		this.DestroyBall ();
	}

	public static int WhoIsBig( Ball ball1, Ball ball2 )
	{
		if (ball1._Trigger.bounds.size.x == ball2._Trigger.bounds.size.x) {
			return 0;
		} else if (ball1._Trigger.bounds.size.x > ball2._Trigger.bounds.size.x) {
			return 1;
		} else {
			return -1;
		}
	}

	bool CheckIfTotallyCover( CircleCollider2D circle1, CircleCollider2D circle2  )
	{
		float r1 = circle1.bounds.size.x / 2.0f;
		float r2 = circle2.bounds.size.x / 2.0f;

		float deltaX = circle1.bounds.center.x - circle2.bounds.center.x;
		float deltaY = circle1.bounds.center.y - circle2.bounds.center.y;
		float dis = Mathf.Sqrt ( deltaX * deltaX + deltaY * deltaY );
		if (dis <= Mathf.Abs (r1 - r2)) {
			return true;
		}

		return false;
	}

	bool CheckIfPartiallyCover( Ball ballOpponent )
	{
		if (m_fMianJi < MapEditor.s_Instance.m_fYaQiuTiaoJian * ballOpponent.GetMianJi ()) {
			return false;
		}

		CircleCollider2D c1 = this._Trigger;
		CircleCollider2D c2 = ballOpponent._Trigger;

		float  d;
		float  s,s1,s2,s3,angle1,angle2;
		float r1 = m_fRadius;
		float r2 = ballOpponent.GetRadius ();

		d = Mathf.Sqrt((c1.bounds.center.x-c2.bounds.center.x)*(c1.bounds.center.x-c2.bounds.center.x)+(c1.bounds.center.y-c2.bounds.center.y)*(c1.bounds.center.y-c2.bounds.center.y));
		if (d >= (r1 + r2)) {//两圆相离
			return false;
		}
		if ((r1 - r2) >= d) {//两圆内含,c1大
			return true;
		}

		angle1 = Mathf.Acos((r1*r1+d*d-r2*r2)/(2*r1*d));
		angle2 = Mathf.Acos((r2*r2+d*d-r1*r1)/(2*r2*d));

			s1=angle1*r1*r1;
		    s2=angle2*r2*r2;
			s3=r1*d*Mathf.Sin(angle1);
			s=s1+s2-s3;

		float fCurPercent = s / ballOpponent.GetMianJi();
		if (fCurPercent >= MapEditor.s_Instance.m_fYaQiuBaiFenBi ) {
			return true;
		}

		return false;
	}

	public bool CheckIfCanForceSpit()
	{
		return true;
	}

	public bool CheckIfCanSpitBall( float fMotherSize, float fChildSize, ref float fMotherLeftSize  )
	{
		fMotherLeftSize= CyberTreeMath.CalculateNewSize ( fMotherSize, fChildSize, Main.s_Instance.m_nShit, -1 );
		return fMotherLeftSize > Main.BALL_MIN_SIZE;
	}

	// 

	public Spore SpitSpore()
	{
		float fMotherSize = GetSize();
		float fMotherLeftSize = 0.0f;
		if (!CheckIfCanSpitBall (fMotherSize, Main.BALL_MIN_SIZE, ref fMotherLeftSize)) {
			return null;
		}
		Main.s_Instance.s_Params [0] = Main.eInstantiateType.Spore;
		Main.s_Instance.s_Params [1] = 444;
		GameObject goSpore = Main.s_Instance.PhotonInstantiate (Main.s_Instance.m_preSpore, Vector3.zero, Main.s_Instance.s_Params );//GameObject.Instantiate( Main.s_Instance.m_preSpore );
		Spore spore = goSpore.GetComponent<Spore>();
		goSpore.transform.parent = Main.s_Instance.m_goSpores.transform;
		spore._srMain.color = this._srMain.color;
		this.CalculateNewBallBornPosAndRunDire ( spore, this._dirx, this._diry );

		SetSize ( fMotherLeftSize );
		spore.BeginEject ( Main.s_Instance.m_fSpitSporeInitSpeed, Main.s_Instance.m_fSpitSporeAccelerate, Main.s_Instance.m_fSpitRunTime );

		return spore;
	}


	public void SpitNail()
	{
		photonView.RPC ( "RPC_SpitNail", PhotonTargets.AllBuffered );
	}

	[PunRPC]
	public void RPC_SpitNail()
	{
		float fMotherSize = GetSize();
		float fMotherLeftSize = 0.0f;
		if (!CheckIfCanSpitBall (fMotherSize, Main.BALL_MIN_SIZE, ref fMotherLeftSize)) {
			return;
		}

		Nail nail = null;

		GameObject goNail = GameObject.Instantiate (Main.s_Instance.m_preNail); // “钉子”是网络版的哦
		goNail.transform.parent = Main.s_Instance.m_goNails.transform;
		nail = goNail.GetComponent<Nail> ();
		float fChildPosX = 0.0f, fChildPosY = 0.0f;
		float fMotherPosX = 0.0f, fMotherPosY = 0.0f;
		float fMotherDirX = 0.0f, fMotherDirY = 0.0f;
		this.GetPos ( ref fMotherPosX, ref fMotherPosY );
		this.GetDir ( ref fMotherDirX, ref fMotherDirY );
		float fMotherRadius = this._srMain.bounds.size.x / 2.0f;
		float fChildRadius = nail._srMain.bounds.size.x / 2.0f;
		CyberTreeMath.CalculateSpittedObjBornPos (fMotherPosX, fMotherPosY, fMotherRadius, fMotherDirX, fMotherDirY, fChildRadius, ref fChildPosX, ref fChildPosY);
		nail.SetPos ( fChildPosX, fChildPosY );
		nail.SetDir ( fMotherDirX, fMotherDirY );
		float fRunSpeed = 0.0f;
		float fRunAccelerate = 0.0f;
		CyberTreeMath.CalculateSpittedObjRunParams ( Main.s_Instance.m_fSpittedObjrunRunDistance, Main.s_Instance.m_fSpittedObjrunRunTime ,ref fRunSpeed, ref fRunAccelerate );
		nail.BeginEject ( fRunSpeed, fRunAccelerate );

		if (photonView.isMine) {
			SetSize (fMotherLeftSize);
		} else {
			Local_SetSize (fMotherLeftSize  );
		}
	}

	public float GetBoundsSize()
	{
		return _srMain.bounds.size.x;
	}

	public void GetDir( ref float fDirX, ref float fDirY )
	{
		fDirX = _dirx;
		fDirY = _diry;
	}

	public Vector3 GetPos()
	{
		return this.gameObject.transform.position;
	}

	public void GetPos( ref float fX, ref float fY )
	{
		fX = this.gameObject.transform.position.x;
		fY = this.gameObject.transform.position.y;
	}

	public float GetPosX()
	{
		return this.gameObject.transform.position.x;
	}

	public float GetPosY()
	{
		return this.gameObject.transform.position.y;
	}

	public void SetPos( Vector3 pos )
	{
		photonView.RPC ( "RPC_SetPos", PhotonTargets.All, pos.x, pos.y, pos.z );
	}

	[PunRPC]
	public void RPC_SetPos( float fX, float fY, float fZ )
	{
		Local_SetPos ( fX, fY, fZ );
	}

	public void Local_SetPos( float fX, float fY, float fZ  )
	{
		vecTempPos.x = fX;
		vecTempPos.y = fY;
		vecTempPos.z = fZ;
		this.gameObject.transform.position = vecTempPos;
	}

	public void SetDir( float fX, float fY )
	{
		photonView.RPC ( "RPC_SetDir", PhotonTargets.All, fX, fY );
	}

	[PunRPC]
	public void RPC_SetDir( float fX, float fY )
	{
		Local_SetDir(  fX,  fY );
	}

	public void Local_SetDir(  float fX, float fY  )
	{
		this._dirx = fX;
		this._diry = fY;
	}

	// 计算新吐出的球（包括刺）初始位置和冲刺方向
	public void CalculateNewBallBornPosAndRunDire( Ball new_ball, float fDirX, float fDirY )
	{
		vecTempPos = GetPos();

		float fMotherCurRadiusAbsVal = _srMain.bounds.size.x  / 2.0f;
		float fChildCurRadiusAbsVal =  new_ball._srMain.bounds.size.x / 2.0f;
		float fDis = fMotherCurRadiusAbsVal +  fChildCurRadiusAbsVal;

		//new_ball._dirx = fDirX;
		//new_ball._diry = fDirY;
		new_ball.SetDir( fDirX, fDirY );

		//vecTempPos.x += fDis * new_ball._dirx;
		//vecTempPos.y += fDis * new_ball._diry;
        CyberTreeMath.ClampBallPosWithinSCreen(vecTempPos, ref vecTempPos2);
		//new_ball.gameObject.transform.position = vecTempPos;
		new_ball.SetPos(vecTempPos2 );
	}



	float m_fChildSize = 0.0f;
	float m_fMotherLeftSize = 0.0f;
	float m_fChildThornSize = 0.0f;
	float m_fMotherLeftThornSize = 0.0f;
	public void CalculateChildSize( float fForceSpitPercent )
	{
        /*
		float fChildSize = 0.0f;
		float fMotherSize = GetSize ();
		float fMotherLeftSize = 0.0f; 
		fChildSize = fMotherSize * fForceSpitPercent;
		if (fChildSize < Main.BALL_MIN_SIZE) {
			fChildSize = Main.BALL_MIN_SIZE;
		}

		fMotherLeftSize = fMotherSize - fChildSize;
		if ( fChildSize >= Main.BALL_MIN_SIZE &&  fMotherLeftSize >= Main.BALL_MIN_SIZE && fMotherLeftSize >= fChildSize) {
			m_fChildSize = fChildSize;
			m_fMotherLeftSize = fMotherLeftSize;
		} 
		*/

        // 按面积来算，不能简单粗暴的按直径来算

        float fMotherSize = GetSize ();
		float fMotherThornSize = GetThornSize ();

        /*
		float fRealPercent = CyberTreeMath.AreaToSize (fForceSpitPercent, Main.s_Instance.m_nShit );
		fChildSize = fMotherSize * fRealPercent; 
		m_fChildThornSize = fMotherThornSize * fRealPercent; 
		if (fChildSize < Main.BALL_MIN_SIZE) {
			fChildSize = Main.BALL_MIN_SIZE;
		}
		fMotherLeftSize = CyberTreeMath.CalculateNewSize ( fMotherSize, fChildSize, Main.s_Instance.m_nShit, -1 );
		m_fMotherLeftThornSize = Mathf.Sqrt ( fMotherThornSize * fMotherThornSize - m_fChildThornSize * m_fChildThornSize );
		if (fChildSize >= Main.BALL_MIN_SIZE && fMotherLeftSize >= Main.BALL_MIN_SIZE && fMotherLeftSize >= fChildSize) {
			m_fChildSize = fChildSize;
			m_fMotherLeftSize = fMotherLeftSize;
		}
        */

        float fMotherArea = CyberTreeMath.SizeToArea(fMotherSize, Main.s_Instance.m_nShit);
        float fChildArea = fForceSpitPercent * fMotherArea;
        float fChildSize = CyberTreeMath.AreaToSize(fChildArea, Main.s_Instance.m_nShit);
        float fMotherLeftArea = fMotherArea - fChildArea;
        float fMotherLeftSize = CyberTreeMath.AreaToSize(fMotherLeftArea, Main.s_Instance.m_nShit);
        m_fChildSize = fChildSize;
        m_fMotherLeftSize = fMotherLeftSize;
        
    }

	public void ClearAvailableChildSize()
	{
		m_fChildSize = 0.0f;
	}

	public float GetAvailableChildSize( ref float fMotherLeftSize )
	{
		fMotherLeftSize = m_fMotherLeftSize;
		return m_fChildSize;
	}

	bool m_bShell = false;
	float m_fShellShrinkCurTime = 0.0f;
	public void BeginShell()
	{
		photonView.RPC ( "RPC_BeginShell", PhotonTargets.All );
	}

	[PunRPC]
	public void RPC_BeginShell()
	{
        Local_BeginShell();


    }

	void Local_BeginShell( float fCurShrinkTime  = 0.0f )
    {
        m_bShell = true;
		if (CheckNeedCollider ()) {
			_Collider.enabled = true; 
		}
		vecTempScale.x = 1.1f;//2.7f;
		vecTempScale.y = 1.1f;//2.7f;
        vecTempScale.z = 1.0f;
        //_shell.transform.localScale = vecTempScale;
        SetShellSize(vecTempScale);
		m_fShellShrinkCurTime = fCurShrinkTime;
        /*
		cTempColor = _srShellForAlpha.color;
        cTempColor.a = 0.4f;
        _srShellForAlpha.color = cTempColor;
		*/
    }


	void Shell()
	{
		if (_shell == null) {
			return;
		}

		if (!m_bShell) {
			return;
		}

		m_fShellShrinkCurTime += Time.deltaTime;
		float fLeftSize = 1.1f - 0.1f * m_fShellShrinkCurTime / Main.s_Instance.m_fShellShrinkTotalTime;
		vecTempScale = _shell.transform.localScale;
		vecTempScale.x = fLeftSize;
		if (vecTempScale.x < 1.0f) {
			if (photonView.isMine) {
				EndShell ();
			} else {
				Local_EndShell ();
			}
		}

		vecTempScale.y = vecTempScale.x;
		//_shell.transform.localScale = vecTempScale;
		SetShellSize(vecTempScale);
	}

	[PunRPC]
	public void RPC_EndShell()
	{
		Local_EndShell ();
	}

	void EndShell()
	{
		photonView.RPC ( "RPC_EndShell", PhotonTargets.All );
	}

	public void Local_EndShell()
	{
		m_bShell = false;
        m_fShellShrinkCurTime = 0.0f;

        vecTempScale.x = 0.9f;
		vecTempScale.y = 0.9f;
		vecTempScale.z = 1f;
		//_shell.transform.localScale = vecTempScale;
		SetShellSize(vecTempScale);

		/*
		cTempColor = _srShellForAlpha.color;
		cTempColor.a = 0.0f;
		_srShellForAlpha.color = cTempColor;
		*/
		_Collider.enabled = false;
	}

	void SetShellSize( Vector3 vecScale )
	{
		_shell.transform.localScale = vecScale;
		//_shellForAlpha.transform.localScale = vecScale;
	}

	public float m_fSpitBallInitSpeed = 5.0f;           // 分球的初速度
	public float m_fSpitBallAccelerate = 0.0f;         // 吐孢子的加速度(这个值通过距离、初速度、时间 算出来，不用配置) 

	public float m_fExplodeInitSpeed = 0.0f;
	public float m_fExplodeInitAccelerate = 0.0f;

	SpitBallTarget m_SpitBallTarget = null;
	float m_fSpitBallTargetParam = 0f;
	public void ShowTarget()
	{
		/*
		if (m_SpitBallTarget == null) {
			m_SpitBallTarget = (GameObject.Instantiate (Main.s_Instance.m_preSpitBallTarget)).GetComponent<SpitBallTarget>();
			m_SpitBallTarget.gameObject.transform.parent = Main.s_Instance.m_goSpitBallTargets.transform;
		}

		float fMotherLeftSize = 0.0f;
		float fChildSize = GetAvailableChildSize ( ref fMotherLeftSize );
		if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE  || fMotherLeftSize < fChildSize) {
			m_SpitBallTarget.gameObject.SetActive (false);
			return;
		} else {
			m_SpitBallTarget.gameObject.SetActive (true);
		}

		vecTempScale.x = fChildSize;
		vecTempScale.y = fChildSize;
		vecTempScale.z = 1.0f;
		m_SpitBallTarget.gameObject.transform.localScale = vecTempScale;

		float fRadiusChild = m_SpitBallTarget.GetBoundsSize() / 2.0f;
		float fRadiusMother = GetBoundsSize( ) * fMotherLeftSize / GetSize () / 2.0f;
		vecTempPos = this.gameObject.transform.position;
		float v0 = Main.s_Instance.m_fBallEjectBaseSpeed;
		float a = Main.s_Instance.m_fBallEjectAccelerate;
		float fDis = v0 * v0 / (2 * a);
		fDis += fRadiusChild;
		fDis += fRadiusMother;
		float fTargetPosX = vecTempPos.x + fDis * _dirx;
		float fTargetPosY = vecTempPos.y + fDis * _diry;
		vecTempPos.x = fTargetPosX;
		vecTempPos.y = fTargetPosY;
		m_SpitBallTarget.gameObject.transform.position = vecTempPos;
		*/
		if (m_SpitBallTarget == null) {
			m_SpitBallTarget = (GameObject.Instantiate (Main.s_Instance.m_preSpitBallTarget)).GetComponent<SpitBallTarget>();
			m_SpitBallTarget.gameObject.transform.parent = Main.s_Instance.m_goSpitBallTargets.transform;
		}

		float fMotherSize = GetSize ();
		float fRadiusMother= GetBoundsSize () / 2.0f;

		float fMotherLeftSize = 0.0f;
		float fChildSize = GetAvailableChildSize ( ref fMotherLeftSize );
		if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE  || fMotherLeftSize < fChildSize) {
			m_SpitBallTarget.gameObject.SetActive (false);
			return;
		} else {
			m_SpitBallTarget.gameObject.SetActive (true);
		}

		vecTempScale.x = fChildSize;
		vecTempScale.y = fChildSize;
		vecTempScale.z = 1.0f;
		m_SpitBallTarget.gameObject.transform.localScale = vecTempScale;

		// right here

		vecTempPos = this.gameObject.transform.position;
		vecTempPos.x += ( Main.s_Instance.m_fSpitBallRunDistanceMultiple * fRadiusMother/* + fRadiusChild / 2f */) * _dirx ;
		vecTempPos.y += ( Main.s_Instance.m_fSpitBallRunDistanceMultiple * fRadiusMother/* + fRadiusChild / 2f*/ ) * _diry ;
		vecTempPos.z = 0;
		m_SpitBallTarget.gameObject.transform.localPosition = vecTempPos;
		//m_SpitBallTarget.m_sr.sprite = Main.s_Instance.GetSpriteByPhotonOwnerId ( photonView.ownerId );
	}

	public void CalculateSpitBallRunParams( float fRadiusMother, float fRadiusChild, float fMultiple, ref float fInitSpeed, ref float fAccelerate )
	{
		float fRunDistance = fMultiple * fRadiusMother;
		fInitSpeed = CyberTreeMath.GetV0 (fRunDistance, Main.s_Instance.m_fSpitRunTime  );
		fAccelerate = CyberTreeMath.GetA ( fRunDistance, Main.s_Instance.m_fSpitRunTime);
	}

	public SpitBallTarget _relateTarget = null;
	public void RelateTarget( Ball ball )
	{
		ball._relateTarget = m_SpitBallTarget;
	}

	public void RefeshPosDueToOutOfScreen()
	{
/*		
		if (_balltype == eBallType.ball_type_thorn && _isSpited) {
			return;
		}
*/
		if (!CheckIfOutOfScreen ()) {
			return;
		}

		this.gameObject.transform.position = Main.s_Instance.RandomPosWithinScreen ();
	}

	public bool CheckIfOutOfScreen()
	{
		Vector3 vecScreenPos = Camera.main.WorldToScreenPoint ( this.gameObject.transform.position );

		if (vecScreenPos.x < 0 || vecScreenPos.x > Screen.width || vecScreenPos.y < 0 || vecScreenPos.y > Screen.height) {
			return true;
		}

		return false;
	}

    public void AutoAttenuateThorn()
    {
        float fThornAttenuate = 0.0f;
        if (MapEditor.s_Instance)
        {
            fThornAttenuate = MapEditor.s_Instance.GetThornAttenuateSpeed();
        }
        float fCurThornSize = GetThornSize();
        fCurThornSize -= fCurThornSize * fThornAttenuate * Time.deltaTime;
        if (fCurThornSize < 0)
        {
            fCurThornSize = 0;
        }
        Local_SetThornSize(fCurThornSize);

    }


    public void AutoAttenuate( float val )
	{
		float fCurSize = GetSize ();
		if (fCurSize <= Main.BALL_MIN_SIZE) {
			return;
		}

		fCurSize -= fCurSize * val;
		if (fCurSize < Main.BALL_MIN_SIZE) {
			fCurSize = Main.BALL_MIN_SIZE;
		}
		Local_SetSize (  fCurSize );



   
    }

	void ShowSize()
	{

		if (_text) {
			string szInfo = GetSize ().ToString ("f2");// GetRadius().ToString ("f2");
			/*
			szInfo += "\n";
			szInfo += GetMianJi ().ToString ("f2");

			*/
			_text.text = szInfo;
		}
	}


    public void EatGrassSeed( int nGrassGUID )
    {
        photonView.RPC("RPC_EatGrassSeed", PhotonTargets.AllBuffered, this.photonView.ownerId, nGrassGUID, PhotonNetwork.time );
    }

    [PunRPC]
    public void RPC_EatGrassSeed( int nOwnerId, int nGrassGUID, double dCurTime )
    {
        Local_EatGrassSeed(nOwnerId, nGrassGUID, dCurTime);
    }

    public void Local_EatGrassSeed(int nOwnerId, int nGrassGUID, double dCurTime)
    {
        MapEditor.s_Instance.ProcessGrassSeed(nOwnerId, nGrassGUID, dCurTime);
    }

	public void SyncBaseInfo()
	{
		vecTempPos = GetPos ();
		photonView.RPC ("RPC_SyncBaseInfo", PhotonTargets.Others, vecTempPos.x, vecTempPos.y, vecTempPos.z);
	}

	[PunRPC]
	public void RPC_SyncBaseInfo( float fPosX, float fPosY, float fPosZ)
	{
		Local_SetPos ( fPosX, fPosY, fPosZ );
	}


	public void Spit()
	{
		if (IsEjecting ()) {
			return;
		}

		float fMotherSize = GetSize ();
		float fMotherLeftSize = 0.0f;
		float fChildSize = GetAvailableChildSize( ref fMotherLeftSize );
		if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE || fMotherLeftSize < fChildSize ) {
			return;
		}


		Ball new_ball = Ball.NewOneBall( photonView.ownerId );//PhotonInstantiate (m_preBall, Vector3.zero, Main.s_Instance.s_Params);
		//Ball new_ball = goNewBall.GetComponent<Ball> ();
		new_ball.SetSize ( fChildSize );
		new_ball.transform.parent = _Player.transform;
		//CalculateNewBallBornPosAndRunDire ( new_ball, _dirx, _diry );
		float fRadiusMother = _srMain.bounds.size.x  / 2.0f;
		float fSpitBallInitSpeed = 0.0f;
		float fSpitBallAccelerate = 0.0f;
		//CalculateSpitBallRunParams ( fRadiusMother, fRadiusChild, Main.s_Instance.m_fSpitBallRunDistanceMultiple, ref fSpitBallInitSpeed, ref fSpitBallAccelerate );
		float fRunDistance = ( Main.s_Instance.m_fSpitBallRunDistanceMultiple ) * fRadiusMother;
		fSpitBallInitSpeed = CyberTreeMath.GetV0 (fRunDistance, Main.s_Instance.m_fSpitRunTime  );
		fSpitBallAccelerate = CyberTreeMath.GetA ( fRunDistance, Main.s_Instance.m_fSpitRunTime);
		vecTempPos = GetPos ();
		new_ball.SetPos ( vecTempPos );
		new_ball.SetDir ( _dirx, _diry );
		RelateTarget ( new_ball );
		SetSize ( fMotherLeftSize );
		BeginShell ();
		new_ball.SetThornSize ( GetThornSize() * fChildSize / fMotherSize );
		//new_ball.BeginShell ();
		new_ball.BeginEject ( fSpitBallInitSpeed, fSpitBallAccelerate, Main.s_Instance.m_fSpitRunTime );
	}

	float m_fMoveSpeedX = 0.0f;
	float m_fMoveSpeedY = 0.0f;
	Vector2 m_vecdirection = new Vector2();
	public void BeginMove( Vector3 vecWorldCursorPos  )
	{
		m_vecdirection = vecWorldCursorPos - this.transform.position;
		m_vecdirection.Normalize();
		float fSpeed = Main.s_Instance.m_fBallBaseSpeed / GetSize2KaiGenHao();;
		m_fMoveSpeedX = fSpeed * m_vecdirection.x;
		m_fMoveSpeedY = fSpeed * m_vecdirection.y;
	}

	void Move()
	{
		if (!_Player.IsMoving()) {
			return;
		}

		vecTempPos = this.transform.position;
		vecTempPos.x += m_fMoveSpeedX * Time.deltaTime;
		vecTempPos.y += m_fMoveSpeedY * Time.deltaTime;
		this.transform.position = vecTempPos;
	}

	public void SyncFullInfo()
	{
		vecTempPos = GetPos ();
		photonView.RPC ( "RPC_SyncFullInfo", PhotonTargets.Others, vecTempPos.x, vecTempPos.y, vecTempPos.z, GetSize(), m_fShellShrinkCurTime );
	}

	[PunRPC]
	public void RPC_SyncFullInfo( float fPosX, float fPosY, float fPosZ, float fSize, float fShellTime )
	{
		Local_SetPos ( fPosX,  fPosY, fPosZ);
		Local_SetSize ( fSize );
		if (fShellTime > 0f) {
			Local_BeginShell ( fShellTime );
		}
	}

	public int Split( Vector3 vecWorldCursorPos, ref float fSmallSize )
	{
		int nRealSmallBallNum = 0;
		if (Main.s_Instance.GetCurAvailableBallCount () <= 0) {
			return nRealSmallBallNum;
		}
		float fCurSize = GetSize ();
		float fCurArea = CyberTreeMath.SizeToArea (fCurSize, Main.s_Instance.m_nShit);
		Vector3 vDir =  vecWorldCursorPos - this.transform.position;
		vDir.Normalize ();
		m_vecdirection.Normalize();
		float fMotherRadius = _srMain.bounds.size.x / 2f;
		for (int i = Main.s_Instance.m_nSplitNum; i >= 2; i--) {
			float fSmallArea = fCurArea / i;
			fSmallSize = CyberTreeMath.AreaToSize ( fSmallArea, Main.s_Instance.m_nShit );
			if (fSmallSize >= Main.BALL_MIN_SIZE) {
				for (int j = 0; j < i; j++) {
					if (Main.s_Instance.GetCurAvailableBallCount () <= 0) {
						return nRealSmallBallNum;
					}
					Ball new_ball = Ball.NewOneBall( photonView.ownerId );
					new_ball.SetSize ( fSmallSize );
					new_ball.transform.parent = _Player.transform;
					new_ball.BeginShell ();
					float fDis = fMotherRadius * nRealSmallBallNum * Main.s_Instance.m_fSpitBallRunDistanceMultiple;
					vecTempPos.x = this.transform.position.x + fDis * vDir.x;
					vecTempPos.y = this.transform.position.y + fDis * vDir.y;
					vecTempPos.z = this.transform.position.z;
					new_ball.transform.position = vecTempPos;
					nRealSmallBallNum++;
				}
				break;
			}
		}
		return nRealSmallBallNum;
	}

	bool m_bSplittig = false;
	Vector3 m_vDir = new Vector3();
	float m_fSmallArea = 0.0f;
	float m_fSmallSize = 0.0f;
	int m_nSplitNum = 0;
	int m_nSplitCount = 0;
	float m_fMotherRadius = 0f;
	float m_fMotherArea = 0f;
	public void BeginSplit( Vector3 vecWorldCursorPos ) // 分帧处理机制
	{
		if (Main.s_Instance.GetCurAvailableBallCount () <= 0) {
			return;
		}

		// m_nSplitNum是策划配置的预期分球数量。实际具体能分几个还得计算。
		float fCurSize = GetSize ();
		float fMotherArea = CyberTreeMath.SizeToArea (fCurSize, Main.s_Instance.m_nShit);
		bool bOK = false;
		int nRealSplitNum = 0;
		float fSmallSize = 0f;
		int nAvailable = Main.s_Instance.GetCurAvailableBallCount () + 1;
		int nCount = Main.s_Instance.m_nSplitNum < nAvailable?Main.s_Instance.m_nSplitNum:nAvailable;
		for (int i = nCount; i >= 2; i--) {
			float fSmallArea = fMotherArea / i;
			if (fSmallArea >= Main.BALL_MIN_SIZE) {
				nRealSplitNum = i; // 具体分球数量定了
				fSmallSize = CyberTreeMath.AreaToSize ( fSmallArea, Main.s_Instance.m_nShit );
				bOK = true;
				break;
			}
		}

		if (!bOK) {
			return;
		}

		float fTotalDis = Vector2.Distance ( GetPos(), Main.s_Instance.m_MainPlayer.GetWorldCursorPos() );
		if (fTotalDis > Main.s_Instance.m_fSplitMaxDistance) {
			fTotalDis = Main.s_Instance.m_fSplitMaxDistance;
		}
		float fSegDis = fTotalDis / nRealSplitNum;
		for (int i = 0; i < nRealSplitNum; i++) {
			Ball new_ball = Ball.NewOneBall( this.photonView.ownerId );
			new_ball.SetSize ( fSmallSize );
			new_ball.SetPos ( GetPos() );
			new_ball.SetDir ( _dirx, _diry );
			float fDis = fSegDis * ( i + 1 );
			float fInitSpeed = CyberTreeMath.GetV0 (fDis, Main.s_Instance.m_fSplitRunTime  );
			float fAccelerate = CyberTreeMath.GetA ( fDis, Main.s_Instance.m_fSplitRunTime);
			new_ball.BeginEject ( fInitSpeed, fAccelerate, Main.s_Instance.m_fSplitRunTime, true, true );
		}

		this.DestroyBall ();
		/*
		if (Main.s_Instance.GetCurAvailableBallCount () <= 0) {
			return;
		}
		this._Trigger.enabled = false;
		m_vDir = vecWorldCursorPos - this.transform.position;
		m_vDir.Normalize ();
		m_fMotherRadius = _srMain.bounds.size.x / 2f;
		float fCurSize = GetSize ();
		m_fMotherArea = CyberTreeMath.SizeToArea (fCurSize, Main.s_Instance.m_nShit);
		bool bOK = false;
		for (int i = Main.s_Instance.m_nSplitNum; i >= 2; i--) {
			m_fSmallArea = m_fMotherArea / i;
			if (m_fSmallArea >= Main.BALL_MIN_SIZE) {
				m_bSplittig = true;
				m_nSplitNum = i;
				m_nSplitCount = 0;
				m_fSmallSize = CyberTreeMath.AreaToSize ( m_fSmallArea, Main.s_Instance.m_nShit );
				m_nSpittingCount = 0;
				return;
			}
		}
		*/
	}

	void CreateBall( Vector3 pos, float fSize, Vector2 dir )
	{
		Ball new_ball = Ball.NewOneBall( this.photonView.ownerId );
		new_ball.SetPos( pos );
		new_ball.SetSize(fSize);
		new_ball.gameObject.transform.parent = _player.transform;
		new_ball._dirx = dir.x;
		new_ball._diry = dir.y;
	}

	int m_nSpittingInterval = 1;
	int m_nSpittingCount = 0;
	public void Splitting()
	{
		if (!m_bSplittig) {
			return;
		}

		if (Main.s_Instance.GetCurAvailableBallCount () <= 0 || m_nSplitCount >= m_nSplitNum) {
			EndSplit ();
			return;
		}

		Ball new_ball = Ball.NewOneBall( photonView.ownerId );
		new_ball.SetSize ( m_fSmallSize );
		new_ball.transform.parent = _Player.transform;
		new_ball.BeginShell ();
		float fDis = m_fMotherRadius * ( (m_nSplitCount + 1 ) * Main.s_Instance.m_fSpitBallRunDistanceMultiple  ) + new_ball._srMain.bounds.size.x;
		vecTempPos.x = this.transform.position.x + fDis * m_vDir.x;
		vecTempPos.y = this.transform.position.y + fDis * m_vDir.y;
		vecTempPos.z = this.transform.position.z;
		new_ball.transform.position = vecTempPos;
		new_ball.SyncBaseInfo ();
		m_nSplitCount++;
	}

	void EndSplit()
	{
		m_bSplittig = false;
		if (m_nSplitCount >= m_nSplitNum) {
			this.DestroyBall ();
		} else {
			float fChildArea = CyberTreeMath.SizeToArea (m_fSmallSize, Main.s_Instance.m_nShit);
			float fLeftArea = m_fMotherArea - fChildArea * m_nSplitCount;
			float fLeftSize = CyberTreeMath.AreaToSize (fLeftArea, Main.s_Instance.m_nShit);
			SetSize ( fLeftSize );
			this._Trigger.enabled = true;
		}
	}

}