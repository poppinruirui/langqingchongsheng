﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using UnityEngine.UI;

public class DaTaoSha : MonoBehaviour {

	//// Prefab
	public GameObject m_preFood;

	public GameObject m_btnStart;

	bool m_bDaTaoShaing = false;

	float m_fTotalTime = 60.0f;
    float m_fFoodTimeInterval = 0.0f; 
    float m_fFoodTimeCount = 0.0f; 
	float m_fMaxSize = 0.0f;
	float m_fMinSize = 30.0f;
	float m_fMinSizePercent = 0.3f;
	List<TattooFood> m_lstFood = new List<TattooFood>();
	List<int> m_lstFoodToCreate = new List<int>();

	float m_fStartTime;
	float m_fTimeCount= 0.0f;
	float m_fSizeDelta = 0.0f;

	static Vector3 vecTempScale = new Vector3();
    static Vector3 vecTempPos = new Vector3();

	public MeshRenderer m_Render;
	public GameObject m_goDouZiContainer;


	// Use this for initialization
	void Start () {
		
	}

	public static DaTaoSha s_Instance = null;	
	void Awake()
	{
		s_Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		DaTaoShaLoop ();


	}

	public void Load()
	{

	}

	public void SetMaxSize( float fMaxSize )
	{
		m_fMaxSize = fMaxSize;
	}

	public float GetMaxSize()
	{
		return m_fMaxSize;
	}

	public void StartDaTaoSha( float fStartTime )
	{
		m_fStartTime = fStartTime;
		m_fTimeCount = 0.0f;
		m_fSizeDelta = m_fMaxSize - m_fMinSize;
		m_bDaTaoShaing = true;

		m_fFoodTimeInterval = m_fTotalTime / ( m_lstFoodToCreate.Count + 1 );
	}

	public void DaTaoShaLoop()
	{
		if (!m_bDaTaoShaing) {
			return;
		}

		FoodLoop();

		m_fTimeCount = (float)PhotonNetwork.time - m_fStartTime;
		float fPercent = m_fTimeCount / m_fTotalTime;
		float fRealtimeSize = fPercent * m_fSizeDelta;
		float fCurSize = m_fMaxSize - fRealtimeSize;
        vecTempScale.x = fCurSize;
        vecTempScale.y = fCurSize;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
		if ( fCurSize <= m_fMinSize )
		{
			m_bDaTaoShaing = false;
		}
  	}

 

	public void SetCurSize( float fCurSize )
	{
		vecTempScale.x = fCurSize;
		vecTempScale.y = fCurSize;
		vecTempScale.z = 1.0f;
		this.transform.localScale = vecTempScale;	
	}

	public void SetMinSizePercent( float val )
	{
		m_fMinSizePercent = val;
		m_fMinSize = m_fMaxSize * m_fMinSizePercent;
	}

	public float GetMinSizePercent()
	{
		return m_fMinSizePercent;
	}

	public float GetMinSize()
	{
		return m_fMinSize;
	}

	public void SetTotalTime( float val )
	{
		m_fTotalTime = val;
	}

	public float GetTotalTime()
	{
		return m_fTotalTime;
	}


	void FoodLoop()
	{
		m_fFoodTimeCount += Time.deltaTime;
		if (m_fFoodTimeCount >= m_fFoodTimeInterval) {
			ShowOneFood ();
			m_fFoodTimeCount = 0f;
		}
	}

	public void AddOneFood( TattooFood food )
	{
		food.transform.parent = m_goDouZiContainer.transform;
		m_lstFood.Add ( food );
	}

	int m_nGUID = 0;
	void ShowOneFood ()
	{
		if (!PhotonNetwork.isMasterClient) {
			return;
		}

		if (m_lstFoodToCreate.Count == 0) {
			return;
		}
		int id = m_lstFoodToCreate [0];
		m_lstFoodToCreate.RemoveAt (0);

		Main.s_Instance.m_MainPlayer.GenerateOneFood ( id, m_nGUID++ );

	

		/*
		if (m_lstFood.Count == 0) {
			return;
		}
		TattooFood food = m_lstFood [0];
		m_lstFood.RemoveAt (0);
		food.gameObject.SetActive ( true );
		*/

	}

	public void AddFoodToCreate( int id )
	{
		m_lstFoodToCreate.Add ( id );
	}
}
