﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;  
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.SceneManagement;
using System.Xml;

public class AccountManager : Photon.PunBehaviour {

    public static AccountManager s_Instance = null;

    string m_szCurAccount = "";

    bool m_bLogin = false;

    public const string c_szRoomListPathAndFileName_Server = "\\\\192.168.31.1\\共享\\liruirui/RoomList.xml";
    public const string c_szRoomListPath_Server = "\\\\192.168.31.1\\共享\\liruirui";
  
    //// !---- UI
    public InputField _inputAccount;
    public Text _txtCurAccount;
    public GameObject _panelBeforeLogin;
    public GameObject _panelAfterLogin;
    public Dropdown _dropdownRoomList;
    public GameObject _panelNewRoom;
    public InputField _inputNewRoomName;

    public MsgBox m_MsgBox;

    List<string> m_lstRoomList = new List<string>();
    public static string m_szCurSelectedRoomName = "";

    string m_szPathAndFileName_Client = "";

    public enum eSceneMode
    {
        Game,
        MapEditor,
    };

    public static eSceneMode m_eSceneMode = eSceneMode.MapEditor;

    void Awake()
    {
        s_Instance = this;

        InitPhoton();

    }

    string _gameVersion = "1";
    void InitPhoton()
    {
        PhotonNetwork.autoJoinLobby = false;
        PhotonNetwork.automaticallySyncScene = true;
    }

    public void PlayRoom( string szRoomName )
    {
        PhotonNetwork.ConnectUsingSettings(_gameVersion);
        m_szCurSelectedRoomName = szRoomName;
    }

    void OnConnectedToMaster()
    {

        Debug.Log("连上了Master Server.  Mater Server是用于分配游戏房间的服务器");
        PhotonNetwork.JoinOrCreateRoom ( m_szCurSelectedRoomName, new RoomOptions() { MaxPlayers = 20}, null );
    }

	// Use this for initialization
	void Start () {
		
        // 从服务器下载房间列表文件
        m_szPathAndFileName_Client = Application.streamingAssetsPath + "/RoomList.xml";
        IOManager.DownLoad( c_szRoomListPathAndFileName_Server, m_szPathAndFileName_Client );
        XmlNode root = null;
        XmlDocument XmlDoc = IOManager.LoadXmlFile( m_szPathAndFileName_Client, ref root );
        for (int i = 0; i < root.ChildNodes.Count; i++)
        {
            //m_lstRoomList.Add( root.ChildNodes[i].InnerText );
            AddOneRoom(root.ChildNodes[i].InnerText, 0);
        }

        RefreshRoomListDropDown();
        if (m_lstRoomList.Count > 0)
        {
            _dropdownRoomList.captionText.text = m_lstRoomList[0];
            m_szCurSelectedRoomName = m_lstRoomList[0];
        }
	}

    void RefreshRoomListDropDown()
    {
        IOManager.UpdateDropdownView( _dropdownRoomList, m_lstRoomList );
        //_dropdownRoomList.captionText.text = "(未选择)";
    }
	
	// Update is called once per frame
	void Update () {

	}

    public void CreateNewRoom()
    {
        _panelNewRoom.SetActive( true );
    }

    public void PlaySelectedRoom()
    {
        if (!CheckIfCurSelectedRoomValid())
        {
            return;
        }

        PlayRoom( m_szCurSelectedRoomName );
    }

    public override void OnJoinedRoom()   
    {
       if (PhotonNetwork.room.PlayerCount == 1)
       {
          PhotonNetwork.LoadLevel("Scene");
       }
       m_eSceneMode = eSceneMode.Game; // “运行游戏”模式
    }

    public bool CheckIfCurSelectedRoomValid()
    {
        if (m_szCurSelectedRoomName.Length == 0)
        {
            return false;
        }

        return true;
    }

    public void EditSelectedRoom()
    {
        if (!CheckIfCurSelectedRoomValid())
        {
            return;
        }

        SceneManager.LoadScene( "Scene" ) ;
    }

    public static string GetCurRoom()
    {
        return m_szCurSelectedRoomName;
    }

    public void CreateNewRoom_OK()
    {
        bool bEmpty = ( m_lstRoomList.Count == 0 );
        string szNewRoomName = _inputNewRoomName.text;
        //m_lstRoomList.Add(_inputNewRoomName.text);
        AddOneRoom(_inputNewRoomName.text, 1);
        RefreshRoomListDropDown();
        ClosePanelNewRoom();
        if (bEmpty) // 列表如果本来为空，现在创建了第一条记录，则自动选择第一条记录。
        {
            _dropdownRoomList.captionText.text = szNewRoomName;
            m_szCurSelectedRoomName = szNewRoomName;
        }

        UpdateRoomListXmlFile();
    }

    void AddOneRoom( string szRoomName, int op )
    {
        m_lstRoomList.Add(szRoomName);

        string szServerName = IOManager.GenerateServerName(szRoomName);
        string szClientName = IOManager.GenerateClientName(szRoomName);
        if (op == 0) // down load
        {
            IOManager.DownLoad( szServerName, szClientName );
        }
        else if (op == 1) // up load
        {
            IOManager.CreateXmlFile( szClientName );
            IOManager.Upload( szClientName, szServerName );
        }
    }

    public void SaveAndUploadMapFile( XmlDocument XmlDoc, string szFileName )
    {
        string szServerName = c_szRoomListPath_Server + "/" + szFileName + ".xml";
        string szClientName = Application.streamingAssetsPath  + "/" + szFileName + ".xml";
        XmlDoc.Save( szClientName );
        IOManager.Upload( szClientName, szServerName );
    }

    void UpdateRoomListXmlFile()
    {
        XmlNode root = null;
        XmlDocument XmlDoc = IOManager.LoadXmlFile( m_szPathAndFileName_Client, ref root );
        root.RemoveAll();
        for (int i = 0; i < m_lstRoomList.Count; i++)
        {
            IOManager.CreateNode(XmlDoc, root, "P", m_lstRoomList[i]);
        }
        XmlDoc.Save(m_szPathAndFileName_Client);
        IOManager.Upload( m_szPathAndFileName_Client, c_szRoomListPathAndFileName_Server );
    }



    public void ClosePanelNewRoom()
    {
        _panelNewRoom.SetActive( false );
        _inputNewRoomName.text = "";
    }

    public void CreateNewRoom_Cancel()
    {
        ClosePanelNewRoom();
    }

    public void OnDropDownValueChanged_RoomList()
    {
        m_szCurSelectedRoomName = _dropdownRoomList.captionText.text;
    }

    public bool CheckIfAccountValid( string szAccount )
    {
        if (szAccount.Length == 0)
        {
            return false;
        }

        return true;
    }

    public bool GetIfLogin()
    {
        return m_bLogin;
    }

    public void LogIn()
    {
        if (!CheckIfAccountValid( _inputAccount.text ))
        {
            return;
        }

        m_szCurAccount = _inputAccount.text;
        _panelBeforeLogin.SetActive( false );
        _panelAfterLogin.SetActive( true );
        _txtCurAccount.text = m_szCurAccount;
        m_bLogin = true;
    }

    public void LogOut()
    {
        m_szCurAccount = "";
        _panelBeforeLogin.SetActive( true );
        _panelAfterLogin.SetActive( false );
        _txtCurAccount.text = "";
        _inputAccount.text = "";
        m_bLogin = false;
    }

    public void EnterMySpace()
    {
        if (!GetIfLogin())
        {
            return;
        }
        
        SceneManager.LoadScene( "SelectQu" );
    }

    public void OnInputValueChanged_Account()
    {

    }


}
