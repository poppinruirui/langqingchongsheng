﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Photon.PunBehaviour , IPunObservable 
{
	public bool _isRebornProtected = true;

	public Color _color_inner;
	public Color _color_ring;
	public Color _color_poison;

	float m_fProtectTime= 0.0f;

	List<Ball> m_lstBalls = new List<Ball>();
	int m_nBallIndex = 0;

	static Vector3 vecTempPos = new Vector3 ();
	// Use this for initialization
	void Start () 
	{
		
	}

	void Awake()
	{
		Init ();
	}
	
	// Update is called once per frame
	void Update () {
		ProtectTimeCount ();
		SyncBallsInfo ();
		Spitting ();
		Syncing_FullInfo ();
		PreDrawLoop ();

	}

	void Init()
	{
		_isRebornProtected = true; // 第一次出生时一定是处于“重生保护状态”

		this.gameObject.name = "player_" + photonView.ownerId;
		this.gameObject.transform.parent = Main.s_Instance.m_goBalls.transform;

		int color_index = 0;
		_color_ring = ColorPalette.RandomOuterRingColor (ref color_index);
		_color_inner = ColorPalette.RandomInnerFillColor ( photonView.ownerId/*color_index*/ );
		_color_poison = ColorPalette.RandomPoisonFillColor ( photonView.ownerId/*color_index*/);

		if (photonView.isMine) {
			FuckMe ();
		}
	}

	// 有新玩家进房间，把已经在房间的玩家信息来一次全量同步
	public void FuckMe()
	{
		photonView.RPC ( "RPC_FuckMe", PhotonTargets.Others );
	}

	[PunRPC]
	public void RPC_FuckMe()
	{
		if (Main.s_Instance) {
			Main.s_Instance.SyncFullInfo ();
		}
	}



	public void PhotonRemoveObject( GameObject go )
	{
		PhotonNetwork.Destroy( go );
	}

	public void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{

	}

	public void BeginProtect()
	{
		photonView.RPC ( "RPC_BeginProtect", PhotonTargets.All );
	}

	[PunRPC]
	public void RPC_BeginProtect()
	{
		m_fProtectTime = 3.0f;
	}

	void ProtectTimeCount()
	{
		if (m_fProtectTime > 0.0f) {
			m_fProtectTime -= Time.deltaTime;
		}
	}

	public bool IsProtect()
	{
		return m_fProtectTime > 0.0f;
	}

	int m_nKingExplodeIndex = 0;
	bool m_bKingExploding = false;
	public void KingExplode ()
	{

	}

	bool m_bSyncingFullInfo = false;
	int m_SyncFullInfoIndex = 0;
	public void SyncFullInfo()
	{
		m_bSyncingFullInfo = true;
		m_SyncFullInfoIndex = 0;
	}

	void Syncing_FullInfo()
	{
		if (m_SyncFullInfoIndex >= m_lstBalls.Count) {
			m_bSyncingFullInfo = false;
			return;
		}

		if (!m_bSyncingFullInfo) {
			return;
		}

		Ball ball = m_lstBalls[m_SyncFullInfoIndex];
		if (ball) {
			ball.SyncFullInfo ();
		}
		m_SyncFullInfoIndex++;
	}

	Vector3 m_vWorldCursorPos = new Vector3();
	public void SyncMoveInfo( Vector3 vecWorldCursorPos )
	{
		m_vWorldCursorPos = vecWorldCursorPos;
		DoMove ( vecWorldCursorPos.x, vecWorldCursorPos.y );
	}

	public Vector3 GetWorldCursorPos ()
	{
		return m_vWorldCursorPos;
	}

	public void DoMove ( float fWorldCursorPosX, float fWorldCursorPosY  )
	{
		if (!photonView.isMine) {
			return;
		}

		photonView.RPC ( "RPC_DoMove", PhotonTargets.Others, fWorldCursorPosX, fWorldCursorPosY );
	}

	// 终于领悟了帧同步的真谛：同步指令而不是同步全量数据

	static Vector2 vec2TempPos = new Vector2 ();
	static Vector3 vec3TempPos = new Vector3 ();
	[PunRPC]
	public void RPC_DoMove( float fWorldCursorPosX, float fWorldCursorPosY )
	{
		m_vWorldCursorPos.x = fWorldCursorPosX;
		m_vWorldCursorPos.y = fWorldCursorPosY;
		for (int i = m_lstBalls.Count - 1; i >= 0; i--) {
			Ball ball = m_lstBalls[i];
			if (ball == null) {
				m_lstBalls.RemoveAt (i);
				continue;
			}

			vec3TempPos.x = fWorldCursorPosX;
			vec3TempPos.y = fWorldCursorPosY;
			ball.BeginMove ( vec3TempPos );
		}
	}

	bool m_bMoving = false;
	public void SetMoving( bool val )
	{
		photonView.RPC ( "RPC_SetMoving", PhotonTargets.All, val );
	}

	[PunRPC]
	public void RPC_SetMoving( bool val )
	{
		m_bMoving = val;
	}

	public bool IsMoving()
	{
		return m_bMoving;
	}

	public void AddOneBall( Ball ball )
	{
		m_lstBalls.Add ( ball );
	}

	public void RemoveOneBall( Ball ball )
	{
		m_lstBalls.Remove ( ball );
	}
		
	int m_nSyncBallsInfoIndex = 0;
	const float c_SyncBallsInfoInterval = 1f;
	float m_fSyncBallsInfoCount = 0.0f;
	public void SyncBallsInfo ()
	{
		if (!photonView.isMine) {
			return;
		}

		if (m_fSyncBallsInfoCount < c_SyncBallsInfoInterval) {
			m_fSyncBallsInfoCount += Time.deltaTime;
			return;
		}
		m_fSyncBallsInfoCount = 0f;

		for (int i = m_lstBalls.Count - 1; i >= 0; i--) {
			Ball ball = m_lstBalls [i];
			if (ball == null) {
				continue;
			}
			ball.SyncBaseInfo ();
		}
		/*
		if (m_lstBalls.Count == 0) {
			return;
		}

		if (m_nSyncBallsInfoIndex <= c_SyncBallsInfoInterval) {
			m_nSyncBallsInfoIndex++;
			return;
		} 
		m_nSyncBallsInfoIndex = 0;

		if (m_nBallIndex >= m_lstBalls.Count) {
			m_nBallIndex = 0;
		}

		Ball ball = m_lstBalls [m_nBallIndex];
		if (ball != null) {
			ball.SyncBaseInfo ();
		} else {
			m_lstBalls.RemoveAt ( m_nBallIndex );
			return;
		}
		m_nBallIndex++;
		*/
	}

	int m_nSpitIndex = 0;
	bool m_bSpitting = false;
	public void BeginSpit()
	{
		// poppin test
		int nCount = m_lstBalls.Count;
		for (int i = 0; i < nCount; i++) {
			Ball ball = m_lstBalls [i];
			if (ball == null) {
				continue;
			}
			ball.Spit ();
		}
		return;


		if (m_bSpitting) {
			return;
		}

		m_nSpitIndex = m_lstBalls.Count - 1;
		m_bSpitting = true;
	}

	void Spitting()
	{

		if (!m_bSpitting) {
			return;
		}

		if (m_lstBalls.Count == 0) {
			EndSpit ();
			return;
		}

		if (m_nSpitIndex >= m_lstBalls.Count) {
			m_nSpitIndex = m_lstBalls.Count - 1;
		}

		Ball ball = m_lstBalls [m_nSpitIndex];
		m_nSpitIndex--;
		if (m_nSpitIndex < 0) {
			EndSpit ();
		}
		if (ball) {
			ball.Spit ();
		}
	}

	void EndSpit()
	{
		m_bSpitting = false;
	}

	float m_fPreDrawCount = 0.0f;
	void PreDrawLoop()
	{
		if (Launcher.m_eGameMode != Launcher.eGameMode.xiqiu) {
			return;
		}

		if (m_fPreDrawCount < 0.3f) {
			m_fPreDrawCount += Time.deltaTime;
			return;
		}
		m_fPreDrawCount = 0.0f;

		for (int i = m_lstBalls.Count - 1; i >= 0; i--) {
			Ball ball = m_lstBalls [i];
			if (ball == null) {
				m_lstBalls.RemoveAt ( i );
				continue;
			}
			ball.DoDraw ();
		}
	}

	public void MergeAll()
	{
		for (int i = m_lstBalls.Count - 1; i >= 1; i--) {
			Ball ball = m_lstBalls[i];
			Ball ballOpponent = m_lstBalls[i-1];
			ball.EatBall (ballOpponent);
		}
	}

	bool m_bSplitting = false;
	int m_nSplitCount = 0;
	public void OneBtnSplitBall( Vector3 vecWorldCursorPos )
	{
		m_nSplitCount = m_lstBalls.Count;
		for (int i = m_nSplitCount - 1	; i >= 0; i--) {
			Ball ball = m_lstBalls [i];
			if (ball == null) {
				m_lstBalls.RemoveAt (i);
				continue;
			}
			float fSmallSize = 0.0f;
			ball.BeginSplit( vecWorldCursorPos ) ;
		}
	}

	void Splitting()
	{
		if (!m_bSplitting) {
			return;
		}

		for (int i = m_nSplitCount - 1; i >= 0; i--) {
			Ball ball = m_lstBalls [i];
			if (ball == null) {
				m_lstBalls.RemoveAt (i);
				continue;
			}
			ball.Splitting ();
		}
	}

	public void StartDaTaoSha()
	{
		photonView.RPC ( "RPC_StartDaTaoSha", PhotonTargets.AllBuffered, (float)PhotonNetwork.time );
	}

	[PunRPC]
	public void RPC_StartDaTaoSha( float fStarttime )
	{
		DaTaoSha.s_Instance.StartDaTaoSha (fStarttime);
		DaTaoSha.s_Instance.m_btnStart.gameObject.SetActive ( false );
	}

	public void EatDaTaoShaFood()
	{

	}

	public int GetCurMainPlayerBallCount()
	{
		return m_lstBalls.Count;
	}

	public void RemoveOnetattoo( TattooFood food )
	{
		food.SetDestroyed ( true );
		photonView.RPC ( "RPC_RemoveOnetattoo", PhotonTargets.AllBuffered, food.GetGUID() );
	}

	[PunRPC]
	public void RPC_RemoveOnetattoo( int nGUID )
	{
		TattooFood food = null;
		Main.s_Instance.RemoveOnetattoo (nGUID);
	}

	public void GenerateOneFood( int id, int nGUID )
	{
		if (!PhotonNetwork.isMasterClient) {
			return;
		}

		float fDis = DaTaoSha.s_Instance.m_Render.bounds.size.x / 2f * 0.707f;
		vecTempPos.x = (float)UnityEngine.Random.Range ( -fDis, fDis );
    	vecTempPos.y = (float)UnityEngine.Random.Range ( -fDis, fDis );
		photonView.RPC ("RPC_GenerateOneFood", PhotonTargets.AllBuffered, id, nGUID, vecTempPos.x, vecTempPos.y);
	}

	[PunRPC]
	public void RPC_GenerateOneFood( int id, int nGUID, float posX, float posY )
	{
		bool ret = false;
		MapEditor.sBeiShuDouZiConfig config = MapEditor.s_Instance.GetFoodConfig (id, ref ret);
		if (!ret) {
			return;
		}

		GameObject goFood = ResourceManager.s_Instance.PrefabInstantiate ( "prefab/TattooFood/preTF_0" );
		TattooFood food = goFood.GetComponent<TattooFood> ();
		food.SetGUID ( nGUID );
		food._type = TattooFood.eTattooFoodType.TattooFood_BeiShuDouZi;
		food._subtype = config.id;
		food.SetParam( 0, (object)config.value );
		food.gameObject.SetActive ( true );
		/*
		if (config.id == 0) {
			food.SetColor ( Color.green );
		} else {
			food.SetColor ( Color.red );
		}
		*/
		vecTempPos.x = posX;
		vecTempPos.y = posY;
		vecTempPos.z = 0f;
		food.transform.localPosition = vecTempPos;
		Main.s_Instance.AddOnetattooFood ( food );
		DaTaoSha.s_Instance.AddOneFood ( food );
	}

	public List<Ball> GetBallList()
	{
		return m_lstBalls;
	}
	
}
