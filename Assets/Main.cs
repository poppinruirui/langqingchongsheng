﻿/*
 *  只要有流水，奖金每月发，所谓年终奖不是真汉子。


*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

/*
 * 关于豆子的基本规则
 * 1、一个竞技场的总面积是有限的（有边界）。面积可按策划需求配置。
 * 2、豆子单位面积的分布数量有一个系数，该系数可按策划需求配置。
 * 3、豆子是要网络同步的，同竞技场每个客户端面对的豆子都完全一致（位置、产出、消耗 等）
 * 4、玩家视角不限，只要在游戏逻辑允许的范围内，视角可以大到同时看到全竞技场所有的豆子，故必须有优化策略（后期再考虑）
 * 5、每个豆子被消耗掉后，并非立即又刷出来，而是间隔一定时间后再刷出来，这个时间间隔可按策划需求配置。
 */
public class Main :  Photon.PunBehaviour 
{
	public Sprite[] m_aryBallSprite; 

	// Self
	public static Main s_Instance;

	public PlayerAction _playeraction;

	  // prefab “预设件”。  游戏中的各种对象都是用预设件来实例化出来的
    public GameObject m_preGridLine;
    public GameObject m_preBallLine;   
	public GameObject m_preBall;  
	public GameObject m_preElectronicBall;  
	public GameObject m_prePlayer;
	public GameObject m_preThorn; //  刺
	public GameObject m_preRhythm; // 节奏环
	public GameObject m_preBean;   // 豆子
	public GameObject m_preEffectPreExplode; // 预爆炸
	public GameObject m_preEffectExplode;    //  爆炸 
	public GameObject m_preBlackHole; // 黑洞
	public GameObject m_preSpitBallTarget; // 吐球的目标指示
	public GameObject m_preSpore; // 孢子
	public GameObject m_preNail;  // 钉子
	public GameObject m_preVoice; // 语音聊天控件

    // UI
    Text m_txtDebugInfo;
	Slider m_sliderStickSize;
	Slider m_sliderAccelerate;
    Button m_btnSpit;
	Scrollbar m_sbSpitSpore;
	Scrollbar m_sbSpitBall;
	Scrollbar m_sbUnfold;
    public GameObject m_uiGamePanel;
    public GameObject m_uiMapEditorPanel;

    // public data

    public int m_nShit = 3;
	public const float BALL_MIN_SIZE = 1.0f; // spore size

	public float m_fBallBaseSpeed;
   
	/// <summary>
	/// new new new 
	/// </summary>
	public float m_fBeanNumPerRange = 0.01f;  // 单位面积豆子产生系数
	public float m_fThornNumPerRange = 0.005f; // 单位面积刺产生系数
	public float m_fBeanSize = 0.5f;
	public float m_fThornSize = 0.5f;

	public float m_fShellShrinkTotalTime;
	public float m_fThornContributeToBallSize = 0.1f; // 刺对球总体积的贡献值

	public float m_fAutoAttenuate = 0.001f; // 球体的自动衰减系数（如果体内含刺，那么刺的体积也跟着衰减）。注意这个衰减是按比例，而不是绝对数值
	public float m_fAutoAttenuateTimeInterval = 1.0f;

	public float m_fArenaWidth = 100.0f;
	public float m_fArenaHeight = 100.0f;
	public float m_fGenerateNewBeanTimeInterval = 10.0f; // 生成新豆子的时间间隔（单位：秒）
	public float m_fGenerateNewThornTimeInterval = 10.0f; // 生成新刺的时间间隔（单位：秒）

	public float m_fSpitSporeTimeInterval = 0.5f;        // 吐孢子时间间隔
	public float m_fSpitBallTimeInterval = 0.5f;         // 吐球时间间隔


	public float m_fMainTriggerUnfoldTime;
	public float m_fMainTriggerPreUnfoldTime;     
	public float m_fUnfoldTimeInterval = 2.0f;           // 膨胀的时间间隔

	public float m_fUnfoldSale = 2.0f;                   // 猥琐膨胀的体积是原始体积的多少倍
	public float m_fUnfoldCircleSale = 0.8f;                   // 猥琐膨胀的体积是原始体积的多少倍

	public float m_fForceSpitTotalTime = 1.0f;
	public float m_fSpitRunTime = 1.0f;                  // 吐孢子或吐球的运行时间(秒)
	public float m_fSpitSporeRunDistance = 30.0f;         // 吐孢子的运行距离(描述绝对距离)
	public float m_fSpitSporeInitSpeed = 5.0f;           // 吐孢子的初速度
	public float m_fSpitSporeAccelerate = 0.0f;                 // 吐孢子的加速度(这个值通过距离、初速度、时间 算出来，不用配置) 
	public float m_fSpittedObjrunRunTime = 1.0f;
	public float m_fSpittedObjrunRunDistance = 5.0f;        


	public float m_fSpitBallRunDistanceMultiple= 3.0f;         // 分球的运行距离（描述母球半径的倍数, 而非直接的距离数值）

	public float m_fBornMinDiameter = 0.0f;              // 初生最小直径
	public float m_fMinDiameterToEatThorn = 0.0f;              // 吃刺最小直径

	public float m_fExplodeRunDistanceMultiple = 0.0f;   // 爆球时子球运行的距离（母球半径的倍数）

	public float m_fMaxBallNumPerPlayer = 0.0f;          // 一个玩家最多的球球数
	public float m_fExpectExplodeNum = 8.0f;             // 每次爆刺期望爆出的球球数

	public float m_fSprayBeanMaxInitSpeed = 4.0f;
	public float m_fSprayBeanMinInitSpeed = 1.5f;
	public float m_fSprayBeanAccelerate = 8.0f;
    public float m_fSprayBeanLiveTime = 40.0f;

    public float m_fCrucifix = 0.0f;
	public int m_nSplitNum = 32;
	public float m_fSplitRunTime = 2f;	
	public float m_fSplitMaxDistance = 40f;
	public float  m_fTriggerRadius = 1.33f;
    // poppin temp
    public GameObject the_spray;
    public GameObject m_goSprays;   

    /// <summary>
    /// 钉子
    public float m_fSpitNailTimeInterval = 0.5f;        // 吐钉子时间间隔
	public float m_fNailLiveTime = 20.0f;  			    // 钉子存活时间

	public LineRenderer m_lineWorldBorderTop;
	public LineRenderer m_lineWorldBorderBottom;
	public LineRenderer m_lineWorldBorderLeft;
	public LineRenderer m_lineWorldBorderRight;

	public GameObject m_goBorderLeft;
	public GameObject m_goBorderRight;
	public GameObject m_goBorderTop;
	public GameObject m_goBorderBottom;


    Vector3 vecTempPos = new Vector3();

	/// <summary>
	/// // public GameObjects
	/// </summary>
	/// 
	// container
	public GameObject m_goThornsSpit;
	public GameObject m_goThorns;
	public GameObject m_goSpores;
	public GameObject m_goBeans;
	public GameObject m_goSpitBallTargets;                    
	public GameObject m_goNails;
	public GameObject m_goBallsBeNailed;
	public GameObject m_goDestroyedBalls;

	public GameObject m_goBalls;

    public GameObject m_goJoyStick;

    public int s_nBallTotalNum = 0;
    public const int c_nStartUserLayerId = 9;

    public Player m_MainPlayer;
	public GameObject m_goMainPlayer;
	public GameObject _mainplayer
	{
		get { return m_goMainPlayer; }
		set { m_goMainPlayer = value; }
	}
	PhotonView m_photonViewMainPlayer;

	/// <summary>
	/// / UI
	/// </summary>
	public Text m_textDebugInfo;

	/// <summary>
	/// / Instantiate Shit
	/// </summary>
	public enum eInstantiateType
	{
		Player,
		Ball,
		Bean,
		Spore,
	};
	public object [] s_Params = new object[256];
    
    // Use this for initialization
    void Awake()
    {
        s_Instance = this;

        ReadGamePlayDataConfig();

        if ( AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game )
        {
            InitArena();
            InitMainPlayer();
            m_uiGamePanel.SetActive( true );
        }
        else
        {
            m_goJoyStick.SetActive( false );
            m_uiGamePanel.SetActive(false);
        }

        if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.MapEditor)
        {
            MapEditor.s_Instance.Init();
            m_uiMapEditorPanel.SetActive( true );

        }
        else
        {
            m_uiMapEditorPanel.SetActive(false);
        }

        //MapEditor.s_Instance.LoadMap();
    }
    
	Dictionary<string, float> m_dicGamePlayDataConfig = new Dictionary<string, float> ();
	string [][]Array;  
	void ReadGamePlayDataConfig()
	{
        return; // 这块功能转移到统一的地图编辑器了

		//按行读取为字符串数组
		string filename = Application.streamingAssetsPath + "/GamePlayData.txt";

		string[] lines = System.IO.File.ReadAllLines( filename  );
		List<string> lst = new List<string> ();
		foreach (string line in lines)
		{
			lst.Add ( line );
		}
		Array = new string [lst.Count][];  //创建二维数组  
		for (int i = 0; i < lst.Count; i++) {
			Array[i] = lst[i].Split ('\t');  
			float val = 0.0f;
			float.TryParse ( Array[i][2], out val );
			m_dicGamePlayDataConfig[Array[i][1]] = val;
		}
	
		m_fBeanNumPerRange = m_dicGamePlayDataConfig["fBeanNumPerRange"];
		m_fThornNumPerRange = m_dicGamePlayDataConfig ["fThornNumPerRange"];
		m_fArenaWidth = m_dicGamePlayDataConfig ["fArenaWidth"];
		m_fArenaHeight = m_dicGamePlayDataConfig ["fArenaHeight"];
		m_fGenerateNewBeanTimeInterval = m_dicGamePlayDataConfig ["fGenerateNewBeanTimeInterval"];
		m_fGenerateNewThornTimeInterval = m_dicGamePlayDataConfig ["fGenerateNewThornTimeInterval"];
		m_fBeanSize = m_dicGamePlayDataConfig ["fBeanSize"];
		m_fThornSize = m_dicGamePlayDataConfig ["fThornSize"];
		m_fThornContributeToBallSize = m_dicGamePlayDataConfig ["fThornContributeToBallSize"];
		m_fBallBaseSpeed = m_dicGamePlayDataConfig ["fBallBaseSpeed"];
		m_fSpitSporeRunDistance = m_dicGamePlayDataConfig ["fSpitSporeRunDistance"];
		m_fSpitRunTime = m_dicGamePlayDataConfig ["fSpitRunTime"];
		m_fSpitBallRunDistanceMultiple = m_dicGamePlayDataConfig ["fSpitBallRunDistanceMultiple"];
		m_fMainTriggerPreUnfoldTime = m_dicGamePlayDataConfig ["fMainTriggerPreUnfoldTime"];
		m_fMainTriggerUnfoldTime = m_dicGamePlayDataConfig ["fMainTriggerUnfoldTime"];
		m_fUnfoldTimeInterval = m_dicGamePlayDataConfig ["fUnfoldTimeInterval"];
		m_fUnfoldSale = m_dicGamePlayDataConfig ["fUnfoldSale"];
		m_fUnfoldCircleSale = m_fUnfoldSale * 0.8f / 2.0f;
		m_fAutoAttenuate = m_dicGamePlayDataConfig ["fAutoAttenuate"];
		m_fAutoAttenuateTimeInterval = m_dicGamePlayDataConfig ["fAutoAttenuateTimeInterval"];
		m_fForceSpitTotalTime = m_dicGamePlayDataConfig ["fForceSpitTotalTime"];
		m_fShellShrinkTotalTime = m_dicGamePlayDataConfig ["fShellShrinkTotalTime"];
		if (m_fShellShrinkTotalTime < 0.1f) {
			m_fShellShrinkTotalTime = 0.1f;
		}
		m_fSpitBallTimeInterval = m_dicGamePlayDataConfig ["fSpitBallTimeInterval"];
		m_fSpitSporeTimeInterval = m_dicGamePlayDataConfig ["fSpitSporeTimeInterval"];
		m_fBornMinDiameter = m_dicGamePlayDataConfig ["fBornMinDiameter"];
		m_fMinDiameterToEatThorn = m_dicGamePlayDataConfig ["fMinDiameterToEatThorn"];
		m_fExplodeRunDistanceMultiple = m_dicGamePlayDataConfig ["fExplodeRunDistanceMultiple"];
		m_fMaxBallNumPerPlayer = m_dicGamePlayDataConfig ["fMaxBallNumPerPlayer"];
		m_fSpitNailTimeInterval = m_dicGamePlayDataConfig ["fSpitNailTimeInterval"];
		m_fNailLiveTime = m_dicGamePlayDataConfig ["fNailLiveTime"];
		m_fSpittedObjrunRunDistance	 = m_dicGamePlayDataConfig ["fSpittedObjrunRunDistance"];
        m_fCrucifix = m_dicGamePlayDataConfig["fCrucifix"];
		m_nSplitNum = (int)m_dicGamePlayDataConfig["nSplitNum"];
		m_fSplitRunTime = m_dicGamePlayDataConfig["fSplitRunTime"];
		m_fSplitMaxDistance = m_dicGamePlayDataConfig["fSplitMaxDistance"];

        m_fSpitSporeAccelerate = CyberTreeMath.GetA(m_fSpitSporeRunDistance,  m_fSpitRunTime);
		m_fSpitSporeInitSpeed = CyberTreeMath.GetV0 (m_fSpitSporeRunDistance,  m_fSpitRunTime);
	}



	void InitMainPlayer ()
	{
		s_Params [0] = eInstantiateType.Player;
		s_Params [1] = 111;
		m_goMainPlayer = PhotonInstantiate ( m_prePlayer, Vector3.zero, s_Params );// GameObject.Instantiate( m_prePlayer );
		m_MainPlayer = m_goMainPlayer.GetComponent<Player>();
		m_goMainPlayer = m_MainPlayer.gameObject;
		m_photonViewMainPlayer = m_goMainPlayer.GetComponent<PhotonView> ();

		m_goMainPlayer.name = "player_" + m_photonViewMainPlayer.ownerId;
		m_goMainPlayer.transform.parent = m_goBalls.transform;

		//GameObject.Instantiate (m_preVoice);
	}


    public int GetMainPlayerPhotonId()
    {
        return m_MainPlayer.photonView.ownerId;
    }

	public GameObject PhotonInstantiate( GameObject prefab, Vector3 vecPos, object[] aryParams = null )
	{
		return PhotonNetwork.Instantiate( prefab.name, vecPos, Quaternion.identity, 0, aryParams );
	}

	public void PhotonDestroy( GameObject obj )
	{
		PhotonNetwork.Destroy ( obj );
	}

    void GameUpdate()
    {
        if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game)
        {
            return;
        }

        InitArenaBeans();
        InitArenaThorns();
        Bean.GenerateNewBeanLoop();
        Thorn.GenerateNewThornLoop();

        // SpitSpore ColdDown
        if (m_fSpitSporeCurColdDownTime > 0.0f)
        {
            m_fSpitSporeCurColdDownTime -= Time.deltaTime;
        }

        // SpitNail ColdDown
        if (m_fSpitNailCurColdDownTime > 0.0f)
        {
            m_fSpitNailCurColdDownTime -= Time.deltaTime;
        }

        // SpitBall ColdDown
        if (!m_bForceSpit)
        {
            if (m_fSpitBallCurColdDownTime > 0.0f)
            {
                m_fSpitBallCurColdDownTime -= Time.deltaTime;
            }
        }

        ForceSpitting();

        if (m_fUnfoldCurColdDownTime > 0.0f)
        {
            m_fUnfoldCurColdDownTime -= Time.deltaTime;
        }

        ProcessKeyboard();
		ProcessMouse ();

        AutoAttenuate();

        DoReborn();


        if (m_goMainPlayer)
        {
            //m_textDebugInfo.text = "主角球球数: " + m_goMainPlayer.transform.childCount.ToString () + "\n场景豆子和场景刺总数:" + (m_nBeanTotalNumDueToRange + m_nThornTotalNumDueToRange) .ToString();
            //m_textDebugInfo.text = PhotonNetwork.time.ToString();
            //m_textDebugInfo.text = PhotonNetwork.isMasterClient.ToString();
            //m_textDebugInfo.text = the_spray.transform.childCount.ToString();
        }

        m_textDebugInfo.text = "当前球球总数：" + s_nBallTotalNum;

		RoomInfo[] artRooms =  PhotonNetwork.GetRoomList();
		for (int i = 0; i < artRooms.Length; i++) {
			RoomInfo room = artRooms [i];
			Debug.Log ( room.Name );
		}
    }

	void Update () 
	{
        GameUpdate();

    }

	// 判断当前是否点击在了UI上
	public bool IsPointerOverUI()
	{

		PointerEventData eventData = new PointerEventData(UnityEngine.EventSystems.EventSystem.current);
		eventData.pressPosition = Input.mousePosition;
		eventData.position = Input.mousePosition;

		List<RaycastResult> list = new List<RaycastResult>();
		UnityEngine.EventSystems.EventSystem.current.RaycastAll(eventData, list);

		return list.Count > 0;

	}

	void ProcessMouse ()
	{
		if (IsPointerOverUI ()) {
			return;
		}

		if (Input.GetMouseButtonDown (1)) {
			if (m_MainPlayer) {
				m_MainPlayer.SetMoving ( false );
			}
		}

		if (Input.GetMouseButtonDown (0)) {
			if (m_MainPlayer) {
				m_MainPlayer.SetMoving ( true );
			}
		} 
	}

	void ProcessKeyboard()
	{
		if (Input.GetKey(KeyCode.Q))
		{
			//SpitSpore ();
		} 

		if (!UI_Spit.s_bUsingUi) {
			if (Input.GetKey (KeyCode.W)) {
				BeginSpit ();
			} else {
				if (m_bForceSpit) {
					EndSpit (); // poppin test for optimizing
				}
			}
		}

		if (Input.GetKey(KeyCode.E))
		{
			if (Launcher.m_eGameMode == Launcher.eGameMode.kuangzhang) {
				Unfold ();
			}
		} 

		if (Input.GetKey(KeyCode.R))
		{
			if (m_fSplitTimeCount > 1f) {
				OneBtnSplitBall ();
				m_fSplitTimeCount = 0f;
			}

		} 
		m_fSplitTimeCount += Time.deltaTime;

		if (Input.GetKey(KeyCode.T))
		{
			Test_AddThornOfBallSize();
		}

	}

	float m_fSplitTimeCount = 0f;

	float m_fArenaRange = 0.0f;
	float m_nBeanTotalNumDueToRange = 0;
	float m_nCurBeanTotalNum = 0;
	float m_nCurGeneraedByMeBeanTotalNum = 0;
	float m_nThornTotalNumDueToRange = 0;
	float m_nCurThornTotalNum = 0;
	void InitArena()
	{
		m_fArenaRange = m_fArenaWidth * m_fArenaHeight;
		m_nBeanTotalNumDueToRange = (int)( m_fBeanNumPerRange * m_fArenaRange );
		m_nThornTotalNumDueToRange =  (int)( m_fThornNumPerRange * m_fArenaRange );
		m_fWorldLeft = -m_fArenaWidth / 2.0f;
		m_fWorldRight = m_fArenaWidth / 2.0f; 
		m_fWorldBottom = -m_fArenaHeight / 2.0f;
		m_fWorldTop = m_fArenaHeight / 2.0f;
	}

	// 为竞技场生成豆子。注意一定要分帧处理，不然会卡死。


	// 平衡场景豆子的总量
	float m_fGenerateBeanTime = 0.0f;
	float m_fGenerateThornTime = 0.0f;
	const float c_GenerateTotalTime = 10.0f;
	public void InitArenaBeans()
	{
		m_nCurBeanTotalNum = m_goBeans.transform.childCount;

		if (m_nCurBeanTotalNum >= m_nBeanTotalNumDueToRange  ) {
			return;
		}

		for (int i = 0; i < 10; i++) {
			if (m_nCurBeanTotalNum >= m_nBeanTotalNumDueToRange  ) {
				break;
			}
			GenerateBean ();
		}

	}

	// 平衡刺的个数
	public void InitArenaThorns()
	{
		if (m_nCurThornTotalNum >= m_nThornTotalNumDueToRange) {
			return;
		}

		for (int i = 0; i < 10; i++) {
			if (m_nCurThornTotalNum >= m_nThornTotalNumDueToRange) {
				break;
			}
			GenerateThorn ();
		}

	}




	/*
	public void CalculateCurTotalBeanNum()
	{
		vecWorldMin = Camera.main.ScreenToWorldPoint ( vecScreenMin );
		vecWorldMax = Camera.main.ScreenToWorldPoint ( vecScreenMax );
		float fWorldWidth = vecWorldMax.x - vecWorldMin.x;
		float fWorldHeight = vecWorldMax.y - vecWorldMin.y;
		float fRange = fWorldWidth * fWorldHeight;
		m_nCurBeanTotalNum = (int)(fRange * m_fBeanNumPerRange);
	}



	public void CalculateCurTotalThornNum()
	{
		vecWorldMin = Camera.main.ScreenToWorldPoint ( vecScreenMin );
		vecWorldMax = Camera.main.ScreenToWorldPoint ( vecScreenMax );
		float fWorldWidth = vecWorldMax.x - vecWorldMin.x;
		float fWorldHeight = vecWorldMax.y - vecWorldMin.y;
		float fRange = fWorldWidth * fWorldHeight;
		//Debug.Log ( "fRange=" + fRange );
		m_nCurThornTotalNum = (int)(fRange * m_fThornNumPerRange);
	}
*/
	float m_fWorldLeft = 0.0f, m_fWorldRight = 0.0f, m_fWorldBottom = 0.0f, m_fWorldTop = 0.0f;
	public Vector3 RandomPosWithinWorld()
	{
		vecTempPos.x = (float)UnityEngine.Random.Range ( m_fWorldLeft, m_fWorldRight );
		vecTempPos.y = (float)UnityEngine.Random.Range ( m_fWorldBottom, m_fWorldTop );
		vecTempPos.z = 0.0f;
		return vecTempPos;
	}

	Vector3 vecScreenMin = new Vector3 ( 0.0f, 0.0f, 0.0f );
	Vector3 vecScreenMax = new Vector3 ( Screen.width, Screen.height, 0.0f );
	Vector3 vecWorldMin = new Vector3();
	Vector3 vecWorldMax = new Vector3();
	public Vector3 RandomPosWithinScreen( )
	{
		vecTempPos.x = (float)UnityEngine.Random.Range (vecWorldMin.x, vecWorldMax.x);
		vecTempPos.y = (float)UnityEngine.Random.Range (vecWorldMin.y, vecWorldMax.y);
		vecTempPos.z = 0.0f;
		return vecTempPos;
	}

	public void GenerateBean()
	{
		//GameObject goBean = PhotonInstantiate( m_preBean, new Vector3 ( (float)UnityEngine.Random.Range(-200, 200), (float)UnityEngine.Random.Range(-200, 200) , 10.0f ) ) ;
		Vector3 pos = RandomPosWithinWorld();
		GameObject goBean = GameObject.Instantiate( m_preBean );
		Bean bean = goBean.GetComponent<Bean> ();
		//goBean.transform.transform.parent = m_goBeans.transform;
		goBean.transform.localPosition = pos;

		m_nCurGeneraedByMeBeanTotalNum++;
//		bean._isLocal = true;
    }

	public void GenerateThorn()
	{
		//GameObject goThorn = PhotonInstantiate(m_preThorn, new Vector3 ( posX,  posY, 10.0f )  );
		Vector3 pos = RandomPosWithinWorld();
		GameObject goThorn = GameObject.Instantiate( m_preThorn );
		goThorn.transform.parent = m_goThorns.transform;
		goThorn.transform.localPosition = pos;

		m_nCurThornTotalNum++;
//		thorn._isLocal = true;
	}

    void FixedUpdate()
    {


    }

	public float m_fForceSpitPercent = 0.0f;
	bool m_bForceSpit = false;
	float m_fSpitBallCurColdDownTime = 0.0f;
	public void BeginSpit ()
	{
        if (m_bForceSpit)
        {
            return;
        }

		int nAvailableNum = GetCurAvailableBallCount ();
		if (nAvailableNum <= 0) {
			return;
		}

		if (m_fSpitBallCurColdDownTime > 0.0f) { // ColdDown not completed
			return;
		}
		m_fSpitBallCurColdDownTime = m_fSpitBallTimeInterval;

		m_bForceSpit = true;
		m_fForceSpitPercent = 0.0f;

		foreach (Transform child in m_goMainPlayer.transform.transform) {
			Ball ball = child.gameObject.GetComponent<Ball> ();
			ball.ClearAvailableChildSize ();

		} // end foreach

		m_fForceSpitCurTime = 0.0f;
	}

	void DoSpit()
	{

	}

	public void EndSpit ()
	{
		if ( !m_bForceSpit )
		{
			return;	
		}

		int nAvailableNum = GetCurAvailableBallCount ();
		if (nAvailableNum <= 0) {
			return;
		}

		m_bForceSpit = false;

		if (m_MainPlayer) {
			m_MainPlayer.BeginSpit ();
		}
	/*

		foreach (Transform child in m_goMainPlayer.transform.transform) {
			Ball ball = child.gameObject.GetComponent<Ball> ();

			if (ball.IsEjecting ()) {
				continue;
			}

			float fMotherSize = ball.GetSize ();
			float fMotherLeftSize = 0.0f;
			float fChildSize = ball.GetAvailableChildSize( ref fMotherLeftSize );
			if (fChildSize < BALL_MIN_SIZE || fMotherLeftSize < BALL_MIN_SIZE || fMotherLeftSize < fChildSize ) {
				continue;
			}

			// 如果母球身上有刺，则按比例被子球带走
			//float fMotherThornSize = ball.GetThornSize ();
			//float fChildThornSize = ;
			//SetThornSize ( Mathf.Sqrt( fCurThornSize * fCurThornSize + fAddedThornSize * fAddedThornSize ) );
			//ball.SetThornSize ( ball.GetThornSize() * fMotherLeftSize / fMotherSize );

			ball.SetSize ( fMotherLeftSize );
            Main.s_Instance.s_Params[0] = eInstantiateType.Ball;
            Main.s_Instance.s_Params[1] = 222;
			Ball new_ball = Ball.NewOneBall( ball.photonView.ownerId );//PhotonInstantiate (m_preBall, Vector3.zero, Main.s_Instance.s_Params);
			//Ball new_ball = goNewBall.GetComponent<Ball> ();
			new_ball.SetSize ( fChildSize );
			new_ball.transform.parent = m_goMainPlayer.transform;
			ball.CalculateNewBallBornPosAndRunDire ( new_ball, ball._dirx, ball._diry );
			ball.RelateTarget ( new_ball );
			ball.BeginShell ();
			new_ball.SetThornSize ( ball.GetThornSize() * fChildSize / fMotherSize );
			new_ball.BeginShell ();
			new_ball.BeginEject ( ball.m_fSpitBallInitSpeed, ball.m_fSpitBallAccelerate );


		} // end foreach

		*/




	}

	public int GetMainPlayerCurBallCount()
	{
		if ( m_MainPlayer == null) {
			return 0;
		}

		return m_MainPlayer.GetCurMainPlayerBallCount();//m_goMainPlayer.transform.childCount;
	}

	public int GetCurAvailableBallCount()
	{
		return (int)Main.s_Instance.m_fMaxBallNumPerPlayer - GetMainPlayerCurBallCount ();
	}

	float m_fForceSpitCurTime = 0.0f;
	void ForceSpitting ()
	{
		int nAvailableNum = GetCurAvailableBallCount ();
		if (nAvailableNum <= 0) {
			return;
		}

		if (!m_bForceSpit) {
			return;
		}

		m_fForceSpitCurTime += Time.deltaTime;  
		m_fForceSpitPercent = m_fForceSpitCurTime / m_fForceSpitTotalTime; 
		if (m_fForceSpitPercent > 0.5f) {
			m_fForceSpitPercent = 0.5f;
		}
        
		/*
		m_fForceSpitPercent += m_fForceSpitInterval;
		if (m_fForceSpitPercent > 0.5f) {
			m_fForceSpitPercent = 0.5f;
		}
		*/
		int nCount = 0;
		foreach (Transform child in m_goMainPlayer.transform.transform) {
			Ball ball = child.gameObject.GetComponent<Ball> ();
			if (!ball.CheckIfCanForceSpit ()) {
				continue;
			}
			ball.CalculateChildSize ( m_fForceSpitPercent );
			ball.ShowTarget ();
			nCount++;
			if (nCount >= nAvailableNum) {
				break;
			}
		} // end foreach
	}

	// 猥琐地扩张
	float m_fUnfoldCurColdDownTime = 0.0f;
	public void Unfold()
	{
		if (m_fUnfoldCurColdDownTime > 0.0f) { // ColdDown not completed
			return;
		}

		foreach (Transform child in m_goMainPlayer.transform) {
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}

			if (!ball.CheckIfCanUnfold ()) {
				continue;
			}

			ball.BeginPreUnfold ();

		} // end foreach		

		m_fUnfoldCurColdDownTime += ( m_fMainTriggerPreUnfoldTime + m_fMainTriggerUnfoldTime + m_fUnfoldTimeInterval );

	}

	float m_fSpitSporeCurColdDownTime = 0.0f;
	float m_fSpitNailCurColdDownTime = 0.0f; // 吐钉子的ColdDown时间

	// 吐钉子
	public void SpitNail ()
	{
		if (m_fSpitNailCurColdDownTime > 0.0f) { // ColdDown还没结束
			return;
		}

		m_fSpitNailCurColdDownTime = m_fSpitNailTimeInterval;

		foreach (Transform child in m_goMainPlayer.transform) {
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}	

			ball.SpitNail();
		} // end foreach	
	}

	// 吐孢子
	public void SpitSpore()
	{
		if (m_fSpitSporeCurColdDownTime > 0.0f) { // ColdDown还没结束
			return;
		}

		m_fSpitSporeCurColdDownTime = m_fSpitSporeTimeInterval;

		foreach (Transform child in m_goMainPlayer.transform) {
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}	

			Ball ballSpore =  ball.SpitSpore();
			if (ballSpore) {

			}
		} // end foreach		
	}

	// 自动衰减
	float m_fAttenuateTimeCount = 0.0f;
	public void AutoAttenuate()
	{
		// poppin test for optimizing
		return;

        foreach (Transform child in m_goMainPlayer.transform)
        {
            GameObject go = child.gameObject;
            Ball ball = go.GetComponent<Ball>();
            if (ball == null)
            {
                continue;
            }
            ball.AutoAttenuateThorn();
         }

        m_fAttenuateTimeCount += Time.deltaTime;
		if (m_fAttenuateTimeCount < Main.s_Instance.m_fAutoAttenuateTimeInterval) {
			return;
		}
		m_fAttenuateTimeCount = 0.0f;
		foreach (Transform child in m_goMainPlayer.transform) {
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}
			ball.AutoAttenuate ( Main.s_Instance.m_fAutoAttenuate );
		}// end foreach		
	}



	public void Test_AddBallsSize()
	{
		foreach (Transform child in m_goMainPlayer.transform) {
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}	

			ball.SetSize ( ball.GetSize() + 1.0f );

		}// end foreach	
	}

	List<Ball> lstTemp = new List<Ball> ();
	public void Test_KingExplode()
	{
		lstTemp.Clear ();
		foreach (Transform child in m_goMainPlayer.transform) {
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}	
			lstTemp.Add (ball);
		}
			
		for (int i = 0; i < lstTemp.Count; i++) {
			Ball ball = lstTemp [i];
			ball.Explode ();
		}


	}

    public void Test_AddThornSize()
    {
        foreach (Transform child in m_goMainPlayer.transform)
        {
            GameObject go = child.gameObject;
            Ball ball = go.GetComponent<Ball>();
            if (ball == null)
            {
                continue;
            }

            ball.SetThornSize(ball.GetThornSize() + 0.1f);

        }// end foreach	
    }

	public void Test_AddThornOfBallSize() 
	{
		foreach (Transform child in m_goMainPlayer.transform) {
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}	

			if ( ball.GetSize () < (2 * Main.BALL_MIN_SIZE ) ) {
				continue;
			}

			ball.SetThornSize ( ball.GetThornSize() + Main.s_Instance.m_fThornSize );

			// 吃刺也会增加球本身的总体积
			ball.SetSize( ball.GetSize() + Main.s_Instance.m_fThornContributeToBallSize );


		}// end foreach	
	}

	void DoReborn()
	{
		if (m_goMainPlayer == null) {
			return;
		}

		if (m_goMainPlayer.transform.childCount > 0) {
			return;
		}

	    vecTempPos = Vector3.zero;
        if ( MapEditor.s_Instance )
        {
            vecTempPos = MapEditor.s_Instance.RandomGetRebornPos();
        }
		GameObject goBall = PhotonInstantiate(m_preBall, vecTempPos, Main.s_Instance.s_Params) ;
		Ball ball = goBall.GetComponent<Ball> ();
		ball.transform.position = vecTempPos;
		ball.SetSize ( m_fBornMinDiameter );   
		m_MainPlayer.BeginProtect ();

	}

    public void ExitGame()
    {
        
		PhotonNetwork.LeaveRoom ();
        SceneManager.LoadScene( "Launcher" );
	
    }

	public void MergeAll()
	{
		m_MainPlayer.MergeAll ();
	}


	public void SyncMoveInfo( Vector3 vecWorldCursorPos )
	{
		if (m_MainPlayer) {
			m_MainPlayer.SyncMoveInfo ( vecWorldCursorPos );
		}
	}

	List<Player> m_lstPlayers = new List<Player>();
	public void AddOnePlayer( Player player )
	{
		m_lstPlayers.Add ( player );
	}

	public void RemoveOnePlayer( Player player )
	{

	}

	public void SyncFullInfo()
	{
		if (m_MainPlayer) {
			m_MainPlayer.SyncFullInfo ();
		}
	}

	// 一键分球
	public void OneBtnSplitBall()
	{
		if (m_MainPlayer) {
			m_MainPlayer.OneBtnSplitBall ( _playeraction.GetWorldCursorPos() );
		}
	}

	public void StartDaTaoSha()
	{
		if (m_MainPlayer) {
			m_MainPlayer.StartDaTaoSha ();
		}
	}

	public Dictionary<int, TattooFood> m_dicTattooFood = new Dictionary<int, TattooFood>();
	int m_nTattooFoodGUID = 0;
	public int GenerateTattooFoodGUID()
	{
		return m_nTattooFoodGUID++;
	}

	public void AddOnetattooFood ( TattooFood food )
	{
		m_dicTattooFood [food.GetGUID()] = food;
	}

	public void RemoveOnetattoo( int nGUID )
	{
		TattooFood food = null;
		m_dicTattooFood.TryGetValue (nGUID, out food);
		if (food) {
			ResourceManager.s_Instance.RemoveTattooFood ( food );
		}
	}

	public Sprite GetSpriteByPhotonOwnerId( int nOwnerId )
	{
		int nSpriteId = nOwnerId - 1;
		if (nSpriteId >= Main.s_Instance.m_aryBallSprite.Length) {
			nSpriteId = nSpriteId % Main.s_Instance.m_aryBallSprite.Length;
		}
		return m_aryBallSprite [nSpriteId];
	}
} // end Main
