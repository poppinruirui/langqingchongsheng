属性描述	程序关键字	数值	备注	时间的单位都是“秒”。尺寸、速度的单位都是逻辑概念，非具体的像素值，以实际体验感觉为准。
场景豆子的密度系数	fBeanNumPerRange	0.0		
场景刺的密度系数	fThornNumPerRange	0.000		
竞技场长度	fArenaWidth	300		
竞技场宽度	fArenaHeight	200		
场景豆子重生的时间间隔	fGenerateNewBeanTimeInterval	600		
场景刺重生的时间间隔	fGenerateNewThornTimeInterval	100		
吃一个豆子增加的球直径	fBeanSize	0.8		
吃一个刺增加的体内刺体积	fThornSize	12		
吃一个刺增加的球体积	fThornContributeToBallSize	10	吃刺也会增加球的总体积	
球的基础移动速度系数	fBallBaseSpeed	24	这个数值是传给小熊做的操作模块，由那边运算	
吐孢子的距离	fSpitSporeRunDistance	2	直接一个固定值（世界坐标）	
吐孢子或分球的运行时间	fSpitRunTime	0.5		
分球的距离	fSpitBallRunDistanceMultiple	5	母球原半径的倍数	
膨胀前摇时间	fMainTriggerPreUnfoldTime	1		
膨胀持续时间	fMainTriggerUnfoldTime	1		
膨胀ColdDown时间	fUnfoldTimeInterval	1		
膨胀后半径是当前的几倍	fUnfoldSale	1.5		
衰减系数	fAutoAttenuate	0	减的是当前半径的百分比。而非直接减一个固定值	
衰减间隔时间	fAutoAttenuateTimeInterval	1	和衰减系数结合起来用	
力度吐的最长蓄力时间	fForceSpitTotalTime	1	（从最小到能够吐的最大值所经历的时间）	
壳持续的时间	fShellShrinkTotalTime	0		
吐球的ColdDown时间	fSpitBallTimeInterval	0		
吐孢子的ColdDown时间	fSpitSporeTimeInterval	0.1		
初生最小直径	fBornMinDiameter	4		
吃刺最小直径	fMinDiameterToEatThorn	3.5		
爆球时子球运行的距离（母球半径的倍数）	fExplodeRunDistanceMultiple	2		
一个玩家最多的球球数	fMaxBallNumPerPlayer	64		
吐钉子的ColdDown	fSpitNailTimeInterval	5		
钉子的存活时间	fNailLiveTime	60		
钉子的运行距离	fSpittedObjrunRunDistance	20		
十字架旋转速度	fCrucifix	200		
一键等分的份数	nSplitNum	10	
一键等分的运行时间	fSplitRunTime	2
一键等分最远距离	fSplitMaxDistance	100		
