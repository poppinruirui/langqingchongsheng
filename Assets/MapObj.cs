﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObj : MonoBehaviour
{
    public float m_fRotatoinZ = 0.0f;

    float m_fLiveTime = 0.0f;
    bool m_bImortal = true; // 是否永久性物件。如果是，则m_fLiveTime属性无效

    Vector3 vecTempPos = new Vector3();
    Vector2 _speed = new Vector2();

	bool m_bDestroyed = false;

    public enum eMapObjType
    {
        shit,
        polygon,
        bean_spray,  // 豆子喷泉
        reborn_spot, // 重生点
        thorn,       // 刺
		tattoo_food, // 食物
    };

    public eMapObjType m_eObjType = eMapObjType.shit;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    public virtual void Update()
    {
        LiveTimeCounting();
        Moving();
    }

	public eMapObjType GetObjType()
	{
		return m_eObjType;
	}

    public virtual void OnMouseDown()
    {
        if (MapEditor.s_Instance)
        {
            MapEditor.s_Instance.PickObj(this);
        }
    }

    public void SetLiveTime(float fLiveTime)
    {
        m_fLiveTime = fLiveTime;
        m_bImortal = false; // 一旦调用了SetLiveTime()，就自然说明了这个物件不是永久性物件
    }

    void LiveTimeCounting()
    {
        if (m_bImortal)
        {
            return;
        }

        m_fLiveTime -= Time.deltaTime;
        if (m_fLiveTime <= 0.0f)
        {
            DestroyMe();
        }
    }

    public void DestroyMe()
    {
        GameObject.Destroy( this.gameObject );
    }

    float m_fInitSpeed = 0.0f;
    float m_fAccelerate = 0.0f;
    float m_fDirection = 1.0f;
    bool m_bMoving = false;
    public void Local_BeginMove(float fInitSpeed, float fAccelerate, float fDirection)
    {
        m_fInitSpeed = fInitSpeed;
        m_fAccelerate = fAccelerate;
        m_fDirection = fDirection;
        m_bMoving = true;
    }

    void Moving()
    {
        if (!m_bMoving)
        {
            return;
        }

        vecTempPos = this.transform.localPosition;
        _speed.x = Mathf.Sin(m_fDirection) * m_fInitSpeed;
        _speed.y = Mathf.Cos(m_fDirection) * m_fInitSpeed;
        vecTempPos.x += _speed.x * Time.deltaTime;
        vecTempPos.y += _speed.y * Time.deltaTime;
        this.transform.localPosition = vecTempPos;
        m_fInitSpeed += m_fAccelerate * Time.deltaTime;
        if (m_fInitSpeed <= 0.0f)
        {
            EndMove();
        }
    }

    void EndMove()
    {
        m_bMoving = false;
    }

	public void SetDestroyed( bool val )
	{
		m_bDestroyed = val;
		this.gameObject.SetActive ( false );
	}

	public bool IsDestroyed()
	{
		return m_bDestroyed;
	}
}
