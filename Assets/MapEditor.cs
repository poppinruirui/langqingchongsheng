﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.IO;
using System.Net;
using OpenWinForm = System.Windows.Forms;

public class MapEditor : MonoBehaviour {

	public static MapEditor s_Instance = null;

    float m_fMaxCameraSize = 50.0f;
    float m_fCameraZoomSpeed = 50.0f;
    public Ball m_ReferBall;

    float m_fWorldWidth = 300.0f;
    float m_fWorldHeight = 200.0f;
	float m_fHalfWorldWidth = 0f;
	float m_fHalfWorldHeight = 0f;
    public WorldBorder m_goWorldBorderTop;
    public WorldBorder m_goWorldBorderBottom;
    public WorldBorder m_goWorldBorderLeft;
    public WorldBorder m_goWorldBorderRight;

    List<Vector3> m_lstRebornPos = new List<Vector3>();

    public Vector3 vecTemp = new Vector3();
	public Vector3 vecTempPos = new Vector3();
	public Vector3 vecTempScale = new Vector3();

    public GameObject m_preGrassTile;
	public GameObject m_preGridLine;

	public GameObject m_goGridLineContainer;
	public GameObject m_goGrassContainer;
    public GameObject m_goPolygonContainer;
    public GameObject m_goBeanSprayContainer;
    public GameObject m_goRebornSpotContainer;
    public GameObject m_goColorThornContainer;

    const int c_GridNum = 1024;

	Dictionary<string, GrassTile> m_dictGrass = new Dictionary<string, GrassTile> ();
    Dictionary<string, float> m_dicDelProtectTime = new Dictionary<string, float>();
    Dictionary<int, Polygon> m_dicGrass = new Dictionary<int, Polygon>();

    static Vector3 s_vecTempPos = new Vector3();


    float m_fGrassTileWidth = 2.46f;
	float m_fGrassTileHeight = 2.46f;
	float m_fGrassHalfTileWidth = 0.0f;
	float m_fGrassHalfTileHeight = 2.46f;

    MapObj m_CurSelectedObj = null;
    MapObj m_CurMovingObj = null;
    Vector3 m_vecLastMousePos = new Vector3();

    //// !! ----  UI
    public GameObject Screen_Cursor;
    public GameObject UI_MiniMap;
        
    public GameObject[] m_arySubPanel = new GameObject[24];
    public Dropdown dropdownItem;


    // Common
    public Text _txtWorldWidth;
    public Text _txtWorldHeight;
    public Slider _sliderWorldWidth;
    public Slider _sliderWorldHeight;
    public Text _txtThornAttenuateSpeed;
    public Slider _sliderThornAttenuateSpeed;
    public Slider _sliderCamSizeCur;
    public InputField _inputfieldCamSizeMax;
    public Text _textCamSizeCur;
    public Slider _sliderReferBall;
	public InputField _inputYaQiuTiaoJian;
	public InputField _inputYaQiuBaiFenBi;
	public InputField _inputXiQiuSuDu;

    // Polygon
    public Slider _sliderScaleX;
    public Slider _sliderScaleY;
    public Slider _sliderRotation;
    public Text _txtPolygonTitle;
    public Toggle _toogleIsGrass;
    public Toggle _toogleIsGrassSeed;
    public InputField _inputfieldSeedGrassLifeTime;

    // Bean Spray
    public Slider _sliderDensity;
    public Slider _sliderMaxDis;
    public Slider _sliderBeanLifeTime;
    public Text _txtDensity;
    public Text _txtMaxDis;
    public Text _txtBeanLifeTime;
    public Toggle _toogleShowRealtimeBeanSpray;
    public InputField _inputfieldThornChance;
    public Text _txtThornChace;
    int m_nThornChance = 0;

    // Color Thorn
    public Dropdown dropdownColorThorn;
    public Image m_imgColorThorn;
    public InputField m_inputfieldColorThornThornSize;
    public InputField m_inputfieldColorThornBallSize;
    public Toggle m_toggleAddThorn;
    public Toggle m_toggleRemoveThorn;
    public static Dictionary<int, Thorn.ColorThornParam> m_dicColorThornParam = new Dictionary<int, Thorn.ColorThornParam>();

	// DaTaoSha
	public Dropdown _dropdownFoodType;
	public InputField _inputName;
	public InputField _inputDesc;
	public InputField _inputValue;
	public InputField _inputNum;
	public InputField _inputTotalTime;
	public InputField _inputMinScale;
	public Slider _sliderCircleSizeRange;
	public struct sBeiShuDouZiConfig
	{
		public int id;
		public float value;
		public int num;
		public string name;
		public string desc;
	};

	sBeiShuDouZiConfig m_CurEditingBeiShuDouZiConfig;
	List<sBeiShuDouZiConfig> m_lstBeiShuDouZi = new List<sBeiShuDouZiConfig>();
	
    //// !! ---- end UI

    ////
    float m_fSeedGrassLifeTime = 20.0f;

	public float m_fYaQiuTiaoJian = 1.0f;
	public float m_fYaQiuBaiFenBi = 0.5f;
	public float m_fXiQiuSuDu = 1.0f;

    //// Dark Forest
    public DarkForest m_DarkForest;
    public Toggle m_toggleDarkForest;

    public enum eMapEditorOp
    {
        common,               // 整体操作
        polygon,            // 多边形
        bean_spray,         // 豆子喷泉
        color_thorn,        // 彩色刺
		dataosha,           // 大逃杀
        self_rotate_obj,    // 自旋转物体
        lever,              // 杠杆
        chopper_tunnel,     // 铡刀隧道

    };

    eMapEditorOp m_eOp = eMapEditorOp.common;

	public DaTaoSha m_DaTaoSha;

	// Use this for initialization
	void Start () {

        SetCurRoom(AccountManager.GetCurRoom());
        LoadMap( m_szCurRoom );

        if ( AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game )
        {
            Screen_Cursor.SetActive( false );
            m_DarkForest.gameObject.SetActive(false);
            UI_MiniMap.SetActive( false );
        }

        SetCamSize(Camera.main.orthographicSize);
       //  _inputfieldCamSizeMax.text = m_fMaxCameraSize.ToString();
    }

    void Awake()
	{
		s_Instance = this;

		m_fGrassHalfTileWidth = m_fGrassTileWidth / 2.0f;
		m_fGrassHalfTileHeight = m_fGrassTileHeight / 2.0f;

        m_lstRebornPos.Clear();

    }

	// Update is called once per frame
	void Update () {
		ProcessInput ();

    }

    public void Init()
    {
        InitGrid();
        InitDropDownItems();
        InitDropDownColorThorn();
    }

    void InitDropDownItems()
    {
        List<string> showNames = new List<string>();

        showNames.Add( "常规");
        showNames.Add( "多边形 ");
        showNames.Add( "豆子喷泉");
        showNames.Add( "彩色刺" );
		showNames.Add( "大逃杀");
//        showNames.Add( "自旋转物体" );
//        showNames.Add( "杠杆" );
//        showNames.Add( "铡刀隧道" );

        UpdateDropdownView( dropdownItem, showNames);
    }

	void InitDropDownBeiShuDouZi()
	{
		List<string> showNames = new List<string>();
		for (int i = 0; i < m_lstBeiShuDouZi.Count; i++) {
			showNames.Add ( m_lstBeiShuDouZi[i].name );
		}
		UpdateDropdownView(_dropdownFoodType, showNames);
	}

    void InitDropDownColorThorn()
    {
        List<string> showNames = new List<string>();
        showNames.Add("默认白(请选择...)");
        showNames.Add("红");
        showNames.Add("橙");
        showNames.Add("黄");
        showNames.Add("绿");
        showNames.Add("青");
        showNames.Add("蓝");
        showNames.Add("紫");
        UpdateDropdownView(dropdownColorThorn, showNames);
        UpdateThornColor(0);
    }

    void UpdateDropdownView( Dropdown dropdownItem,  List<string> showNames)
    {
        dropdownItem.options.Clear();
        Dropdown.OptionData tempData;
        for (int i = 0; i < showNames.Count; i++)
        {
            tempData = new Dropdown.OptionData();
            tempData.text = showNames[i];
            dropdownItem.options.Add(tempData);
        }
        dropdownItem.captionText.text = showNames[0];
    }

    public static Thorn.ColorThornParam GetColorThornParamByIdx(int nColorIdx)
    {
        Thorn.ColorThornParam param;
        if (!m_dicColorThornParam.TryGetValue(nColorIdx, out param))
        {
            param = new Thorn.ColorThornParam();
            param.ball_size = 0.0f;
            param.thorn_size = 0.0f;
            m_dicColorThornParam[nColorIdx] = param;
        }
        return param;
    }

    void UpdateThornColor( int nColorIdx )
    {
        m_nCurSelectedColorThornIdx = nColorIdx;
           Color color = Thorn.GetColorByIdx(nColorIdx);
        m_imgColorThorn.color = color;
        Thorn.ColorThornParam param;
        param = GetColorThornParamByIdx(nColorIdx);
        m_inputfieldColorThornThornSize.text = param.thorn_size.ToString("f2");
        m_inputfieldColorThornBallSize.text = param.ball_size.ToString("f2");
    }

    int m_nCurSelectedColorThornIdx = 0;
    public void OnDropDownValueChanged_ThornColor()
    {
        int nColorIdx = dropdownColorThorn.value;
        UpdateThornColor(nColorIdx);
    }

    public void OnInputFieldContentChanged_ThornSize()
    {
        float fThornSize = 0.0f;
        if ( !float.TryParse( m_inputfieldColorThornThornSize.text, out fThornSize))
        {
            return;
        }

        Thorn.ColorThornParam param;
        param = GetColorThornParamByIdx(m_nCurSelectedColorThornIdx);
        param.thorn_size = fThornSize;
        m_dicColorThornParam[m_nCurSelectedColorThornIdx] = param;
    }

    public void OnInputFieldContentChanged_BallSize()
    {
        float fBallSize = 0.0f;
        if (!float.TryParse(m_inputfieldColorThornBallSize.text, out fBallSize))
        {
            return;
        }

        Thorn.ColorThornParam param;
        param = GetColorThornParamByIdx(m_nCurSelectedColorThornIdx);
        param.ball_size = fBallSize;
        m_dicColorThornParam[m_nCurSelectedColorThornIdx] = param;
    }

    public void OnToggleValueChanged_AddThorn()
    {
        if (m_toggleAddThorn.isOn)
        {
            m_toggleRemoveThorn.isOn = false;
        }

    }

    public void OnToggleValueChanged_RemoveThorn()
    {
        if (m_toggleRemoveThorn.isOn)
        {
            m_toggleAddThorn.isOn = false;
        }
   
    }

    public void RemoveCurSelectedThorn()
    {
        if ( m_eOp != eMapEditorOp.color_thorn )
        {
            return;
        }

        if ( m_CurSelectedObj == null )
        {
            return;
        }

        Thorn thorn = (Thorn)m_CurSelectedObj;
        if (thorn == null)
        {
            return;
        }

        thorn.DestroyMe();
    }

    public void OnDropDownValueChanged()
    {
        SwitchOp( (eMapEditorOp)dropdownItem.value );
    }

    void SwitchOp(eMapEditorOp op )
    {
        m_eOp = op;
        switch( op )
        {
            case eMapEditorOp.common:
                {
				Enter_Common_SubPanel ();
                }
                break;
      
            case eMapEditorOp.polygon:
                {

                }
                break;
            case eMapEditorOp.bean_spray:
                {

                }
                break;
			case eMapEditorOp.dataosha:
				{
				Enter_DaTaoSha_SubPanel ();
				}
				break;
        }

        for ( int i = 0; i < m_arySubPanel.Length; i++ )
        {
            GameObject panel = m_arySubPanel[i];
            if ( panel == null )
            {
                continue;
            }
            panel.SetActive( false );
        }

        int nIndex = (int)m_eOp;
        if (nIndex < m_arySubPanel.Length)
        {
            GameObject cur_panel = m_arySubPanel[nIndex];
            if (cur_panel)
            {
                cur_panel.SetActive(true);
            }
        }
    }

	void Enter_DaTaoSha_SubPanel ()
	{
		DaTaoSha.s_Instance.SetCurSize ( DaTaoSha.s_Instance.GetMinSize() );
		_sliderCircleSizeRange.value = _sliderCircleSizeRange.minValue;
		_inputTotalTime.text = DaTaoSha.s_Instance.GetTotalTime ().ToString( "f2" );
		_inputMinScale.text = DaTaoSha.s_Instance.GetMinSizePercent ().ToString( "f2" );
		SwitchBeiShuDouZiType (0);
	}

	void Enter_Common_SubPanel()
	{
		DaTaoSha.s_Instance.SetCurSize ( DaTaoSha.s_Instance.GetMaxSize() );
	}

    public void InitGrid()
	{
		for (int i = -c_GridNum; i < c_GridNum; i++) {
				GameObject goLine = GameObject.Instantiate (m_preGridLine);
				goLine.transform.parent = m_goGridLineContainer.transform;
				LineRenderer lr = goLine.GetComponent<LineRenderer>();
            lr.startColor = Color.gray;
            lr.endColor = Color.gray;
            vecTempPos.x = m_fGrassTileWidth * (i + 0.5f);
				vecTempPos.y = -10000.0f;
				lr.SetPosition ( 0, vecTempPos );
				vecTempPos.y = 10000.0f;
				lr.SetPosition ( 1, vecTempPos );
		}

		for (int i = -c_GridNum; i < c_GridNum; i++) {
			GameObject goLine = GameObject.Instantiate (m_preGridLine);
			goLine.transform.parent = m_goGridLineContainer.transform;
			LineRenderer lr = goLine.GetComponent<LineRenderer>();
            lr.startColor = Color.gray;
            lr.endColor = Color.gray;
            vecTempPos.y = m_fGrassTileHeight * (i + 0.5f);
			vecTempPos.x = -10000.0f;
			lr.SetPosition ( 0, vecTempPos );
			vecTempPos.x = 10000.0f;
			lr.SetPosition ( 1, vecTempPos );
		}
	}

    void GetMousePosition( ref int nX, ref int nY )
    {
        vecTempPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float x = 0.0f, y = 0.0f;
        if (vecTempPos.x > 0.0f)
        {
            x = vecTempPos.x + m_fGrassHalfTileWidth;
        }
        else if (vecTempPos.x < 0.0f)
        {
            x = vecTempPos.x - m_fGrassHalfTileWidth;
        }

        if (vecTempPos.y > 0.0f)
        {
            y = vecTempPos.y + m_fGrassHalfTileHeight;
        }
        else if (vecTempPos.y < 0.0f)
        {
            y = vecTempPos.y - m_fGrassHalfTileHeight;
        }

        nX = (int)(x / m_fGrassTileWidth);
        nY = (int)(y / m_fGrassTileHeight);
    }

    bool CheckIfCanMoveObj()
    {
        /*
        if ( m_eOp == eMapEditorOp.color_thorn && ( m_toggleAddThorn.isOn || m_toggleRemoveThorn.isOn ) )
        {
            return false;
        }
        */
        return true;
    }
    bool m_bFirstMouseLefDown = true;
    void ProcessInput()
	{
        int nX = 0, nY = 0;

        if ( AccountManager.m_eSceneMode != AccountManager.eSceneMode.MapEditor )
        {
            return;
        }

		if (IsPointerOverUI ()) {
			return;
		}

		ProcessMouseWheel ();

		ProcessDragCamera ();

        GetMousePosition(ref nX, ref nY);
        GrassTile grass = GetGrass(nX, nY);

        if (Input.GetMouseButtonDown (0)) {
            ProcessColorThorn();

        } // end if (Input.GetMouseButtonDown (0))

        if (Input.GetMouseButton(0))
        {
            vecTempPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (m_bFirstMouseLefDown)
            {
                m_vecLastMousePos = vecTempPos;
                m_bFirstMouseLefDown = false;
            }
            else
            {
                if (m_CurMovingObj)
                {
                    Vector3 vecDelta = vecTempPos - m_vecLastMousePos;
                    m_vecLastMousePos = vecTempPos;
                    vecTempPos = m_CurMovingObj.transform.position;
                    vecTempPos.x += vecDelta.x;
                    vecTempPos.y += vecDelta.y;
                    m_CurMovingObj.transform.position = vecTempPos;

                }
            }
        }
        else
        {
            m_bFirstMouseLefDown = true;
        }

        if ( Input.GetMouseButtonUp(0) )
        {
            m_CurMovingObj = null;
        }

        if (Input.GetKey( KeyCode.Escape )  )
        {
            m_eOp = eMapEditorOp.common;
            PickPolygon( null );
        }

    }

    void ProcessColorThorn()
    {
        if ( m_eOp != eMapEditorOp.color_thorn )
        {
            return;
        }

        if ( m_toggleAddThorn.isOn ) // 添加一个彩色刺
        {
            AddOneThorn(Camera.main.ScreenToWorldPoint(Input.mousePosition), m_nCurSelectedColorThornIdx);
        }
        else // 删除一个彩色刺
        {
            
        }
    }

    void AddOneThorn( Vector3 pos, int nColorIdx )
    {
        GameObject goThorn = null;
        goThorn = GameObject.Instantiate((GameObject)Resources.Load("hedge"));
        goThorn.transform.parent = m_goColorThornContainer.transform;
        goThorn.transform.position = pos;
        Thorn thorn = goThorn.GetComponent<Thorn>();
        thorn.SetColorIdx(nColorIdx);
        thorn.m_eObjType = MapObj.eMapObjType.thorn;
    }

    public GrassTile GetGrass( int nX, int nY )
	{
		GrassTile grass = null;
		string key = nX + "," + nY;
		m_dictGrass.TryGetValue (key, out grass);
		return grass;
	}

	void SetGrass( GrassTile grass, int nX, int nY   )
	{
		string key = nX + "," + nY;
		m_dictGrass[key] = grass;
	}

	void RemoveGrass( GrassTile grass, int nX, int nY )
	{
		if (grass != null) {
			grass.Die ();
		}

		string key = nX + "," + nY;
        m_dictGrass.Remove(key);
        m_dicDelProtectTime[key] = Time.time;
    }

    bool CheckIfDelProtect( int nX, int nY )
    {
        string key = nX + "," + nY;
        float val = 0.0f;
        if ( m_dicDelProtectTime.TryGetValue(key, out val) )
        {
            if ( Time.time - val > 0.5f)
            {
                m_dicDelProtectTime.Remove(key);
                return false;
            }
            else
            {
                return true;
            }
        }

        return false; 
    }

	GrassTile CreateGrassTile( int nX, int nY )
	{
		GrassTile grass = GameObject.Instantiate (m_preGrassTile).GetComponent<GrassTile> ();
		vecTempPos.x = m_fGrassTileWidth * nX;	
		vecTempPos.y = m_fGrassTileHeight * nY;
		grass.transform.localPosition = vecTempPos;
		grass.transform.parent = m_goGrassContainer.transform;
		grass.SetIndex ( nX, nY );
		grass.BindNeighbor (); // 注意，一定要先SetIndex()之后才能BindNeighbor(),否则没有意义。所以BindNeighbor()也没法放在Awake()里面作
		string key = nX + "," + nY;
		m_dictGrass [key] = grass;
		return grass;
	}

	void ProcessMouseWheel()
	{
		if (Input.GetAxis ("Mouse ScrollWheel") < 0) {
            //Camera.main.orthographicSize += 1.0f;//Time.deltaTime * 1.0f;
            SetCamSize(Camera.main.orthographicSize + 1.0f);
        } else if (Input.GetAxis ("Mouse ScrollWheel") >0) {
            //Camera.main.orthographicSize -= 1.0f;//Time.deltaTime * 1.0f;
            SetCamSize(Camera.main.orthographicSize - 1.0f);
        }
	}

    void SetCamSize( float fSize )
    {
        if (fSize < 5.0f)
        {
            fSize = 5.0f;
        }
        Camera.main.orthographicSize = fSize;
        _textCamSizeCur.text = Camera.main.orthographicSize.ToString();
    }

	bool m_bFirstMouseRightDown = true;
	void ProcessDragCamera ()
	{
		if (Input.GetMouseButton(1)) {
			vecTempPos = Camera.main.ScreenToWorldPoint ( Input.mousePosition );
			if (m_bFirstMouseRightDown == true) {
				m_vecLastMousePos = vecTempPos;
				m_bFirstMouseRightDown = false;
				return;
			}
			Vector3 vecDelta = vecTempPos - m_vecLastMousePos;
			Camera.main.transform.position -= vecDelta / 2.0f;
			m_vecLastMousePos = vecTempPos;
		} else {
			m_bFirstMouseRightDown = true;
		}
	}


	// 判断当前是否点击在了UI上
	public bool IsPointerOverUI()
	{

		PointerEventData eventData = new PointerEventData(UnityEngine.EventSystems.EventSystem.current);
		eventData.pressPosition = Input.mousePosition;
		eventData.position = Input.mousePosition;
	
		List<RaycastResult> list = new List<RaycastResult>();
		UnityEngine.EventSystems.EventSystem.current.RaycastAll(eventData, list);
	
		return list.Count > 0;

	}

    public void SaveCommon(XmlDocument xmlDoc, XmlNode nodeCommon)
    {
        /*
        string content = m_fWorldWidth + "," + m_fWorldHeight;
        CreateNode(xmlDoc, nodeCommon, "WorldSize", content);
        content = "";
        foreach ( Transform child in m_goRebornSpotContainer.transform )
        {
            content += child.gameObject.transform.position.x + "," + child.gameObject.transform.position.y + ",";
        }
        CreateNode(xmlDoc, nodeCommon, "RebornSpots", content);
        CreateNode(xmlDoc, nodeCommon, "ThornAttenuateSpeed", m_fThornAttenuateSpeed.ToString());
        CreateNode(xmlDoc, nodeCommon, "CamSizeMax", m_fMaxCameraSize.ToString());
        CreateNode(xmlDoc, nodeCommon, "SeedGrassLifeTime", m_fSeedGrassLifeTime.ToString());
		CreateNode(xmlDoc, nodeCommon, "YaQiu", m_fYaQiuTiaoJian.ToString() + "," + m_fYaQiuBaiFenBi.ToString() );
		CreateNode(xmlDoc, nodeCommon, "XiQiu", m_fXiQiuSuDu.ToString() );
        */
    }

    public void SaveGrass(XmlDocument xmlDoc, XmlNode nodeGrass)
	{
        foreach (Transform child in m_goPolygonContainer.transform)
        {
            Polygon polygon = child.gameObject.GetComponent<Polygon>();
            if ( !polygon.GetIsGrass() )
            {
                continue;
            }
            string content = polygon.m_szFileName + "," + child.localPosition.x + "," + child.localPosition.y + "," + child.localScale.x + "," + child.localScale.y + "," + polygon.m_fRotatoinZ + "," + (polygon.GetIsGrassSeed()?"True":"False");  
            CreateNode(xmlDoc, nodeGrass, "P", content);
        }
	}

    public void SavePolygons(XmlDocument xmlDoc, XmlNode nodePolygon )
    {
        foreach (Transform child in m_goPolygonContainer.transform)
        {
            Polygon polygon = child.gameObject.GetComponent<Polygon>();
            if (polygon.GetIsGrass())
            {
                continue;
            }
            string content = polygon.m_szFileName + "," + child.localPosition.x + "," + child.localPosition.y + "," + child.localScale.x + "," + child.localScale.y + "," + polygon.m_fRotatoinZ;
            CreateNode(xmlDoc, nodePolygon, "P", content);
        }
    }

    public void SaveBeanSpray(XmlDocument xmlDoc, XmlNode nodeSpray)
    {
        foreach (Transform child in m_goBeanSprayContainer.transform)
        {
            Spray spray = child.gameObject.GetComponent<Spray>();
            string content = spray.transform.position.x + "," + spray.transform.position.y + "," + spray.GetDensity() + "," + spray.GetMaxDis() + "," + spray.GetBeanLifeTime() + "," + spray.GetThornChance();
            CreateNode(xmlDoc, nodeSpray, "P", content);
        }
    }

    public void SaveColorThorn(XmlDocument xmlDoc, XmlNode nodeThorn)
    {
        for ( int i = 0; i < Thorn.c_aryThornColor.Length; i++ )
        {
            Thorn.ColorThornParam param = GetColorThornParamByIdx( i );
            string content = param.thorn_size + "," + param.ball_size;
            CreateNode(xmlDoc, nodeThorn, "C", content);
        }

        foreach (Transform child in m_goColorThornContainer.transform)
        {
            Thorn thorn = child.gameObject.GetComponent<Thorn>();
            string content = thorn.transform.position.x + "," + thorn.transform.position.y + "," + thorn.GetColorIdx();
            CreateNode(xmlDoc, nodeThorn, "P", content);
        }
    }


	public void SaveDaTaoSha( XmlDocument xmlDoc, XmlNode node )
	{
		string content = "";
		content += DaTaoSha.s_Instance.GetTotalTime () + "," + DaTaoSha.s_Instance.GetMinSizePercent () + "," + m_lstBeiShuDouZi.Count + ",";
		for (int i = 0; i < m_lstBeiShuDouZi.Count; i++) {
			sBeiShuDouZiConfig config = m_lstBeiShuDouZi [i];
			content += ( config.value + "|" + config.num );
			content += ",";
		}
		CreateNode(xmlDoc, node, "C", content);
	}

    public void CreateNode(XmlDocument xmlDoc,XmlNode parentNode,string name,string value)  
	{  
		XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, name, null);  
		node.InnerText = value;  
		parentNode.AppendChild(node);  
	}

    public void GenerateCommon(XmlNode nodeCommon)
    {
        m_lstRebornPos.Clear();
        UpdateWorldBorder();

        if (nodeCommon == null)
        {
            return;
        }

        int nIndex = 0;

        m_fMaxCameraSize = nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):60f;
        m_fCameraZoomSpeed = nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):60f;
        Main.s_Instance.m_fBeanNumPerRange = nodeCommon.ChildNodes.Count > nIndex?float.Parse( nodeCommon.ChildNodes[nIndex++].InnerText ):0.1f;
        Debug.Log( "m_fBeanNumPerRange= " + Main.s_Instance.m_fBeanNumPerRange );

        /*
        if (nodeCommon.ChildNodes.Count < 1)
        {
            return;
        }


        XmlNode node = nodeCommon.ChildNodes[0];
        string content = node.InnerText;
        string[] ary = content.Split(',');
        if (ary.Length < 2)
        {
            return;
        }

        m_fWorldWidth = float.Parse(ary[0]);
        m_fWorldHeight = float.Parse(ary[1]);
		m_fHalfWorldWidth = m_fWorldWidth / 2f;
		m_fHalfWorldHeight = m_fWorldHeight / 2f;
        _sliderWorldWidth.value = m_fWorldWidth;
        _sliderWorldHeight.value = m_fWorldHeight;

        UpdateWorldBorder();

        if (nodeCommon.ChildNodes.Count >= 2)
        {
            node = nodeCommon.ChildNodes[1];
            content = node.InnerText;
            ary = content.Split(',');
            for (int i = 0; i < ary.Length; i++)
            {
                if (ary[i] == "")
                {
                    continue;
                }
                if (i % 2 == 0)
                {
                    vecTempPos.x = float.Parse(ary[i]);
                }
                else
                {
                    vecTempPos.y = float.Parse(ary[i]);
                    vecTempPos.z = 0.0f;
                    //if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game)
                    // {
                    AddRebornSpot(vecTempPos);
                    // }
                    m_lstRebornPos.Add(vecTempPos);
                }


            }
        }

        if (nodeCommon.ChildNodes.Count >= 3)
        {
            node = nodeCommon.ChildNodes[2];
            m_fThornAttenuateSpeed = float.Parse(node.InnerText);
            _sliderThornAttenuateSpeed.value = m_fThornAttenuateSpeed;
            _txtThornAttenuateSpeed.text = "刺的衰减速度(每秒)：\n" + ( m_fThornAttenuateSpeed);
        }

        if (nodeCommon.ChildNodes.Count >= 4)
        {
            node = nodeCommon.ChildNodes[3];
            m_fMaxCameraSize = float.Parse(node.InnerText);
            _inputfieldCamSizeMax.text = m_fMaxCameraSize.ToString();
        }

        if (nodeCommon.ChildNodes.Count >= 5)
        {
            node = nodeCommon.ChildNodes[4];
            m_fSeedGrassLifeTime = float.Parse(node.InnerText);
            _inputfieldSeedGrassLifeTime.text = m_fSeedGrassLifeTime.ToString();
        }

		if (nodeCommon.ChildNodes.Count >= 6) {
			node = nodeCommon.ChildNodes[5];
			ary = node.InnerText.Split( ',' );
			m_fYaQiuTiaoJian = float.Parse ( ary[0] );
			m_fYaQiuBaiFenBi = float.Parse ( ary[1] );
			_inputYaQiuTiaoJian.text = m_fYaQiuTiaoJian.ToString ();
			_inputYaQiuBaiFenBi.text = m_fYaQiuBaiFenBi.ToString ();
		}

		if (nodeCommon.ChildNodes.Count >= 7) {
			node = nodeCommon.ChildNodes [6];
			m_fXiQiuSuDu = float.Parse ( node.InnerText );
			_inputXiQiuSuDu.text = m_fXiQiuSuDu.ToString ();
		}
        */
    } // end generate common

    public void GenerateDarkForest()
    {
        float fScaleX = m_fWorldWidth * 10.0f / 64.0f;
        float fScaleY = m_fWorldHeight * 10.0f / 64.0f;
        m_DarkForest.transform.localScale = new Vector3( fScaleX, fScaleY, 1.0f );
    }

    int m_nGrassGUID = 0;
    public void GenerateGrass( XmlNode nodeGrass )
	{
        if (nodeGrass == null)
        {
            return;
        }

        for (int i = 0; i < nodeGrass.ChildNodes.Count; i++)
        {
            XmlNode node = nodeGrass.ChildNodes[i];
            string key = node.InnerText;
            string[] ary = key.Split(',');
            string filename = ary[0];
            float posX = float.Parse(ary[1]);
            float posY = float.Parse(ary[2]);
            float scaleX = float.Parse(ary[3]);
            float scaleY = float.Parse(ary[4]);
            float rotationZ = float.Parse(ary[5]);
            bool bSeed = false;
            float fSeedGrassLifeTime = 0.0f;
            if (ary.Length > 6)
            {
                string szShit = ary[6];
                bSeed = bool.Parse(szShit/*ary[6]*/);
            }

  
            string[] aryPointPos = PolygonEditor.LoadXML(Application.dataPath + "/StreamingAssets/MapData/PolygonElement/" + filename);
            Polygon polygon = PolygonEditor.NewPolygon();
           
            polygon.m_szFileName = filename;
            polygon.GeneratePolygon(aryPointPos, false);
            polygon.transform.parent = m_goPolygonContainer.transform;
            vecTempPos.x = posX;
            vecTempPos.y = posY;
            vecTempPos.z = -400.0f;
            polygon.transform.localPosition = vecTempPos;
            vecTemp.x = scaleX;
            vecTemp.y = scaleY;
            vecTemp.z = 1.0f;
            //polygon.transform.localScale = vecTemp;
            polygon.SetScale(vecTemp);
            polygon.transform.localRotation = Quaternion.identity;
            polygon.transform.Rotate(0.0f, 0.0f, rotationZ);
            polygon.m_fRotatoinZ = rotationZ;

            polygon.SetIsGrass(true);
            polygon.SetIsGrassSeed(bSeed);

            polygon.SetGUID(m_nGrassGUID);
            m_dicGrass[m_nGrassGUID] = polygon;
            m_nGrassGUID++;

            // right here
        } // end for
        
    }

    public void GenerateBeanSpray(XmlNode nodeSpray)
    {
        if (nodeSpray == null)
        {
            return;
        }

        for (int i = 0; i < nodeSpray.ChildNodes.Count; i++)
        {
            XmlNode node = nodeSpray.ChildNodes[i];
            string key = node.InnerText;
            string[] ary = key.Split(',');
            vecTempPos.x = float.Parse(ary[0]);
            vecTempPos.y = float.Parse(ary[1]);
            float fDensity = float.Parse(ary[2]);
            float fMaxDis = float.Parse(ary[3]);
            float fBeanLifeTime = float.Parse(ary[4]);
            int nThornChance = 0;
            if (ary.Length >= 6) // 这就是典型的向旧版兼容原则。可以读取旧版存档
            {
                nThornChance = int.Parse( ary[5] );
            }
            _inputfieldThornChance.text = nThornChance.ToString();
            vecTempPos.z = -10.0f;
            AddOneBeanSpray(vecTempPos, fDensity, fMaxDis, fBeanLifeTime, nThornChance );
        }
    }

    public void GenerateColorThorn(XmlNode nodeThorn)
    {
        if (nodeThorn == null)
        {
            return;
        }

        if (nodeThorn.ChildNodes.Count < Thorn.c_aryThornColor.Length)
        {
            return;
        }

        for (int i = 0; i < Thorn.c_aryThornColor.Length; i++)
        {
            XmlNode node = nodeThorn.ChildNodes[i];
            string key = node.InnerText;
            string[] ary = key.Split(',');
            float fThornSize = float.Parse(ary[0]);
            float fBallSize  = float.Parse(ary[1]);
            Thorn.ColorThornParam param;
            param.thorn_size = fThornSize;
            param.ball_size = fBallSize;
            m_dicColorThornParam[i] = param;
            //            AddOneBeanSpray(vecTempPos, fDensity, fMaxDis, fBeanLifeTime, nThornChance);
        }

        for ( int i = Thorn.c_aryThornColor.Length; i < nodeThorn.ChildNodes.Count; i++ )
        {
            XmlNode node = nodeThorn.ChildNodes[i];
            string key = node.InnerText;
            string[] ary = key.Split(',');
            vecTempPos.x = float.Parse(ary[0]);
            vecTempPos.y = float.Parse(ary[1]);
            int nColorIdx = int.Parse(ary[2]);
            AddOneThorn(vecTempPos, nColorIdx); 
        }
    }

    public void GeneratePolygon( XmlNode nodePolygon )
    {
        if (nodePolygon == null)
        {
            return;
        }

        for (int i = 0; i < nodePolygon.ChildNodes.Count; i++)
        {
            XmlNode node = nodePolygon.ChildNodes[i];
            string key = node.InnerText;
            string[] ary = key.Split(',');
            string filename = ary[0];
            float posX = float.Parse(ary[1]);
            float posY = float.Parse(ary[2]);
            float scaleX = float.Parse(ary[3]);
            float scaleY = float.Parse(ary[4]);
            float rotationZ = float.Parse(ary[5]);

            string[] aryPointPos = PolygonEditor.LoadXML(Application.dataPath + "/StreamingAssets/MapData/PolygonElement/" + filename);
            Polygon polygon = PolygonEditor.NewPolygon();

            polygon.m_szFileName = filename;
            polygon.GeneratePolygon(aryPointPos, false);
            polygon.transform.parent = m_goPolygonContainer.transform;
            vecTempPos.x = posX;
            vecTempPos.y = posY;
            vecTempPos.z = 0.0f;
            polygon.transform.localPosition = vecTempPos;
            vecTemp.x = scaleX;
            vecTemp.y = scaleY;
            vecTemp.z = 1.0f;
            polygon.transform.localScale = vecTemp;
            polygon.transform.localRotation = Quaternion.identity;
            polygon.transform.Rotate(0.0f, 0.0f, rotationZ);
            polygon.m_fRotatoinZ = rotationZ;

            polygon.SetIsGrass(false);
            // right here
        } // end for
    }

    public void LoadMap( string szRoomName )
    {
        SetCurRoom( szRoomName );
        string szFileName = IOManager.GenerateClientName( szRoomName );
        GenerateMap(szFileName);
    }  

    void GenerateMap( string szFileName )
    {
        XmlDocument myXmlDoc = new XmlDocument();
        myXmlDoc.Load(szFileName);
        XmlNode root = myXmlDoc.SelectSingleNode("root");

        XmlNode nodeCommon = root.SelectSingleNode("common");
        GenerateCommon(nodeCommon);
        /*
        XmlNode nodeGrass = root.SelectSingleNode( "grass" );
        GenerateGrass(nodeGrass);

        XmlNode nodePolygon = root.SelectSingleNode("polygon");
        GeneratePolygon(nodePolygon);

        XmlNode nodeSpray = root.SelectSingleNode("beanspray");
        GenerateBeanSpray(nodeSpray);

        XmlNode nodeThorn = root.SelectSingleNode("colorthorn");
        GenerateColorThorn(nodeThorn);

		XmlNode nodeDaTaoSha = root.SelectSingleNode ("DaTaoSha");
		GenerateDaTaoSha (nodeDaTaoSha);

        MiniMap.s_Instance.InitMiniMap();
        */
    }

	void GenerateDaTaoSha ( XmlNode nodeDaTaoSha )
	{
        // poppin test 
        return;

        if ( nodeDaTaoSha == null || nodeDaTaoSha.ChildNodes.Count == 0 )
        {
            return;
        }
		XmlNode node = nodeDaTaoSha.ChildNodes [0];
		string[] ary = node.InnerText.Split ( ',' );
		int nIndex = 0;
		DaTaoSha.s_Instance.SetTotalTime ( float.Parse ( ary[nIndex++] ) );
		DaTaoSha.s_Instance.SetMinSizePercent ( float.Parse ( ary[nIndex++] ) );
		int nDouZiTypeNum = int.Parse (ary[nIndex++]);
		for (int i = 0; i < nDouZiTypeNum; i++) {
			sBeiShuDouZiConfig config = new sBeiShuDouZiConfig ();
			string[] sub_ary = ary [nIndex++].Split ( '|' );
			config.id = i;
			config.num = int.Parse (sub_ary [1]);
			config.value = float.Parse (sub_ary [0]);
			if (i == 0) {
				config.name = "单球体积加倍丸";
				config.desc = "哪个球吃了这个丸，体积涨为原来的N倍，以‘属性值’配置的数值为准";
			} else if (i == 1) {
				config.name = "整队体积加倍丸";
				config.desc = "一个球吃了这个丸，整队的球体积涨为各自原来的N倍，以‘属性值’配置的数值为准";
			}
			m_lstBeiShuDouZi.Add( config );
			for (int j = 0; j < config.num; j++) {
				DaTaoSha.s_Instance.AddFoodToCreate ( config.id );
			}
		} // end for i
		//DaTaoSha.s_Instance.InitBeiShuDouZi();
		InitDropDownBeiShuDouZi ();
		SwitchBeiShuDouZiType (0);
	}

    public void SaveMap()
    {
       XmlDocument xmlDoc = new XmlDocument();

        //创建类型声明节点  
        XmlNode node = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", "");
        xmlDoc.AppendChild(node);
        //创建根节点  
        XmlNode root = xmlDoc.CreateElement("root");
        xmlDoc.AppendChild(root);

        //// !---- 保存Common信息
        XmlNode nodeCommon = xmlDoc.CreateElement("common");
        root.AppendChild(nodeCommon);
        SaveCommon( xmlDoc, nodeCommon);

        //// !---- 保存草丛的信息
        XmlNode nodeGrass = xmlDoc.CreateElement("grass");
        root.AppendChild(nodeGrass);
        SaveGrass( xmlDoc, nodeGrass );

        //// !---- 保存多边形的信息
        XmlNode nodePolygon = xmlDoc.CreateElement("polygon");
        root.AppendChild(nodePolygon);
        SavePolygons(xmlDoc, nodePolygon);

        //// !--- 保存豆子喷泉的信息
        XmlNode nodeBeanSpray = xmlDoc.CreateElement("beanspray");
        root.AppendChild(nodeBeanSpray);
        SaveBeanSpray(xmlDoc, nodeBeanSpray);

        //// !--- 保存彩色刺的信息
        XmlNode nodeThorn = xmlDoc.CreateElement("colorthorn");
        root.AppendChild(nodeThorn);
        SaveColorThorn(xmlDoc, nodeThorn);


		//// !----保存“大逃杀”相关信息
		XmlNode nodeDaTaoSha = xmlDoc.CreateElement("DaTaoSha");
		root.AppendChild(nodeDaTaoSha);
		SaveDaTaoSha(xmlDoc, nodeDaTaoSha );
			
        //xmlDoc.Save(Application.dataPath + "/StreamingAssets/MapData/map001.xml");
		//Upload ( "e:\\1.txt", "\\\\192.168.31.1\\tddownload" );    
        AccountManager.s_Instance.SaveAndUploadMapFile( xmlDoc, m_szCurRoom );
    }

	// \\192.168.31.1\共享\liruirui
	void Upload (string fileNamePath, string uriString)
	{
		string fileName = fileNamePath.Substring(fileNamePath.LastIndexOf("\\") + 1);
		string NewFileName = fileName;
		string fileNameExt = fileName.Substring(fileName.LastIndexOf(".") + 1);
		if (uriString.EndsWith("/") == false) uriString = uriString + "/";

		uriString = uriString + NewFileName;
      

		///  创建WebClient实例 
		WebClient myWebClient = new WebClient();
		myWebClient.Credentials = CredentialCache.DefaultCredentials;

	
			// 要上传的文件 
			FileStream fs = new FileStream(fileNamePath, FileMode.Open, FileAccess.Read);
			BinaryReader r = new BinaryReader(fs);
			byte[] postArray = r.ReadBytes((int)fs.Length);
			Stream postStream = myWebClient.OpenWrite(uriString, "PUT");

		
				// 使用UploadFile方法可以用下面的格式
				// myWebClient.UploadFile(uriString,"PUT",fileNamePath);

				if (postStream.CanWrite)
				{
					postStream.Write(postArray, 0, postArray.Length);
					postStream.Close();
					fs.Dispose();
				}
				else
				{
					postStream.Close();
					fs.Dispose();
				}

	}

	void DownLoad(string url, string path)
	{
        WebClient client = new WebClient();

        string URLAddress = url;

        string receivePath= path;

        client.DownloadFile(URLAddress, receivePath );
	}

	public void BtnClick_Load()
	{
        //DownLoad ("\\\\192.168.31.1\\tddownload/1.txt", "f:\\1.txt" );
	}

	public static Dictionary<string, GrassTile> s_dicTemp = new Dictionary<string, GrassTile> ();
	public void ProcessPlayerEncounterGrass( GrassTile grass, int nStatus )
	{
        s_dicTemp.Clear();
        grass.SetStatus( nStatus );
    }

    public void LeaveGrass( Polygon grass )
    {
        grass.SetStatus( 0 );
    }

    public void EnterGrass(Polygon grass)
    {
        grass.SetStatus( 1 );
    }

    public Polygon GetGrassByGUID( int nGrassGUID )
    {
        Polygon grass = null;
        m_dicGrass.TryGetValue(nGrassGUID, out grass);
        return grass;
    }

    public void ProcessGrassSeed( int nOwnerId, int nGrassGUID, double dCurTime)
    {
        Polygon grass = GetGrassByGUID(nGrassGUID);
        if ( grass == null )
        {
            Debug.LogError( "通过GUID没有找到Grass" );
            return;
        }
        
        grass.SeedBecomeGrass(nOwnerId, dCurTime);
    }

    public void ExitMapEditor()
    {
        //SceneManager.LoadScene( "Launcher" );
        SceneManager.LoadScene( "SelectCaster" );
    }

    // 进入多边形编辑器
    public void EnterPolygonEditor()
    {
        SceneManager.LoadScene( "PolygonEditor" );
    }

    public void EnterGrassMode()
    {
       
    }

    void SetOp( eMapEditorOp op )
    {
        m_eOp = op;
    }

    public void PickPolygon( Polygon polygon )
    {
        /*
        m_CurSelectedPolygon = polygon;
        m_CurMovingPolygon = polygon;
        if (m_CurSelectedPolygon == null)
        {
            _txtPolygonTitle.text = "当前未选中多边形";
            return;
        }
        _txtPolygonTitle.text = "当前选中的多边形参数";
        _sliderScaleX.value = polygon.transform.localScale.x;
        _sliderScaleY.value = polygon.transform.localScale.y;
        _sliderRotation.value = polygon.m_fRotatoinZ ;
        */
    }

    public void PickObj( MapObj obj )
    {
        if (IsPointerOverUI())
        {
            return;
        }

        m_CurSelectedObj = obj;
        m_CurMovingObj = obj;
        switch( obj.m_eObjType )
        {
            case MapObj.eMapObjType.polygon:
                {
                    _txtPolygonTitle.text = "当前选中的多边形参数";
                    _sliderScaleX.value = obj.transform.localScale.x;
                    _sliderScaleY.value = obj.transform.localScale.y;
                    _sliderRotation.value = obj.m_fRotatoinZ;
                    _toogleIsGrass.isOn = ((Polygon)obj).GetIsGrass();
                    _toogleIsGrassSeed.isOn = ((Polygon)obj).GetIsGrassSeed();
                }
                break;
            case MapObj.eMapObjType.bean_spray:
                {
                    Spray spray = (Spray)obj;
                    _sliderBeanLifeTime.value = spray.GetBeanLifeTime();
                    _sliderDensity.value = spray.GetDensity();
                    _sliderMaxDis.value = spray.GetMaxDis();
                    _txtDensity.text = _sliderDensity.value.ToString("f2") + " 个/秒";
                    _txtMaxDis.text = _sliderMaxDis.value.ToString("f2") + " (世界坐标单位)";
                    _txtBeanLifeTime.text = _sliderBeanLifeTime.value.ToString("f2") + " 秒";

                }
                break;
            case MapObj.eMapObjType.thorn:
                {
                    if ( m_toggleRemoveThorn.isOn )
                    {
                        RemoveCurSelectedThorn();
                    }
                }
                break;
        }
    }


    public void AddPolygon()
    {
        OpenWinForm.OpenFileDialog op = new OpenWinForm.OpenFileDialog();
        string szFileName = CyberTreeMath.ReOrganizeFilePath(Application.streamingAssetsPath);
        szFileName += "MapData\\PolygonElement\\";
        op.InitialDirectory = szFileName;
        op.Title = "打开一个存档文件";
        op.Filter = "XML文件(*.xml)|*.xml";
        if (op.ShowDialog() == OpenWinForm.DialogResult.OK || op.ShowDialog() == OpenWinForm.DialogResult.Yes)
        {
            string selectName = op.FileName;
            string[] aryPointPos = PolygonEditor.LoadXML(selectName);
            Polygon polygon = PolygonEditor.NewPolygon();
            string[] artTemp = selectName.Split( '\\' );
            polygon.m_szFileName = artTemp[artTemp.Length-1];
            polygon.GeneratePolygon(aryPointPos, false);
            polygon.transform.parent = m_goPolygonContainer.transform;
            vecTempPos.x = Screen.width / 2.0f;
            vecTempPos.y = Screen.height / 2.0f;
            vecTempPos = Camera.main.ScreenToWorldPoint(vecTempPos);
            vecTempPos.z = 0.0f;
            polygon.transform.localPosition = vecTempPos;
            PickPolygon(polygon);
        }
        else
        {
            return;
        }

    }
    
    public static Vector3 GetScreenCenter()
    {
        s_vecTempPos.x = Screen.width / 2.0f;
        s_vecTempPos.y = Screen.height / 2.0f;
        s_vecTempPos = Camera.main.ScreenToWorldPoint(s_vecTempPos);
        s_vecTempPos.z = -10.0f;
        return s_vecTempPos;
    }

    public void OnSliderScaleXChanged()
    {
        if (m_CurSelectedObj == null)
        {
            return;
        }

        if (m_CurSelectedObj.m_eObjType == MapObj.eMapObjType.polygon)
        {
            vecTemp = m_CurSelectedObj.transform.localScale;
            vecTemp.x = _sliderScaleX.value;
            //m_CurSelectedObj.transform.localScale = vecTemp;
            ((Polygon)m_CurSelectedObj).SetScale(vecTemp);
        }
    }

    public void OnSliderScaleYChanged()
    {
        if (m_CurSelectedObj == null)
        {
            return;
        }

        if (m_CurSelectedObj.m_eObjType == MapObj.eMapObjType.polygon)
        {
            vecTemp = m_CurSelectedObj.transform.localScale;
            vecTemp.y = _sliderScaleY.value;
            //m_CurSelectedObj.transform.localScale = vecTemp;
            ((Polygon)m_CurSelectedObj).SetScale(vecTemp);
        }
    }

    public void OnSliderRotationChanged()
    {
        if (m_CurSelectedObj == null)
        {
            return;
        }

        if (m_CurSelectedObj.m_eObjType == MapObj.eMapObjType.polygon)
        {
            m_CurSelectedObj.transform.localRotation = Quaternion.identity;
            m_CurSelectedObj.transform.Rotate(0.0f, 0.0f, _sliderRotation.value);
            m_CurSelectedObj.m_fRotatoinZ = _sliderRotation.value;
        }
    }


    public void RemoveCurSelectedPolygon()
    {
        if (m_CurSelectedObj == null)
        {
            return;
        }

        RemovePolygon((Polygon)m_CurSelectedObj);
        m_CurSelectedObj = null;
        m_CurMovingObj = null; ;
    }

    public static void RemovePolygon( Polygon polygon ) 
    {
        GameObject.Destroy( polygon.gameObject );
    }

    public void AddBeanSpray()
    {
        AddOneBeanSpray( GetScreenCenter(), _sliderDensity.value, _sliderMaxDis.value, _sliderBeanLifeTime.value, 0);
        _txtDensity.text = _sliderDensity.value.ToString("f2") + " 个/秒";
        _txtMaxDis.text = _sliderMaxDis.value.ToString("f2") + " (世界坐标单位)";
        _txtBeanLifeTime.text = _sliderBeanLifeTime.value.ToString("f2") + " 秒";
        _inputfieldThornChance.text = m_nThornChance.ToString();
    }

    public Spray AddOneBeanSpray( Vector3 pos, float fDensity, float fMaxDis, float fBeanLifeTime, int nThornChance )
    {
        GameObject goSpray = GameObject.Instantiate((GameObject)Resources.Load("MapEditor/Spray/preBeanSpray_001"));
        goSpray.transform.parent = m_goBeanSprayContainer.transform;
        goSpray.transform.localPosition = pos;
        Spray spray = goSpray.GetComponent<Spray>();
        spray.SetBeanLifeTime(fBeanLifeTime);
        spray.SetDensity(fDensity);
        spray.SetMaxDis(fMaxDis);
        spray.SetThornChance(nThornChance);
        return spray;
    }

    public void OnSliderValueChanged_BeanSprayDensity()
    {
        if ( m_CurSelectedObj == null )
        {
            return;
        }

        Spray spray = (Spray)m_CurSelectedObj;
        if (spray == null)
        {
            return;
        }

        spray.SetDensity( _sliderDensity.value );
        _txtDensity.text = _sliderDensity.value.ToString("f2") + " 个/秒";
    }

    public void OnSliderValueChanged_BeanSprayMaxDis()
    {
        if (m_CurSelectedObj == null)
        {
            return;
        }

        Spray spray = (Spray)m_CurSelectedObj;
        if (spray == null)
        {
            return;
        }

        spray.SetMaxDis( _sliderMaxDis.value );
        _txtMaxDis.text = _sliderMaxDis.value.ToString("f2") + " (世界坐标单位)";
    }

    public void OnSliderValueChanged_BeanSprayBeanLifeTime()
    {
        if (m_CurSelectedObj == null)
        {
            return;
        }

        Spray spray = (Spray)m_CurSelectedObj;
        if (spray == null)
        {
            return;
        }

        spray.SetBeanLifeTime( _sliderBeanLifeTime.value );
        _txtBeanLifeTime.text = _sliderBeanLifeTime.value.ToString("f2") + " 秒";
    }

    public bool m_bShowRealtimeBeanSpray = true;
    public void ToogleValueChanged_ShowRealtime()
    {
        m_bShowRealtimeBeanSpray = !m_bShowRealtimeBeanSpray;
        if (m_bShowRealtimeBeanSpray == false)
        {

        }
    }

    public void RemoveSelectedSpray()
    {
        if ( m_CurSelectedObj == null )
        {
            return;
        }

        Spray spray = (Spray)m_CurSelectedObj;
        if ( spray == null )
        {
            return;
        }

        GameObject.Destroy( spray.gameObject );
    }

    public void SetPolygonAsGrass()
    {
        if ( m_CurSelectedObj == null )
        {
            return;
        }

        Polygon polygon = (Polygon)m_CurSelectedObj;
        if ( polygon == null )
        {
            return;
        }

        polygon.SetIsGrass( _toogleIsGrass.isOn );
        if (_toogleIsGrass.isOn == false)
        {
            _toogleIsGrassSeed.isOn = false;
        }
    }

    public void OnToggleValueChanged_GrassSeed()
    {
        if ( AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game )
        {
            return;
        }

        if ( m_CurSelectedObj == null )
        {
            return;
        }
        Polygon polygon = (Polygon)m_CurSelectedObj;
        if (polygon == null)
        {
            return;
        }
        if ( !polygon.GetIsGrass() )
        {
            _toogleIsGrassSeed.isOn = false; // 如果根本不是草丛，则无法选择种子模式
            return;
        }

        polygon.SetIsGrassSeed(_toogleIsGrassSeed.isOn);    
    }

    public void OnSliderValueChanged_SetWorldWidth()
    {
        m_fWorldWidth = _sliderWorldWidth.value;
		m_fWorldHeight = m_fWorldWidth;
        UpdateWorldBorder();
    }

    public void OnSliderValueChanged_SetWorldHeight()
    {
        m_fWorldHeight = _sliderWorldHeight.value;
        UpdateWorldBorder();
    }

    void UpdateWorldBorder()
    {
        m_goWorldBorderTop.SetSize(m_fWorldWidth, m_fWorldHeight, 0);
        m_goWorldBorderBottom.SetSize(m_fWorldWidth, m_fWorldHeight, 1);
        m_goWorldBorderLeft.SetSize(m_fWorldWidth, m_fWorldHeight, 2);
        m_goWorldBorderRight.SetSize(m_fWorldWidth, m_fWorldHeight, 3);

        _txtWorldWidth.text = "世界尺寸X:" + m_fWorldWidth.ToString();
        _txtWorldHeight.text = "世界尺寸Y:" + m_fWorldHeight.ToString();

		UpdateDaTaoShaCircle ();
    }

	float m_fDaTaoShaSize = 1.0f;
	void UpdateDaTaoShaCircle ()
	{
		float fSize = m_fWorldWidth > m_fWorldHeight?m_fWorldWidth:m_fWorldHeight;
		LineRenderer Line = m_goWorldBorderTop.GetComponent<LineRenderer> ();
		float fTheBoundsSizeOfBorder = Vector2.Distance (Line.GetPosition (1), Line.GetPosition (0));
		vecTempScale.x = 1f;
		vecTempScale.y = 1f;
		vecTempScale.z = 1f;
		m_DaTaoSha.gameObject.transform.localScale = vecTempScale;
		float fDaTaoShaInitBoundsSize = m_DaTaoSha.m_Render.bounds.size.x;
		float fDaTaoShaScale = fTheBoundsSizeOfBorder / fDaTaoShaInitBoundsSize * 1.414f;
		vecTempScale.x = fDaTaoShaScale;
		vecTempScale.y = fDaTaoShaScale;
		vecTempScale.z = 1f;
		m_DaTaoSha.transform.localScale = vecTempScale;
		m_DaTaoSha.SetMaxSize ( m_fDaTaoShaSize );
		m_fDaTaoShaSize = fDaTaoShaScale;

		UpdateCircleMinSize ();
	}

    // 添加重生点
    public void AddRebornSpot()
    {
        AddRebornSpot( Vector3.zero );
    }

    void AddRebornSpot( Vector3 pos )
    {
        GameObject goSpot = GameObject.Instantiate((GameObject)Resources.Load("MapEditor/preRebornSpot"));
        goSpot.transform.parent = m_goRebornSpotContainer.transform;
        goSpot.transform.position = pos;
    }

    // 删除当前选中的重生点
    public void RemoveCurSelectedRebornSpot()
    {
        if ( m_CurSelectedObj && m_CurSelectedObj.m_eObjType == MapObj.eMapObjType.reborn_spot)
        {
            GameObject.Destroy(m_CurSelectedObj.gameObject);
        }
    }

    public Vector3 RandomGetRebornPos()
    {
        if ( m_lstRebornPos.Count < 1 )
        {
            return Vector3.zero;
        }
        int nIndex =(int)UnityEngine.Random.Range(0.0f, m_lstRebornPos.Count);
        return m_lstRebornPos[nIndex];

    }

    float m_fThornAttenuateSpeed = 0.01f;
    public float GetThornAttenuateSpeed()
    {
        return m_fThornAttenuateSpeed;
    }

    public void OnSliderValueChanged_ThornAttenuateSpeed()
    {
        m_fThornAttenuateSpeed = _sliderThornAttenuateSpeed.value;
        _txtThornAttenuateSpeed.text = "刺的衰减速度(每秒)：\n" + ( m_fThornAttenuateSpeed  );
    }

    public void OnInputFieldContentChanged_ThornChance()
    {
        if (!int.TryParse(_inputfieldThornChance.text, out m_nThornChance))
        {
            m_nThornChance = 0;
        }

        if ( m_CurSelectedObj )
        {
            Spray spray = (Spray)m_CurSelectedObj;
            if ( spray )
            {
                spray.SetThornChance(m_nThornChance);
            }
        }
    }


    public void OnInputFieldValueChanged_CamSizeMax()
    {
        float fValue = 0.0f;
        if (!float.TryParse(_inputfieldCamSizeMax.text, out fValue))
        {
            return;
        }

        if (fValue < 5.0f)
        {
            return;
        }

        m_fMaxCameraSize = fValue;
    }

    public float GetMaxCameraSize()
    {
        return m_fMaxCameraSize;
    }

    public void OnSliderValueChanged_ReferBall()
    {
        /*
        float fSize = _sliderReferBall.value;
        if ( fSize < 0.0f )
        {
            fSize = 0.0f;
        }
        m_ReferBall.Local_SetSize( fSize );
        */
    }

    public void OnInputFieldValueChanged_SeedGrassLifeTime()
    {
        float val = 0.0f;
        if ( !float.TryParse( _inputfieldSeedGrassLifeTime.text, out val  ) )
        {
            return;
        }

        if ( val < 5.0f )
        {
            return;
        }

        m_fSeedGrassLifeTime = val;
    }

    public float GetSeedGrassLifeTime()
    {
        return m_fSeedGrassLifeTime;
    }

    public float GetWorldWidth()
    {
        return m_fWorldWidth;
    }

    public float GetWorldHeight()
    {
        return m_fWorldHeight;
    }

    public void OnToggleValueChanged_DarkForest()
    {
        ToggleDarkForestMode(m_toggleDarkForest.isOn);
    }

    void ToggleDarkForestMode( bool isOn )
    {
        DarkForest.s_Instance.ToggleDarkForestMode(isOn);
    }

	public void OnInputFieldValueChanged_YaQiuTiaoJian()
	{
		float val = 1.0f;
		if (!float.TryParse (_inputYaQiuTiaoJian.text, out val)) {
			val = 1.0f;
		}

		if (val < 1.0f) {
			val = 1.0f;
		}

		m_fYaQiuTiaoJian = val;
	}

	public void OnInputFieldValueChanged_YaQiuNaiFenBi()
	{
		float val = 0.5f;
		if (!float.TryParse (_inputYaQiuBaiFenBi.text, out val)) {
			val = 0.5f;
		}

		if (val < 0.0f || val > 1.0f) {
			val = 0.5f;
		}

		m_fYaQiuBaiFenBi = val;
	}

	public void OnInputFieldValueChanged_XiQiuSuDu()
	{
		float val = 1.0f;
		if (!float.TryParse (_inputXiQiuSuDu.text, out val)) {
			val = 1.0f;
		}

		if (val <= 0.0f) {
			val = 1.0f;
		}

		m_fXiQiuSuDu = val;
	}

	public void OnDropdownValuedChanged_BeiShuDouZiType()
	{
		SwitchBeiShuDouZiType ( _dropdownFoodType.value );
	}

	public void SwitchBeiShuDouZiType( int nType )
	{
		if (nType >= m_lstBeiShuDouZi.Count) {
			return;
		}

		m_CurEditingBeiShuDouZiConfig = m_lstBeiShuDouZi [nType];
		_inputNum.text = m_CurEditingBeiShuDouZiConfig.num.ToString();
		_inputValue.text = m_CurEditingBeiShuDouZiConfig.value.ToString ( "f2" );
	}

	public void OnInputFieldValueChanged_BeiShuDouZi_Value()
	{
		m_CurEditingBeiShuDouZiConfig.value = float.Parse( _inputValue.text );
	}


	public void OnInputFieldValueChanged_BeiShuDouZi_Num()
	{
		m_CurEditingBeiShuDouZiConfig.num = int.Parse( _inputNum.text );
	}

	public void OnInputFieldValueChanged_BeiShuDouZi_Desc()
	{
		m_CurEditingBeiShuDouZiConfig.desc = _inputDesc.text;
	}

	public void OnInputFieldValueChanged_DaTaoSha_TotalTime()
	{
		float val = 60f;
		if (!float.TryParse (_inputTotalTime.text, out val)) {
			return;
		}
		if (val <= 0f) {
			val = 60f;
		}
		DaTaoSha.s_Instance.SetTotalTime ( val );
	}

	public void OnInputFieldValueChanged_DaTaoSha_CircleMinScale()
	{
		float val = 0.0f;
		if (!float.TryParse (_inputMinScale.text, out val)) {
			return;
		}
		if (val <= 0 || val >= 1) {
			return;
		}
		DaTaoSha.s_Instance.SetMinSizePercent ( val );
		UpdateCircleMinSize ();
	}

	public void OnSliderValueChanged_CircleSizeRange()
	{
		DaTaoSha.s_Instance.SetCurSize ( _sliderCircleSizeRange.value );
	}

	void UpdateCircleMinSize()
	{
		_sliderCircleSizeRange.maxValue = DaTaoSha.s_Instance.GetMaxSize ();
		_sliderCircleSizeRange.minValue = DaTaoSha.s_Instance.GetMinSize ();
		_sliderCircleSizeRange.value = _sliderCircleSizeRange.minValue;
	}

	public List<sBeiShuDouZiConfig> GetBeiShuDouZiConfig()
	{
		return m_lstBeiShuDouZi;
	}

	public sBeiShuDouZiConfig GetFoodConfig( int nIndex, ref bool ret )
	{
		sBeiShuDouZiConfig config;
		if (nIndex < 0 || nIndex >= m_lstBeiShuDouZi.Count) {
			ret = false;
			config = new sBeiShuDouZiConfig();
			return config;
		}
		config = m_lstBeiShuDouZi[nIndex];
		ret = true;
		return config;
	}

	public bool CheckIfExceedWorldBorder( float val, int op )
	{
		switch (op) {
		case 0: // right
			{
				if (val > m_fHalfWorldWidth) {
					return true;
				}
			}
			break;
		case 1: // left
			{
				if (val < -m_fHalfWorldWidth) {
					return true;
				}
			}
			break;
		case 2: // top
			{
				if (val > m_fHalfWorldHeight) {
					return true;
				}
			}
			break;
		case 3: // bottom
			{
				if (val < -m_fHalfWorldHeight) {
					return true;
				}
			}
			break;

		} // end switch

		return false;
	}

    string m_szCurRoom = "";
    public void SetCurRoom( string szRoomName )
    {
        m_szCurRoom = szRoomName;
    }

    public void Play()
    {
        AccountManager.s_Instance.PlayRoom( m_szCurRoom );
    }
}
