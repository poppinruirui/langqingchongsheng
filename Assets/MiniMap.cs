﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour {

    public GameObject m_MiniWorld;
    public GameObject m_MainPlayerBallContainer;
    public GameObject m_OtherPlayerBallContainer;

    public static MiniMap s_Instance = null;

    public SpriteRenderer _srBg;
    public SpriteRenderer _srMiniWorld;

    Vector3 vecTempPos = new Vector3();
    Vector3 vecTempScale = new Vector3();

    public const float c_MiniMapPosZ = 0.0f;

    Vector2 m_vecMiniWorldInitScale = new Vector2();

    public Camera m_MiniMapCamera;

    List<MiniMapNode> m_lstMainPlayerBall = new List<MiniMapNode>();
    List<MiniMapNode> m_lstOtherPlayerBall = new List<MiniMapNode>();


    float m_fRatioX = 1.0f;
    float m_fRatioY = 1.0f;

    float m_fUpdatePosTimeCount = 0.0f;

    public CyberFrame m_MainPlayerFrame;

    public float m_fShit = 0.0f;

    public struct MiniMapNode
    {
        public Ball ball;
        public MiniMapBall miniball;
    };

    void Awake()
    {
        s_Instance = this;

        if ( AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game )
        {
            this.gameObject.SetActive( false );
        }

        m_vecMiniWorldInitScale.x = m_MiniWorld.transform.localScale.x;
        m_vecMiniWorldInitScale.y = m_MiniWorld.transform.localScale.y;

        
    }

    // Use this for initialization
    void Start () {
        RefreshPos();
    }

    // Update is called once per frame

    void Update () {

        if ( AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game )
        {
            return;
        }

        m_fUpdatePosTimeCount += Time.deltaTime;
        if (m_fUpdatePosTimeCount > 0.25f)
        {
            UpdateMainPlayerBallsPos();
            UpdateOtherPlayerBallsPos();
            m_fUpdatePosTimeCount = 0.0f;
        }
    }

    public void InitMiniMap()
    {
        float ratio = MapEditor.s_Instance.GetWorldWidth() / MapEditor.s_Instance.GetWorldHeight();
        if (ratio > 1.0f)
        {
            m_MiniWorld.transform.localScale = new Vector3(m_vecMiniWorldInitScale.x, m_vecMiniWorldInitScale.y / ratio, 1.0f );
        }
        else if ( ratio < 1.0f )
        {
            m_MiniWorld.transform.localScale = new Vector3(m_vecMiniWorldInitScale.x / ratio, m_vecMiniWorldInitScale.y, 1.0f);
        }

        m_fRatioX = _srMiniWorld.bounds.size.x / MapEditor.s_Instance.GetWorldWidth();
        m_fRatioY = _srMiniWorld.bounds.size.y / MapEditor.s_Instance.GetWorldHeight();
    }

    void RefreshPos()
    {
        vecTempPos.x = 0;
        vecTempPos.y = Screen.height;
        vecTempPos = m_MiniMapCamera.ScreenToWorldPoint(vecTempPos);
        vecTempPos.z = c_MiniMapPosZ;
		vecTempPos.x += _srMiniWorld.bounds.size.x / 2.0f;
		vecTempPos.y -= _srMiniWorld.bounds.size.y / 2.0f;
        this.transform.position = vecTempPos;
    }

    public void SetMiniMapCameraSize( float val )
    {
        m_MiniMapCamera.orthographicSize = val;
    }


    public void AddBall( Ball ball, bool bIsMainPlayer )
    {
        MiniMapNode node;
        node.ball = ball;
        node.miniball = GameObject.Instantiate((GameObject)Resources.Load("MiniMap_Ball")).GetComponent<MiniMapBall>();
        node.miniball.SetMainColor(ball._srMain.color);
        node.miniball.transform.parent = m_MainPlayerBallContainer.transform;
        node.miniball.transform.localPosition = Vector3.zero;

        if (bIsMainPlayer)
        {
            m_lstMainPlayerBall.Add(node);  
        }
        else
        {
            m_lstOtherPlayerBall.Add(node);
        }

       
    }

    void UpdateMainPlayerBallsPos()
    {
        for ( int i = m_lstMainPlayerBall.Count - 1; i >= 0; i-- )
        {
            MiniMapNode node = m_lstMainPlayerBall[i];
            if (node.ball == null )
            {
                GameObject.Destroy( node.miniball.gameObject );
                m_lstMainPlayerBall.RemoveAt(i);
                continue;
            }

            vecTempPos.x = node.ball.transform.position.x * m_fRatioX;//   / MapEditor.s_Instance.GetWorldWidth() * _srMiniWorld.bounds.size.x;
            vecTempPos.y = node.ball.transform.position.y * m_fRatioY; //  / MapEditor.s_Instance.GetWorldHeight() * _srMiniWorld.bounds.size.y;
            vecTempPos.z = -1.0f;
            vecTempScale.x = vecTempScale.y = node.ball.GetSize() * 0.02f;
            vecTempScale.z = 1.0f;
            node.miniball.transform.localPosition = vecTempPos;
            node.miniball.transform.localScale = vecTempScale;
        }

        CalculateMainPlayerFrameBorderPos();
        m_MainPlayerFrame.UpdateFramePos( m_fLeft, m_fRight, m_fTop, m_fBottom );
    }

    void UpdateOtherPlayerBallsPos()
    {
        for (int i = m_lstOtherPlayerBall.Count - 1; i >= 0; i--)
        {
            MiniMapNode node = m_lstOtherPlayerBall[i];
            if (node.ball == null)
            {
                GameObject.Destroy(node.miniball.gameObject);
                m_lstOtherPlayerBall.RemoveAt(i);
                continue;
            }

            vecTempPos.x = node.ball.transform.position.x / MapEditor.s_Instance.GetWorldWidth() * _srMiniWorld.bounds.size.x;
            vecTempPos.y = node.ball.transform.position.y / MapEditor.s_Instance.GetWorldHeight() * _srMiniWorld.bounds.size.y;
            vecTempPos.z = -1.0f;
            vecTempScale.x = vecTempScale.y = node.ball.GetSize() * 0.02f;
            vecTempScale.z = 1.0f;
            node.miniball.transform.localPosition = vecTempPos;
            node.miniball.transform.localScale = vecTempScale;
        }
    }

    float m_fLeft = 0.0f;
    float m_fRight = 0.0f;
    float m_fTop = 0.0f;
    float m_fBottom = 0.0f;
    void CalculateMainPlayerFrameBorderPos()
    {
        vecTempPos.x = 0;
        vecTempPos.y = 0;
        vecTempPos = Camera.main.ScreenToWorldPoint(vecTempPos);
        m_fLeft = vecTempPos.x * m_fRatioX;
        m_fBottom = vecTempPos.y * m_fRatioY;

        vecTempPos.x = Screen.width;
        vecTempPos.y = Screen.height;
        vecTempPos = Camera.main.ScreenToWorldPoint(vecTempPos);
        m_fTop = vecTempPos.y * m_fRatioY;
        m_fRight = vecTempPos.x * m_fRatioX;
        /*
        m_fLeft = -vecTempPos.x * m_fRatioX;
        m_fRight = vecTempPos.x * m_fRatioX;
        m_fTop = vecTempPos.y * m_fRatioY;
        m_fBottom = -vecTempPos.y * m_fRatioY;
        */
        /*
        bool bFirst = true; 
        for (int i = m_lstMainPlayerBall.Count - 1; i >= 0; i--)
        {
            MiniMapNode node = m_lstMainPlayerBall[i];
            if (node.ball == null || node.miniball == null)
            {
                continue;
            }

            float fLeft = node.miniball.transform.localPosition.x - node.miniball._srMain.bounds.size.x / 2.0f;
            float fRight = node.miniball.transform.localPosition.x + node.miniball._srMain.bounds.size.x / 2.0f;
            float fTop = node.miniball.transform.localPosition.y + node.miniball._srMain.bounds.size.x / 2.0f;
            float fBottom = node.miniball.transform.localPosition.y - node.miniball._srMain.bounds.size.x / 2.0f;
            
            if (bFirst)
            {
                m_fLeft = fLeft;
                m_fRight = fRight;
                m_fTop = fTop;
                m_fBottom = fBottom;
                bFirst = false;
                continue;
            }

            if ( fLeft < m_fLeft)
            {
                m_fLeft = fLeft;
            }

            if ( fRight > m_fRight )
            {
                m_fRight = fRight;
            }

            if ( fTop > m_fTop)
            {
                m_fTop = fTop;
            }

            if ( fBottom < m_fBottom)
            {
                m_fBottom = fBottom;
            }
        } // end for
        */
        UpdateMainPlayerFrameBorder();
    }

    void UpdateMainPlayerFrameBorder()
    {
        
    }
}
