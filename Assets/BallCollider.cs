﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallCollider : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



	// 接触
	void OnCollisionEnter2D(Collision2D other)
	{
		
	}
		
	// 胶着状态(继续PK)
	void OnCollisionStay2D(Collision2D other)
	{
		
	}

	void ProcessCollision( Collision2D other )
	{
		return;

		Ball ballSelf = this.transform.parent.gameObject.GetComponent<Ball> ();  
		Ball ballOpponet = other.transform.gameObject.GetComponent<Ball> ();

		if (ballOpponet && ballSelf) {
			if (ballOpponet._Player.photonView.ownerId != ballSelf._Player.photonView.ownerId) {
			
				Collider2D collider_this = this.transform.gameObject.GetComponent<Collider2D> ();
				Collider2D collider_other = other.transform.gameObject.GetComponent<Collider2D> ();
				Physics2D.IgnoreCollision (ballSelf._Collider, ballOpponet._Collider);
			}

		}

	}
	
	// 分离
	void OnCollisionExit2D(Collision2D coll)
	{
	}
}
