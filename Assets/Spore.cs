﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spore : Ball {

	// Use this for initialization
	void Start ()
	{
		
	}

	void Awake()
	{
		_balltype = eBallType.ball_type_spore;

		Init ();
	}

	public override void  Init ()
	{
		_srMain = this.gameObject.GetComponent<SpriteRenderer> ();
		_Trigger = this.gameObject.GetComponent<CircleCollider2D> ();
	}

	// Update is called once per frame
	public override void Update () {


		//RefeshPosDueToOutOfScreen ();
	}

	public override void FixedUpdate()
	{
		base.FixedUpdate ();

	}

	public void DestroySpore()
	{
		photonView.RPC ( "RPC_DestroySpore", PhotonTargets.All );
	}


	[PunRPC]
	public void RPC_DestroySpore()
	{
		this.gameObject.SetActive ( false );
		_Trigger.enabled = false;
		if (photonView.isMine) {
			Main.s_Instance.PhotonDestroy( this.gameObject );
		}
	}

}
