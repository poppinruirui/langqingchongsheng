﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.KeMengWorld.ElectricMusicBall
{
	public class PlayerManager : Photon.PunBehaviour , IPunObservable
	{
		GameManager m_GameManager;

		GameObject m_goOutter; // 球球的外环

		Vector3 vecTemp = new Vector3();
		Vector3 m_vecMoveTarget = new Vector3();
		float m_fKX = 0.0f;
		float m_fKY = 0.0f;
		
		bool IsFiring = false;
		int  Health = 10;

		[Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
		public static GameObject LocalPlayerInstance;
		
		// Use this for initialization
		void Start () 
		{
			GameObject go;
			go = GameObject.Find ( "GameManager" );
			m_GameManager = go.GetComponent<GameManager> ();

			m_goOutter = this.gameObject.transform.Find ("Outter").gameObject;
		}

		bool m_bAlreadyInBallList = false;

		// Update is called once per frame
		void Update ()
		{
			ProcessInput();

			/*
			if (m_bAlreadyInBallList == false) {
				if (m_GameManager != null) {
					m_GameManager.AddBallToBallList ( this.gameObject );
					m_bAlreadyInBallList = true;
				}

			}
			*/
		}

		void MoveCam()
		{
			if (!photonView.isMine) {
				return;
			}

			if (m_GameManager == null || m_GameManager.m_MainCam == null) {  
				return;
			}

			vecTemp = m_GameManager.m_MainCam.transform.position;
			vecTemp.x = m_goOutter.transform.position.x;
			vecTemp.y = m_goOutter.transform.position.y;
			m_GameManager.m_MainCam.transform.position = vecTemp;
		}

		void FixedUpdate()
		{
			MoveCam ();
			GoLeft ();
			GoRight ();
			GoUp ();
			GoDown ();
		}

		float m_fSpeed = 0.01f;
		bool m_bGoLeft = false;
		bool m_bGoRight = false;
		bool m_bGoUp = false;
		bool m_bGoDown = false;



		void GoUp()
		{
			if (!photonView.isMine) {
				//return;
			}

			if (!m_bGoUp) {
				return;
			}

			vecTemp = this.gameObject.transform.position;
			vecTemp.y += m_fSpeed;
			this.gameObject.transform.position = vecTemp;
		}

		void GoLeft()
		{
			if (!photonView.isMine) {
					//return;
			}

			if (!m_bGoLeft) {
				return;
			}

			vecTemp = this.gameObject.transform.position;
			vecTemp.x -= m_fSpeed;
			this.gameObject.transform.position = vecTemp;
		}

		void GoDown()
		{
			if (!photonView.isMine) {
				//return;
			}

			if (!m_bGoDown) {
				return;                                                                        
			}

			vecTemp = this.gameObject.transform.position;
			vecTemp.y -= m_fSpeed;
			this.gameObject.transform.position = vecTemp;
		}

		void GoRight()
		{
			if (!photonView.isMine) {
				//	return;
			}

			if (!m_bGoRight) {
				return;
			}

			vecTemp = this.gameObject.transform.position;
			vecTemp.x += m_fSpeed;
			this.gameObject.transform.position = vecTemp;
		}

		void ProcessInput()
		{
			return;
			
			if ( !photonView.isMine ) // 只有本客户端的MainPlayer才处理输入操作
			{
				return;
			}

			if (Input.GetMouseButton (0)) {
				m_bGoLeft = true;
			}
			else 
			{
				m_bGoLeft = false;


			}

			if (Input.GetMouseButton (1)) {
				m_bGoRight = true;
			} else {
				m_bGoRight = false;
			}

			if(Input.GetKey (KeyCode.A) )
			{
				m_bGoLeft = true;
			}
			else
			{
				m_bGoLeft = false;
			}

			if (Input.GetKey (KeyCode.W)) {
				m_bGoUp = true;
			} else {
				m_bGoUp = false;
			}

			if (Input.GetKey (KeyCode.S)) {
				m_bGoDown = true;
			} else {
				m_bGoDown = false;
			}


			if (Input.GetKey (KeyCode.D)) {
				m_bGoRight = true;
			} else {
				m_bGoRight = false;
			}

			if (Input.GetMouseButton (0)) {
				m_vecMoveTarget = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				MoveTo (this.gameObject, m_vecMoveTarget.x, m_vecMoveTarget.y, m_fSpeed, ref m_fKX, ref m_fKY);
				//vecTemp = m_GameManager.m_MainCam.transform.position;
				//vecTemp.x += m_fKX * m_fSpeed;
				//vecTemp.y += m_fKY * m_fSpeed;
				//m_GameManager.m_MainCam.transform.position = vecTemp;
			}

		}
		
		void Awake()
		{
			// #Important
			// used in GameManager.cs: we keep track of the localPlayer instance to prevent instantiation when levels are synchronized
			if ( photonView.isMine)
			{
				PlayerManager.LocalPlayerInstance = this.gameObject;
			}
			// #Critical
			// we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
		
			DontDestroyOnLoad(this.gameObject);			
		}

		#region IPunObservable implementation

		public void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
		{
			if ( photonView.isMine && stream.isWriting) // 经过实测，发现每一帧都在做这个write操作
			{
				stream.SendNext (m_bGoLeft);
				stream.SendNext (m_bGoRight);
				stream.SendNext (m_bGoUp);
				stream.SendNext (m_bGoDown);

			}
			else
			{
				// Network player, receive data
				bool bLeft = (bool)stream.ReceiveNext();
				bool bRight = (bool)stream.ReceiveNext();
				bool bUp = (bool)stream.ReceiveNext();
				bool bDown = (bool)stream.ReceiveNext();

				if (this.m_bGoUp != bUp) {
					Debug.Log ( "go up" );
				}

				if (this.m_bGoDown != bDown) {
					Debug.Log ( "go down" );
				}

				if (this.m_bGoLeft != bLeft) {
					Debug.Log ( "go left" );
				}

				if (this.m_bGoRight != bRight) {
					Debug.Log ( "go right" );
				}

				this.m_bGoLeft = bLeft;
				this.m_bGoRight = bRight;
				this.m_bGoUp = bUp;
				this.m_bGoDown = bDown;

			}
		}

		void MoveTo( GameObject go, float fDestX, float fDestY, float fBaseSpeed, ref float fKX, ref float fKY)
		{
			float fSrcX = go.transform.position.x;
			float fSrcY = go.transform.position.y;
			float fDeltaX = fDestX - fSrcX;
			float fDeltaY = fDestY - fSrcY;
			float fShit = Mathf.Sqrt(fDeltaX * fDeltaX + fDeltaY * fDeltaY);
			if ( fShit == 0  )
			{
				return;
			}
			fKX = fDeltaX / fShit;
			fKY = fDeltaY / fShit;
			vecTemp = go.transform.position;
			float fSpeed = fBaseSpeed;
			vecTemp.x += fSpeed * fKX;
			vecTemp.y += fSpeed * fKY;

			//ball.SetMoveDir(fKX, fKY);

			vecTemp.z = 0;
			go.transform.position = vecTemp;

		}

		#endregion
	}
}