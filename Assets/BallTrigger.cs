﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// 接触

	void OnTriggerEnter2D(Collider2D other)
	{
        if ( AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game )
        {
            return;
        }

		ProcessTrigger ( other );
        
		Ball ballSelf = this.transform.gameObject.GetComponent<Ball>();


        if (ballSelf && ballSelf._balltype == Ball.eBallType.ball_type_ball)
        {
            Polygon grass = other.transform.gameObject.GetComponent<Polygon>();
            if (grass && grass.GetIsGrass())
            {
                if (ballSelf.photonView.isMine)
                {
                    if (grass.GetIsGrassSeed())
                    {
                        if (grass.IsNowSeeding())
                        {
                            ballSelf.EatGrassSeed(grass.GetGUID());
                        }
                    }
                    else
                    {
                        MapEditor.s_Instance.EnterGrass(grass);
                    }
                }
            }
        }

        if (ballSelf && ballSelf._balltype == Ball.eBallType.ball_type_ball)
        {
            Thorn thorn = other.transform.gameObject.GetComponent<Thorn>();
            if (thorn)
            {
                ballSelf.EatThorn( thorn);
            }
        }

		if (ballSelf && ballSelf._balltype == Ball.eBallType.ball_type_ball) {
			DaTaoSha dataosha = other.transform.gameObject.GetComponent<DaTaoSha>();
			if ( dataosha )
			{
				ballSelf.DestroyBall();
			}
		}

    }

	void OnTriggerStay2D(Collider2D other)
	{
 	   if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game)
        {
            return;
        }

        ProcessTrigger ( other );
        Ball ballSelf = this.transform.gameObject.GetComponent<Ball>();
		if (ballSelf && ballSelf._balltype == Ball.eBallType.ball_type_ball)
        {
            Polygon grass = other.transform.gameObject.GetComponent<Polygon>();
            if (grass && grass.GetIsGrass())
            {
                if (ballSelf.photonView.isMine)
                {
                    MapEditor.s_Instance.EnterGrass(grass);
                }

				return;
            }
        }

		MapObj obj = other.transform.gameObject.GetComponent<MapObj>();
		if ( ballSelf && ballSelf._balltype == Ball.eBallType.ball_type_ball && obj) {
			switch (obj.GetObjType ()) {
				case MapObj.eMapObjType.tattoo_food:
				{
					ballSelf.ProcessTattooFood ( (TattooFood)obj );
				}
				break;
			}
		}
    }


	void ProcessTrigger( Collider2D other )
	{
        if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game)
        {
            return;
        }

        Ball ballSelf = this.transform.gameObject.GetComponent<Ball> ();
		if (ballSelf == null) {
			return;
		}

		Ball ballOpponet = null;
		ballOpponet = other.transform.gameObject.GetComponent<Ball> ();
		if (ballOpponet == null) {
			return;
		}


		if ( Ball.WhoIsBig( ballSelf, ballOpponet ) == 1) {
			ballSelf.ProcessPk (ballOpponet);
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		Ball ballSelf = this.transform.gameObject.GetComponent<Ball> ();
		if (ballSelf == null) {
			return;
		}
		/*
        BayonetTunnel bayotunnel = other.gameObject.GetComponent<BayonetTunnel>();
        if (ballSelf && ballSelf._balltype == Ball.eBallType.ball_type_ball && bayotunnel)
        {
            ballSelf._bayoTunnel = null;
            return;
        }
		*/
        if (ballSelf && ballSelf._balltype == Ball.eBallType.ball_type_ball)
        {
            Polygon grass = other.transform.gameObject.GetComponent<Polygon>();
            if (grass && grass.GetIsGrass())
            {
                if (ballSelf.photonView.isMine)
                {
                    MapEditor.s_Instance.LeaveGrass(grass);
                }
            }
        }

	
    }
}
