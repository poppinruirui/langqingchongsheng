﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpitBallTarget : MonoBehaviour {

	Ball m_ballTarget = null;
	public SpriteRenderer m_sr = null;
	Vector3 m_vecTemp = new Vector3();

	// Use this for initialization
	void Awake () {
		
		m_sr = this.gameObject.GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetTarget( Ball ballTarget )
	{
		m_ballTarget = ballTarget;
	}

	public float GetBoundsSize()
	{
		return m_sr.bounds.size.x;
	}

	public float GetSize()
	{
		return this.gameObject.transform.localScale.x;
	}
}
